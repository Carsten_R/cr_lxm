function limit = lxlimits(action,varargin);

% LXLIMITS Calculating limits for Hotelling's T^2 and residuals in X-block
%
%  limit = lxlimits('HotT2',NumSamps,NumPC,ConfLimit);
%  limit = lxlimits('Residual',eigvals,NumPC,ConfLimit);
%  limit = lxlimits('Tlim',eigvals,NumPC,NumSamps,ConfLimit);
%
% CR, 09-05-2005

  switch action
    case 'HotT2',
      NumSamps  = varargin{1};
      NumPC     = varargin{2};
      ConfLimit = varargin{3};
      limit = tsqlim(NumSamps,NumPC,ConfLimit);

    case 'Residual',
      eigvals   = varargin{1};
      NumPC     = varargin{2};
      ConfLimit = varargin{3};
      limit = reslim(eigvals,NumPC,ConfLimit);

    case 'Tlim'
      eigvals   = varargin{1};
      NumPC     = varargin{2};
      NumSamps  = varargin{3};
      ConfLimit = varargin{4};
      alpha = (1 - ConfLimit/100)/2;
      limit = sqrt(eigvals(NumPC))*ttestp(alpha,NumSamps-NumPC,2);
      % se Wise-manual side 34 (formel 5)

    case 'Fdist'
      F         = varargin{1};
      dftaeller = varargin{2};
      dfnaevner = varargin{3};
      limit = fcdf(F,dftaeller,dfnaevner);

  end % switch action


% ==============================================================================
function tsqcl = tsqlim(I,NumPC,ConfLimit)
%TSQLIM confidence limits for Hotelling's T^2
%  Inputs are the number of samples (I), the
%  number of PCs used (NumPC), and the confidence
%  limit (ConfLimit) in %. The output (tsqcl) is the
%  confidence limit (tsqcl).
%
%I/O: tsqcl = tsqlim(I,NumPC,ConfLimit);
%
%Example: tsqcl = tsqlim(15,2,95);

if ConfLimit >= 100 | ConfLimit <= 0
  error('confidence limit must be 0<ConfLimit<100')
end
alpha = (100-ConfLimit)/100;
tsqcl = NumPC*(I-1)/(I-NumPC)*ftest(alpha,NumPC,I-NumPC);

% ==============================================================================
function rescl = reslim(eigvals,NumPC,ConfLimit)
%RESLIM confidence limits for Q residuals in X-block
%  Inputs are the number of PCs used (NumPC), the vector
%  of eigenvalues (eigvals), and the confidence limit (ConfLimit)
%  in %. The output (rescl) is the confidence limit.
%  Jackson (1991).

if ConfLimit>=100|ConfLimit<=0
  error('confidence limit must be 0<ConfLimit<100')
end
[m,n] = size(eigvals);
if m>1&n>1
  error('input eigvals must be a vector')
end
if n>m
  eigvals   = eigvals';
  m   = n;
end
if NumPC>=length(eigvals)
  rescl = 0;
else
  ConfLimit = 2*ConfLimit-100;
  theta1 = sum(eigvals(NumPC+1:end,1));
  theta2 = sum(eigvals(NumPC+1:end,1).^2);
  theta3 = sum(eigvals(NumPC+1:end,1).^3);
  h0     = 1-2*theta1*theta3/3/(theta2^2);
  if h0<0.001
    h0 = 0.001;
  end
  ca    = sqrt(2)*erfinv(ConfLimit/100);
  h1    = ca*sqrt(2*theta2*h0^2)/theta1;
  h2    = theta2*h0*(h0-1)/(theta1^2);
  rescl = theta1*(1+h1+h2)^(1/h0);
end

% ==============================================================================
function y = ttestp(x,a,z)
%TTESTP Evaluates t-distribution and its inverse
%  Evaluates a t-distribution with input flag (z).
%  For z = 1 the output (y) is the probability for given
%  t-statistic (x) with (a) degrees of freedom.
%Example: y = ttestp(1.9606,5000,1).
%  y  = 0.025;
%
%  For z = 2 the output (y) is the t-statistic for given
%  probability (x) with (a) degrees of freedom.
%Example: y = ttestp(0.005,5000,2).
%  y  = 2.533;
%
%I/O: y = ttestp(x,a,z)
%
%See also: FTEST, STATDEMO

%Based on a public domain stats toolbox
%Modified 12/94 BMW, 10/96 NBG

aa = a * 0.5;
if z == 1
  xx = a / (a + x^2);
  bb = 0.50;
  tmp = betainc(xx,aa,bb);
  y = tmp * 0.50;
elseif z == 2
  ic = 1;
  xl = 0.0;
  xr = 1.0;
  fxl = -x*2;
  fxr = 1.0 - (x*2);
  if fxl * fxr > 0
    error('* Probability not in the range(0,1) *')
  else
    while ic < 30
	  xx = (xl + xr) * 0.5;
   	  p1 = betainc(xx,aa,0.5);
	  fcs = p1 - (x*2);
	  if fcs * fxl > 0
	    xl = xx;
	    fxl = fcs;
	  else
	    xr = xx;
	    fxr = fcs;
	  end
	  xrmxl = xr - xl;
	  if xrmxl <= 0.0001 | abs(fcs) <= 1E-4
	    break
	  else
	    ic = ic + 1;
   	  end
    end
  end
  if ic == 30
    error(' * Failed to converge *')
  end
  tmp = xx;
  y = sqrt((a - a * tmp) / tmp);
else
  error('z must be 1 or 2')
end


% ==============================================================================
function fstat = ftest(p,n,d,flag)
%FTEST Inverse F test and F test
%  For (flag) set to 1 {default} FTEST calculates the
%  F statistic (fstat) given the probability point (p)
%  and the numerator (n) and denominator (d) degrees
%  of freedom. (flag is an optional input {default = 1}.
%  For (flag) set to 2 FTEST calculates the probability
%  point (fstat) given the F statistic (p) and the
%  numerator (n) and denominator (d) degrees of freedom.
%
%Example:
%  a = ftest(0.05,5,8);
%  a = 3.685;
%  a = ftest(3.685,5,8,2);
%  a = 0.050;
%
%I/O: fstat = ftest(p,n,d,flag);
%
%See also: TTESTP, STATDEMO

%Based on a public domain stats toolbox
%Modified 11/93,12/94 BMW, 10/96,12/97 NBG

n    = n/2;
d    = d/2;
if nargin<4
  flag = 1;
end
if flag==1
  p    = 1-p;
  ic   = 1;
  xl   = 0.0;
  xr   = 1.0;
  fxl  = -p;
  fxr  = 1.0 - p;
  if fxl*fxr>0
    error('probability not in the range(0,1) ')
  else
    while ic<30
      x   = (xl+xr)*0.5;
      p1  = betainc(x,n,d);
      fcs = p1-p;
      if fcs*fxl>0
        xl  = x;
        fxl = fcs;
      else
        xr  = x;
        fxr = fcs;
      end
      xrmxl = xr-xl;
      if xrmxl<=0.0001 | abs(fcs)<=1E-4
        break
      else
        ic  = ic+1;
      end
    end
  end
  if ic == 30
    error(' failed to converge ')
  end
  % Inverted numerator and denominator 12-26-94
  fstat = (d * x) / (n - n * x);
else
  fstat = betainc(2*d/(2*d+2*n*p),d,n);
end


% ==============================================================================
%
% fcdf.m
% chi2cdf.m
% distchck.m
% gamcdf.m
%
% Indsat CR, 13-09-2006 12:26 (forbereder F-stats ved PLS-prędiktion)
% ==============================================================================
function p = fcdf(x,v1,v2);
%FCDF   F cumulative distribution function.
%   P = FCDF(X,V1,V2) returns the F cumulative distribution function
%   with V1 and V2 degrees of freedom at the values in X.
%
%   The size of P is the common size of the input arguments. A scalar input
%   functions as a constant matrix of the same size as the other inputs.

%   References:
%      [1]  M. Abramowitz and I. A. Stegun, "Handbook of Mathematical
%      Functions", Government Printing Office, 1964, 26.6.

%   Copyright 1993-2002 The MathWorks, Inc.
%   $Revision: 2.13 $  $Date: 2002/01/17 21:30:32 $

if nargin < 3,
    error('Requires three input arguments.');
end

[errorcode x v1 v2] = distchck(3,x,v1,v2);

if errorcode > 0
    error('Requires non-scalar arguments to match in size.');
end

%   Initialize P to zero.
p = zeros(size(x));

t = (v1 <= 0 | v2 <= 0 | isnan(x) | isnan(v1) | isnan(v2));
p(t) = NaN;
s = (x==Inf) & ~t;
if any(s)
   p(s) = 1;
   t = t | s;
end

% Compute P when X > 0.
k = find(x > 0 & ~t & isfinite(v1) & isfinite(v2));
if any(k),
% use A&S formula 26.6.2 to relate to incomplete beta function
    % Also use 26.5.2 to avoid cancellation by subtracting from 1
    xx = x(k)./(x(k) + v2(k)./v1(k));
    p(k) = betainc(xx, v1(k)/2, v2(k)/2);
end

if any(~isfinite(v1(:)) | ~isfinite(v2(:)))
   k = find(x > 0 & ~t & isfinite(v1) & ~isfinite(v2) & v2>0);
   if any(k)
      p(k) = chi2cdf(v1(k).*x(k),v1(k));
   end
   k = find(x > 0 & ~t & ~isfinite(v1) & v1>0 & isfinite(v2));
   if any(k)
      p(k) = 1 - chi2cdf(v2(k)./x(k),v2(k));
   end
   k = find(x > 0 & ~t & ~isfinite(v1) & v1>0 & ~isfinite(v2) & v2>0);
   if any(k)
      p(k) = (x(k)>=1);
   end
end


% ==============================================================================
function p = chi2cdf(x,v)
%CHI2CDF Chi-square cumulative distribution function.
%   P = CHI2CDF(X,V) returns the chi-square cumulative distribution
%   function with V degrees of freedom at the values in X.
%   The chi-square density function with V degrees of freedom,
%   is the same as a gamma density function with parameters V/2 and 2.
%
%   The size of P is the common size of X and V. A scalar input
%   functions as a constant matrix of the same size as the other input.

%   References:
%      [1]  M. Abramowitz and I. A. Stegun, "Handbook of Mathematical
%      Functions", Government Printing Office, 1964, 26.4.
%
%   Notice that we do not check if the degree of freedom parameter is integer
%   or not. In most cases, it should be an integer. Numerically, non-integer
%   values still gives a numerical answer, thus, we keep them.

%   Copyright 1993-2002 The MathWorks, Inc.
%   $Revision: 2.10 $  $Date: 2002/01/17 21:30:14 $

if   nargin < 2,
    error('Requires two input arguments.');
end

[errorcode x v] = distchck(2,x,v);

if errorcode > 0
    error('Requires non-scalar arguments to match in size.');
end

% Call the gamma distribution function.
p = gamcdf(x,v/2,2);

% ==============================================================================
function [errorcode,varargout] = distchck(nparms,varargin)
%DISTCHCK Checks the argument list for the probability functions.

%   Copyright 1993-2002 The MathWorks, Inc.
%   $Revision: 2.12 $  $Date: 2002/05/08 18:43:58 $

errorcode = 0;
varargout = varargin;

if nparms == 1
    return;
end

% Get size of each input, check for scalars, copy to output
isscalar = (cellfun('prodofsize',varargin) == 1);

% Done if all inputs are scalars.  Otherwise fetch their common size.
if (all(isscalar)), return; end

n = nparms;

for j=1:n
   sz{j} = size(varargin{j});
end
t = sz(~isscalar);
size1 = t{1};

% Scalars receive this size.  Other arrays must have the proper size.
for j=1:n
   sizej = sz{j};
   if (isscalar(j))
      t = zeros(size1);
      t(:) = varargin{j};
      varargout{j} = t;
   elseif (~isequal(sizej,size1))
      errorcode = 1;
      return;
   end
end

% ==============================================================================
function p = gamcdf(x,a,b)
%GAMCDF Gamma cumulative distribution function.
%   P = GAMCDF(X,A,B) returns the gamma cumulative distribution
%   function with parameters A and B at the values in X.
%
%   The size of P is the common size of the input arguments. A scalar input
%   functions as a constant matrix of the same size as the other inputs.
%
%   Some references refer to the gamma distribution with a single
%   parameter. This corresponds to the default of B = 1.
%
%   GAMMAINC does computational work.

%   References:
%      [1]  L. Devroye, "Non-Uniform Random Variate Generation",
%      Springer-Verlag, 1986. p. 401.
%      [2]  M. Abramowitz and I. A. Stegun, "Handbook of Mathematical
%      Functions", Government Printing Office, 1964, 26.1.32.

%   Copyright 1993-2002 The MathWorks, Inc.
%   $Revision: 2.12 $  $Date: 2002/01/17 21:30:39 $

if nargin < 3,
    b = 1;
end

if nargin < 2,
    error('Requires at least two input arguments.');
end

[errorcode x a b] = distchck(3,x,a,b);

if errorcode > 0
    error('Requires non-scalar arguments to match in size.');
end

% Initialize P to zero.
p = zeros(size(x));

%   Return NaN if the arguments are outside their respective limits.
p(a <= 0 | b <= 0) = NaN;

k = find(x > 0 & ~(a <= 0 | b <= 0));
if any(k),
    p(k) = gammainc(x(k) ./ b(k),a(k));
end

% Make sure that round-off errors never make P greater than 1.
p(p > 1) = 1;

% If we have NaN or Inf, fix if possible
k = ~isfinite(p);
if (any(k)), p(x>=sqrt(realmax)) = 1; end


