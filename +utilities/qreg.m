function [p, ys] = qreg(x, y, fractile, polorder);
% QREG Quantile regression
%
%       p = qreg(x, y, fractile, polorder);
%
% [p, ys] = qreg(x, y, fractile, polorder);
%
% Simplified version of "quantreg.m" by Aslak Grinsted
%
%  Copyright (C) 2008, Aslak Grinsted
%
%   This software may be used, copied, or redistributed as long as it is not
%   sold and this copyright notice is reproduced on each copy made.  This
%   routine is provided as is without any express or implied warranties
%   whatsoever.
%
% References:
%   http://www.econ.uiuc.edu/~roger/research/intro/rq.pdf
%   http://www.econ.uiuc.edu/~roger/research/rq/QRJEP.pdf
%
% Carsten Ridder, 11-11-2015 (lagt ind i +utilities 15-06-2016 11:20:06)


% Create the Vandermonde matrix of order polorder
  vmx = nan(size(x,1),polorder+1);
  vmx(:,1) = ones(size(x));
  for i = 1:polorder
    vmx(:,i+1) = x.^i;
  end
  xx = fliplr(vmx);

  pmean = xx\y; % Use OLS regression as initial guess

  rho = @(r)sum(abs(r.*(fractile-(r<0))));
  p = fminsearch(@(p)rho(y-xx*p),pmean);

  ys = polyval(p,x);
