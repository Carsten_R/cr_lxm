function Label = removeampersand(Label);
% CR, 28-04-2015 11:09:26

  Label = cellstr(Label);
  for i = 1:numel(Label)
    Label{i}(ismember(Label{i},'&')) = '';
  end
