function Out = lxcatstructure(In,dim);

% CATSTRUCTURE Indexed structures are concatenated into matrix fields
%
% Out = UnfoldStructure(In);
%
% E.g:
%
%   output =
%
%   1x191 struct array with fields:
%       X
%       kantbredde
%       kanter
%       N
%       LoLimit
%       frakrange
%       fraks
%       stdfraks
%       CR1
%       medi
%       stdX
%       nonzeros
%       frakslope
%       randomness
%       nonzeroLimit
%       frequentLimit
%       logcells
%
%  Out =
%
%
% Carsten Ridder, January 2005

  if nargin<2
    dim = 1;
  end

  FN = fieldnames(In);
  for j = 1:size(FN,1)
     try % if size(values,1) == 1
       values = cat(dim,In.(FN{j}));
       Out.(FN{j}) = values;
     end
  end



