function stats = lxmeaspredstats(Ref, Pred);

% Omskrevet (håndtering af NaN's)
%
% CR, 02-11-2005
%     15-08-2017 13:54:03


  WS = warning;
  warning off

  [N,K] = size(Ref);
  for k = 1:K
    temp{k} = getstats(Ref(:,k),Pred(:,k));
  end

  dim     = 2;
  temp    = cat(dim,temp{:});
  stats   = utilities.lxcatstructure(temp,dim);

  warning(WS);

% -----------------------------------------
function stats = getstats(Ref,Pred);

% Only one colomn in Ref and Pred
%
% We now take care of NaN's in Ref and/or Pred
%
% CR, 02-11-2005 16:24

  [goodrows, dum] = find(~isnan(sum([Ref, Pred],2)));

  if isempty(goodrows)
      N         = 0;
      RMSE      = NaN;
      SEPCorr   = NaN;
      SEP       = NaN;
      MeanVal   = NaN;
      RangeMean = NaN;
      Stds      = NaN;
      Range_    = NaN;
      bias      = NaN;
      slope     = NaN;
      intercept = NaN;
      r         = NaN;
      r2        = NaN;
  else
      Ref  = Ref(goodrows);
      Pred = Pred(goodrows);
    % Now only NaN-free data

      [N,K] = size(Ref);

      RMSE  = sqrt(sum((Ref - Pred).^2)/N);
      bias  = mean(Ref-Pred);
      SEP   = sqrt(sum((Ref-Pred-bias).^2)/(N-1)); % equals: std(handles.Ref-handles.Pred(:,handles.ActPC))

      SI        = polyfit(Pred,Ref,1);
      slope     = SI(1);
      intercept = SI(2);
      SEPCorr   = sqrt(sum((Ref-Pred*slope - intercept).^2)/(N-2));

      if N <= 2
        r = 1;
      else
        r = diag(corrcoef(Ref,Pred),1);
      end
      r2 = r*r; % CR, 02-11-2005 13:04

      MinVal    = min(Ref);
      MaxVal    = max(Ref);
      RangeMean = mean([MinVal MaxVal]);
      Stds      = std(Ref);

      Range_    = max(Ref) - min(Ref);
      MeanVal   = mean(Ref);
  end

  stats.N         = N;
  stats.RMSE      = RMSE;
  stats.SEPCorr   = SEPCorr;
  stats.SEP       = SEP;
  stats.MeanVal   = MeanVal;
  stats.RangeMean = RangeMean;
  stats.Range     = Range_;
  stats.Std       = Stds;
  stats.bias      = bias;
  stats.slope     = slope;
  stats.intercept = intercept;
  stats.r         = r;
  stats.r2        = r2;

% Bedre rækkefølge (LN + CR, 12-04-2006 14:09)
%  stats.N         = N;
%  stats.RMSE      = RMSE;
%  stats.SEP       = SEP;
%  stats.bias      = bias;
%  stats.SEPCorr   = SEPCorr;
%  stats.slope     = slope;
%  stats.intercept = intercept;
%  stats.MeanVal   = MeanVal;
%  stats.RangeMean = RangeMean;
%  stats.Range     = Range_;
%  stats.Std       = Stds;
%  stats.r         = r;
%  stats.r2        = r2;

  prederr3(Ref,Pred)