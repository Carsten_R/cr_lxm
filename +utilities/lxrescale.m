function Y = lxrescale(Y, TransData);

% Perform the opposite scaling of a matrix (usually the Y-matrix)
%
% CR, 01-03-2004
%     08-07-2004

  [I,J] = size(Y);

  for i=length(TransData):-1:1  % The opposite direction!?!?

     try
       ThisTransform = TransData{i}{1};
     catch
       ThisTransform = TransData{i}; % MYSTIK!!! CR, 23-03-2006 14:53
       lxlogerror('resca-tlop')
     end

     switch ThisTransform
       case 'None', % Do nothing

       case 'Mean center',
             mx = TransData{i}{2};
             Y  = Y + mx(ones(I,1),:);

       case 'Autoscale',
             mx = TransData{i}{2};
             sx = TransData{i}{3};
             Y  = (Y.*sx(ones(I,1),:))+mx(ones(I,1),:);

       case 'SG',            % Do nothing
       case 'FD2',           % Do nothing
       case 'MSC',           % Do nothing
       case 'EISC',          % Do nothing

       case 'SNV',           % Do nothing
       case '2-norm',        % Do nothing
       case 'Infinity-norm', % Do nothing

       case '-log10',
             Y = 10.^(-Y);

     end % switch ThisTransform
  end % for i=length(TransData):-1:1

