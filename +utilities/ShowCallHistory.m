function ShowCallHistory
% CR, 29-09-2016 14:53:42

  ST = dbstack;
  if numel(ST) > 1
    ST = ST(2:end);
  end
  disp(' ')
  disp('---- stack:')
  disp([{ST.name}',{ST.line}'])
  disp(' ')
