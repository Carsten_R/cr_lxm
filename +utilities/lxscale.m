function Xauto = lxscale(X,mx,sx)
%LXSCALE Centers or autoscales a matrix
%  Scales a matrix (X) using means (mx) and standard deviations (sx) specified.
%
%  Autoscaling: Xauto = lxscale(X,mx,stdx);
%  Centering:   Xauto = lxscale(X,mx);
%
%  "neutral":   Xauto = lxscale(X);  (Xauto = X)
%
%  Carsten Ridder, August 2017

  if nargin == 3
    Xauto = bsxfun(@rdivide,bsxfun(@minus,X,mx),sx);
  elseif nargin == 2
    Xauto = bsxfun(@minus,X,mx);
  else
    Xauto = X;
  end
