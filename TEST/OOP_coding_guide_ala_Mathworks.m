% Afluring af Mathworks m�de at kode p� baseret p� eksemplet "cluster analysis", som
% har fire forskellige algoritmer.
%
% Vi har lige nu to forskellige algoritmer: PCA og PLS, som m� kunne implementeres efter samme
% principper.
%
% CR, 01-11-2016 08:29:47


% From: Henrik Toft [mailto:ht@latentix.com]
% Sent: 30. oktober 2016 16:46
% To: �smund Rinnan <asmundrinnan@gmail.com>; Carsten Ridder <cr@carstenridder.dk>
% Cc: S�ren Balling Engelsen <se@latentix.com>
% Subject: Re: Lidt inspiration (RE: M�de onsdag eftermiddag/aften?)
%
% Hej Carsten,
% Jeg har ikke f�et noget ned som funktions-prototyper endnu, men et hurtig overslag kunne se ud som noget i stil med:
%
% lxModel � Generisk �moder objekt� p� linje med �lxTranslate�.
% �+models� � Namespace folder som indeholder �lxPLS� og �lxPCA� som begge er objekter.
%
% �lxPCA� og �lxPLS� har begge to metoder:
% �	�calculate�
% �	�predict�
%
% Disse metoder kaldes via "lxModel".
% �+validation� � Namespace folder som ligger under �+models� som indeholder en funktion (eller et objekt � det kan jeg ikke lige
%                 overskue om giver mening) som �up-front� genererer de s�t der skal bruges til at validere
%                 modellen. Den/de funktioner/objekter der ligger i denne folder vil v�re f�lles for alle modeller (derfor
%                 under �+models�) men stadig adskilt fra model-objekterne.
%




FileNames = dir_ext('C:\Program Files\MATLAB\R2015b\toolbox\stats\clustering\','m',0)

FileNames =
                                     ... \clustering\evalclusters.m  % function obj = evalclusters(X, YorFun, crit, varargin)
                                                                     %   Evaluate clustering solutions.
                                                                     %
                                                                     %   EVA = EVALCLUSTERS(X, CLUST, CRITERION) creates an evaluation object
                                                                     %   which can be used for estimating the number of clusters on data.

... \clustering\+clustering\+evaluation\ClusterCriterion.m           % ClusterCriterion is an abstract class used for clustering evaluation.
                                                                      % classdef (Abstract) ClusterCriterion < classreg.learning.internal.DisallowVectorOps ...
... \clustering\+clustering\+evaluation\CalinskiHarabaszEvaluation.m % classdef CalinskiHarabaszEvaluation  < clustering.evaluation.ClusterCriterion
... \clustering\+clustering\+evaluation\DaviesBouldinEvaluation.m    % classdef DaviesBouldinEvaluation     < clustering.evaluation.ClusterCriterion
... \clustering\+clustering\+evaluation\GapEvaluation.m              % classdef GapEvaluation               < clustering.evaluation.ClusterCriterion
... \clustering\+clustering\+evaluation\SilhouetteEvaluation.m       % classdef SilhouetteEvaluation        < clustering.evaluation.ClusterCriterion



% "\clustering\" er overskriften p� konceptet. LX kunne f.eks. v�re "\lxmodels\"
% "\clustering\+clustering\" er en tom namespace-folder (hvorfor egentlig det!?) med samme navn. LX: "\lxmodels\+lxmodels\"

                                           \clustering\              % Overskriften p� konceptet. LX kunne f.eks. v�re \lxmodels\
                                                                     % Folderen indeholder kun en funktion, som genererer under-klasserne
                                                                     % LX: "makemodel.m"
                                       ... \clustering\+clustering\  % En tom namespace-folder (hvorfor egentlig det!?) med samme navn
                           ... \clustering\+clustering\+evaluation\  % Endnu en namespace-folder, hvor b�de den abstrakte klasse og under-klasserne ligger
                                                                     % Den abstrakte LX-klasse kunne hedde: lxModels



% Princippet m� v�re, at i �verste folder (\clustering) ligger den FUNKTION (evalclusters.m), som er indgangen til alle
% de definerede algoritmer. Koden er en simpel switch-case, som leder input-data videre til den valgte funktion, f.eks:
% E = evalclusters(meas,'kmeans','CalinskiHarabasz','klist',[1:5])

%  narginchk(3,inf);
%
%  [~,critPos] = internal.stats.getParamVal(crit,...
%           {'CalinskiHarabasz', 'GAP', 'Silhouette', 'DaviesBouldin'},...
%           'CRITERION');
%   switch critPos
%       case 1 %'CalinskiHarabasz'
%            obj = clustering.evaluation.CalinskiHarabaszEvaluation(X,YorFun,varargin{:});
%       case 2 %'GAP'
%            obj = clustering.evaluation.GapEvaluation(X,YorFun,varargin{:});
%       case 3 %'Silhouette'
%            obj = clustering.evaluation.SilhouetteEvaluation(X,YorFun,varargin{:});
%       case 4 %'DaviesBouldin'
%            obj = clustering.evaluation.DaviesBouldinEvaluation(X,YorFun,varargin{:});
%   end



