

  sg = transformations.savgol

%  sg =
%
%    savgol with properties:
%
%           funcname: 'savgol'
%           menuname: '&Savitsky-Golay ...'
%         parmprefix: 'SG'
%      allowRepeated: 1
%          exclusive: 0
%           helptext: 'Savitsky-Golay smoothing and differentiation.'


  trans = getTransformationInformation(sg)

%  trans =
%
%      funcname: 'savgol'
%      menuname: '&Savitsky-Golay ...'
%      showname: []
%         width: []
%         order: []
%         deriv: []
%       NumVars: []
%             D: []


  SGtxt = 'SG w=9 o=2 d=2';
  set_parm(sg, SGtxt)
  trans = getTransformationInformation(sg)

%  trans =
%
%      funcname: 'savgol'
%      menuname: '&Savitsky-Golay ...'
%      showname: 'SG w=9 o=2 d=2'
%         width: 9
%         order: 2
%         deriv: 2
%       NumVars: []
%             D: []

  x = gengauss(23,60,36)';

  calculateTransform(sg,x);
  trans = getTransformationInformation(sg)




  sg = transformations.savgol;
  SGtxt = 'SG w=9 o=2 d=2';
  set_parm(sg, SGtxt)

  trans = getTransformationInformation(sg);

  figure
  subplot(311)
    plot(1:numel(x),x,'r-s')
    title(trans.showname)
    grid
  subplot(312)
    plot(1:numel(x),calculateTransform(sg,x),'b-o')
    grid
  subplot(313)
    plot(1:numel(x),applyTransform(sg,x),'b-o')
    grid

