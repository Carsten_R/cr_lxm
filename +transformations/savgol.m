classdef savgol < handle
% savgol Savitsky-Golay smoothing and differentiation
%
%   Usage:
%           h = savgol;
%           h = savgol(width,order,deriv);
%           x   = h.calculateTransform(data);
%           x   = h.applyTransform(data);
%           ast = h.getTransformationInformation;
%
% Carsten Ridder, 29-09-2016 10:38:49

% Properties section

properties (Constant)
   funcname  = 'savgol';              % Transformation object classdef file funcname
   menuname  = '&Savitsky-Golay ...'  % Description suitable for e.g. GUI's

   parmprefix = 'SG'                  % if "parmprefix" is found in "lx.lxt.selectedTransform" call "set_parm",
                                      %

   allowRepeated = true;            % You can godt have more than one occurence in a transform-sequence
   exclusive = false;               % Allow only one transformation tagged as "exclusive" in a transform-sequence
                                    % For now these are exclusive: 'Mean center', 'Autoscale' and 'Pareto scaling'
   helptext  = char('Savitsky-Golay smoothing and differentiation.')
end

properties (Access = private)
  showname % is set in "get_parm", e.g. 'SG w=9 o=2 d=2'
  width    % the number of points in the filter (width)
  order    % the order of the polynomial (order)
  deriv    % the derivative (deriv)
  D        % a sparse derivative matrix (D)
  NumVars  % the number of variables
end

% Methods section
methods
function obj = savgol % (width,order,deriv)
% savgol object constructor function

   if ~isequal(nargin,0)
      ME = MException('savgol:noInputRequired', ...
                      'No input variables should be specified!');
      throw(ME);
   end
end

function x = calculateTransform(obj,data)
% x = h.calculateTransform(data);
   canApply = checkObject(obj,data);
   if canApply
     [x ,obj.D]= lxsg(data,obj.width,obj.order,obj.deriv);
     obj.NumVars = size(data,2);
   else
      x = [];
   end
end

function x = applyTransform(obj,data)
% savgol input data using previously calculated mean and std:
%   x = h.applyTransform(data);
   canApply = checkObject(obj,data);
   if canApply
      x = data*obj.D;
   else
      x = [];
   end
end

function data = inverseTransform(obj,data)
% CR, dsk.2016, Middelfart, 03-11-2016 14:39:41
    ME = MException('savgol:inverseTransform:cannotInverseTransform', ...
                      'This transformation can not be inversed!');
    throw(ME);
end


function trans = getTransformationInformation(obj)
   % Return transformation vectors
   %   ast = h.getTransformationInformation;
   trans.funcname = obj.funcname;
   trans.menuname = obj.menuname;
   trans.showname = obj.showname;
   trans.width    = obj.width;
   trans.order    = obj.order;
   trans.deriv    = obj.deriv;
   trans.NumVars  = obj.NumVars;
   trans.D        = obj.D;
end


function get_parm(obj)
% CR, 01-10-2016 14:40:14

  %% J=350 % tester (29-09-2016 11:13:44)
  J = obj.NumVars;

% Re-use code from latentix.m (function rcmTransform)
  defwidth = 9;  % Tages evt. fra et huske-sted (CR, 06-12-2005 10:57)
  deforder = 2;
  defderiv = 2;

  if isempty(J)
    obj.width = defwidth;
    obj.order = deforder;
    obj.deriv = defderiv;
    return
  end

  defwidth = min(defwidth,floor(J/2)) - ~odd(defwidth);
  if defwidth < 3
    warndlg(['Only ',int2str(J),' variable(s) selected'],'Too few variables ...','modal')
    return
  end

  deforder = min([max(0,round(deforder)),5,defwidth-1]);
  deforder = max(0,deforder);
  defderiv = min(max(0,round(defderiv)),deforder);

  OK = 0;
  while ~OK % CR, 11-04-2006 09:09
    OK = 1;
    sw = '';
    so = '';
    sd = '';

    prompt = {'Window size','Polynomial order','Derivative calculated'}; % v2.00 11-04-2007 16:38
    dlg_title = 'Input Savitzky-Golay parameters';
    num_lines = 1;
    def    = {num2str(defwidth),num2str(deforder),num2str(defderiv)};
    answer = inputdlg(prompt,dlg_title, [1 50; 1 50; 1 50],def);
    if isempty(answer)
      NewTransform = OldTransform;
    else
      width = str2num(answer{1});
      order = str2num(answer{2});
      deriv = str2num(answer{3});

    % Check the inputs (CR, 11-04-2006 09:56)
      w = min(width,floor(J/2)); % Not too big
      w = w + ~odd(w);           % Odd
      w = max(3,w);              % Not too small
      if w ~= width
        defwidth = w;
        sw = sprintf('The width is changed to %g (must be >= 3 and odd)',defwidth);
        OK = 0;
      else
        defwidth = width;
      end

      o = min(w-1,order);        % Not bigger than w-1
      o = min(5,round(o));       % Not bigger than 5
      o = max(0,o);              % Not less than zero
      if o ~= order
        deforder = o;
        so = sprintf('The order is changed to %g (must be <= width-1 and <= 5)',deforder);
        OK = 0;
      else
        deforder = order;
      end

      d = min(round(deriv),o);   % Not bigger than order
      d = max(0,d);              % Not less than zero
      if d ~= deriv
        defderiv = d;
        sd = sprintf('The derivative is changed to %g (must be <= order)',defderiv);
        OK = 0;
     else
        defderiv = deriv;
     end

      if OK
        obj.showname = ['SG w=',answer{1},' o=',answer{2},' d=',answer{3}];
        obj.width = answer{1};
        obj.order = answer{2};
        obj.deriv = answer{3};
      else
        uiwait(msgbox(strvcat(sw,so,sd),'Wrong SG-parameters!','modal'));
      end
    end
  end % while ~OK
end % get_parm

function set_parm(obj, SGtxt)
% HT, 2011-01-13
  tokens = regexp(SGtxt,'(\w*)\s*(.*)','tokens','once');
  params = regexp(tokens{2},'w=(\d+)\s*o=(\d+)\s*d=(\d+)','tokens','once');
  params = cellfun(@(x)(str2double(x)),params,'UniformOutput',false);
  [obj.width,obj.order,obj.deriv] = deal(params{:});

% CR, 01-10-2016 14:45:45
  obj.showname = SGtxt;
end % set_parm


end % methods

methods (Access = private)
function canApply = checkObject(obj,data)
   if size(data,2) ~= obj.NumVars
      canApply = false;
      ME = MException('savgol:applyTransform:WrongNumberOfVariables', ...
                      [int2str(obj.NumVars),' variables required and ',int2str(size(data,2)),' supplied!']);
   else
      canApply = true;
   end
   if ~canApply && ~isdeployed
      throw(ME)
   end
end % checkObject

end % methods (Access = private)
end % classdef

function [y_hat,D]= lxsg(y,width,order,deriv)

% LXSG Savitsky-Golay smoothing and differentiation.
%
%   Inputs are the matrix of ROW vectors to be smoothed (y),
%   and the optional variables specifying the number of points in
%   filter (width), the order of the polynomial (order), and the
%   derivative (deriv). The output is the matrix of smoothed
%   and differentiated ROW vectors (y_hat) and the matrix of
%   coefficients (cm) which can be used to create a new smoothed/
%   differentiated matrix, i.e. y_hat = y*cm. If number of points,
%   polynomial order and derivative are not specified,
%   they are set to 15, 2 and 0, respectively.
%
%   Example: if y is a 5 by 100 matrix then lxsg(y,11,3,1)
%   gives the 5 by 100 matrix of first-derivative row vectors
%   resulting from a 11-point cubic Savitzky-Golay smooth of
%   each row of y.
%
% I/O: [y_hat,cm] = lxsg(y,width,order,deriv);
%

% Sijmen de Jong Unilever Research Laboratorium Vlaardingen Feb 1993
% Modified 5/94
%         ***   Further modified, 1998-03, Martin Andersson
%         ***   Adjusting the calcn. of the bulk data.
%         ***   Based on calcn. of a sparse derivative matrix (D)

  %[m,n] = size(y);
  [~,n] = size(y); % CR, 29-09-2016 10:37:34

  y_hat = y;

% set default values: 15-point quadratic smooth
  if nargin<4
    deriv= 0;
    lxlogerror('  '), lxlogerror('Derivative set to zero')
  end

  if nargin<3
    order= 2;
    lxlogerror('  '), lxlogerror('Polynomial order set to 2')
  end

  if nargin<2
    width=min(15,floor(n/2));
    s = sprintf('Width set to %g',width);
    lxlogerror('  '), lxlogerror(s)
  end

% In case of input error(s) set to reasonable values
  w = max( 3, 1+2*round((width-1)/2) );
  if w ~= width
    s = sprintf('Width changed to %g',w);
    lxlogerror('  '), lxlogerror('Width must be >= 3 and odd'), lxlogerror(s)
  end

  o = min([max(0,round(order)),5,w-1]);
  if o ~= order
    s = sprintf('Order changed to %g',o); lxlogerror('  ')
    lxlogerror('Order must be <= width -1 and <= 5'), lxlogerror(s)
  end

  d = min(max(0,round(deriv)),o);
  if d ~= deriv
    s = sprintf('Derivative changed to %g',d); lxlogerror('  ')
    lxlogerror('Deriviative must be <= order'), lxlogerror(s)
  end

  p = (w-1)/2;
% Calculate design matrix and pseudo inverse
  x       = ((-p:p)'*ones(1,1+o)).^(ones(size(1:w))'*(0:o));
  weights = x\eye(w);

% Smoothing and derivative for bulk of the data
  coeff = prod(ones(d,1)*[1:o+1-d]+[0:d-1]'*ones(1,o+1-d,1),1);
  D     = spdiags(ones(n,1)*weights(d+1,:)*coeff(1),p:-1:-p,n,n);

% Smoothing and derivative for tails
  w1               = diag(coeff)*weights(d+1:o+1,:);
  D(1:w,1:p+1)     = [x(1:p+1,1:1+o-d)*w1]';
  D(n-w+1:n,n-p:n) = [x(p+1:w,1:1+o-d)*w1]';

% Operate on y using the filtering/derivative matrix, D
  y_hat = y*D;
end % lxsg