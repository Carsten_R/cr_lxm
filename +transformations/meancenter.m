classdef meancenter < handle
%MEANCENTER Apply mean centering
%   Perform mean centering of data over the first array dimension.
%
%   Usage:
%           mcObj = meancenter;
%           mcx   = mcObj.calculateTransform(data);
%           mcx   = mcObj.applyTransform(data);
%           mct   = mcObj.getTransformationInformation;
%
% Henrik Toft, 14-Jul-2015
% Revision: Henrik Toft, 17-Jun-2016
% Revision: Carsten Ridder, 26-09-2016

%% Properties section
properties (Constant)
   funcname  = 'meancenter';   % Transformation method file funcname
   menuname  = 'Mean &center'; % Descriptive funcname  (�ndret til Mean center, 26-09-2016 13:23:34)

   parmprefix = ''             % No parameters

   helptext     = char('Mean centering subtracts the coloumn mean from each element in the coloumn.', ...
                       'This corresponds to moving the origo of the coordinate system to the center of the data', ...
                       '... can be continued ...');
   allowRepeated = false;           % You can not have more than one occurence in a transform-sequence
   exclusive = true;                % Allow only one transformation tagged as "exclusive" in a transform-sequence
                                    % For now these are exclusive: 'Mean center', 'Autoscale' and 'Pareto scaling'
end

properties (Access = private)
   showname   % is set in "get_parm"
   meanVector % Saves mean vector for use with prediction
end

%% Methods section
methods

function obj = meancenter
% Meancenter object creator function
   if ~isequal(nargin,0)
      ME = MException('meancenter:noInputRequired', ...
                      'No input variables should be specified!');
      throw(ME);
   end
end

function mcx = calculateTransform(obj,data)
% Mean center input data using data mean:
%   mcx = mcObj.calculateTransform(data);
   mx  = mean(data,1,'omitnan'); % CR, 26-09-2016 11:55:51
   obj.meanVector = mx;
   mcx = bsxfun(@minus,data,obj.meanVector);
end

function mcx = applyTransform(obj,data)
% Mean center input data using previously calculated mean:
%   mcx = mcObj.applyTransform(data);
   canApply = checkObject(obj,data);
   if canApply
      mcx = bsxfun(@minus,data,obj.meanVector);
   else
      mcx = [];
   end
end

function mcx = inverseTransform(obj,data)
% "lxrescale": data+mx;
% CR, dsk.2016 03-11-2016 14:35:25

   canApply = checkObject(obj,data);
   if canApply
      mcx  = bsxfun(@plus,data,obj.meanVector);
   else
      mcx = [];
   end
end


function trans = getTransformationInformation(obj)
% Return transformation vectors
%   mct = mcObj.getTransformationInformation;
   trans.funcname = obj.funcname;
   trans.menuname = obj.menuname;
   trans.showname = obj.showname;
   trans.meanVector = obj.meanVector;
end

function get_parm(obj)
  obj.showname = removeampersand(obj.menuname);
end


function set_parm(obj, ~)
  obj.showname = obj.menuname;
end

end % methods

methods (Access = private)

function canApply = checkObject(obj,data)
   if isempty(obj.meanVector)
      canApply = false;
      ME = MException('meancenter:applyTransform:cannotApplyTransform', ...
                      'Transformation has not previously been calculated!');
   elseif ~isequal(size(data,2),length(obj.meanVector))
      canApply = false;
      ME = MException('meancenter:applyTransform:incorrectMeanVectorLength', ...
                      'Mean vector length (%d) does not match number of columns in data (%d)!',length(obj.meanVector),size(data,2));
   else
      canApply = true;
   end
   if ~canApply && ~isdeployed
      throw(ME)
   end
end

end % methods (Access = private)

end % classdef
