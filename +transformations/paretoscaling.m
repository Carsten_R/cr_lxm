classdef paretoscaling < handle
%PARETOSCALING Apply pareto scaling
%   Perform  pareto scaling of data over the first array dimension.
%
%   Usage:
%           psObj = paretoscaling;
%           psx   = psObj.calculateTransform(data);
%           psx   = psObj.applyTransform(data);
%           pst   = asObj.getTransformationInformation;
%
% Henrik Toft, 15-Jul-2015
% Revision: Henrik Toft, 17-Jun-2016

%% Proterties section
properties (Constant)
   funcname         = 'paretoscaling';  % Transformation method file funcname
   menuname  = '&Pareto scaling'; % Descriptive funcname

   parmprefix = ''    % No parameters

   helptext     = char('Pareto scaling subtracts from each element in a coloumn the coloumn mean and divide by the square root of the coloumn standard deviation.', ...
                       '... can be continued ...');
   allowRepeated = false;           % You can not have more than one occurence in a transform-sequence
   exclusive = true;                % Allow only one transformation tagged as "exclusive" in a transform-sequence
                                    % For now these are exclusive: 'Mean center', 'Autoscale' and 'Pareto scaling'

end

properties (Access = private)
   showname % is set in "get_parm"
   meanVector                       % Saves mean vector for use with prediction
   stdVector                        % Saves std vector for use with prediction
end

%% Methods section
methods
function obj = paretoscaling
   % Pareto scaling object creator function
   if ~isequal(nargin,0)
      ME = MException('paretoscaling:noInputRequired', ...
                      'No input variables should be specified!');
      throw(ME);
   end
end

function asx = calculateTransform(obj,data)
   % Pareto scale input data using data mean and std:
   %   psx = psObj.calculateTransform(data);
   if size(data,1) == 1
      mx = data;
     %sx = zeros(1,size(data,2));
      sx = ones(1,size(data,2)); % !! CR, 26-09-2016 12:02:07
   else
     %mx = utilities.nanmean(data);        % B�r omskrives s� "dim" kommer med ligesom i "mean"
     %sx = sqrt(utilities.nanstd(data));   % B�r omskrives s� "dim" kommer med ligesom i "std"
      mx = mean(data,1,'omitnan');        % CR, 26-09-2016 12:01:46
      sx = sqrt(std(data,0,1,'omitnan')); % CR, 26-09-2016 12:01:49
   end
   obj.meanVector = mx;
   obj.stdVector  = sx;
   asx = bsxfun(@rdivide,bsxfun(@minus,data,obj.meanVector),obj.stdVector);
end

function psx = applyTransform(obj,data)
   % Pareto scale input data using previously calculated mean and std:
   %   psx = psObj.applyTransform(data);
   canApply = checkObject(obj,data);
   if canApply
      psx = bsxfun(@rdivide,bsxfun(@minus,data,obj.meanVector),sqrt(obj.stdVector));
   else
      psx = [];
   end
end

function asx = inverseTransform(obj,data)
% "lxrescale": data*sx+mx;
% CR, dsk.2016, Middelfart, 03-11-2016 14:39:41

   canApply = checkObject(obj,data);
   if canApply
      asx  = bsxfun(@plus,bsxfun(@times,data,obj.stdVector),obj.meanVector);
   else
      asx = [];
   end
end


function trans = getTransformationInformation(obj)
   % Return transformation vectors
   %   ast = asObj.getTransformationInformation;
   trans.funcname = obj.funcname;
   trans.menuname = obj.menuname;
   trans.showname = obj.showname;
   trans.meanVector = obj.meanVector;
   trans.stdVector = obj.stdVector;
end

function get_parm(obj)
  obj.showname = removeampersand(obj.menuname);
end


function set_parm(obj, ~)
  obj.showname = obj.menuname;
end

end

methods (Access = private)
function canApply = checkObject(obj,data)
   if isempty(obj.meanVector)
      canApply = false;
      ME = MException('paretoscaling:applyTransform:missingMeanVector', ...
                      'No mean vector exist!');
   elseif isempty(obj.stdVector)
      canApply = false;
      ME = MException('paretoscaling:applyTransform:missingStdVector', ...
                      'No std vector exist!');
   elseif ~isequal(size(data,2),length(obj.meanVector))
      canApply = false;
      ME = MException('paretoscaling:applyTransform:incorrectMeanVectorLength', ...
                      'Mean vector length (%d) does not match number of columns data (%d)!',length(obj.meanVector),size(data,2));
   elseif ~isequal(size(data,2),length(obj.stdVector))
      canApply = false;
      ME = MException('paretoscaling:applyTransform:incorrectStdVectorLength', ...
                      'Std vector length (%d) does not match number of columns data (%d)!',length(obj.stdVector),size(data,2));
   else
      canApply = true;
   end
   if ~canApply && ~isdeployed
      throw(ME)
   end
end
end
end