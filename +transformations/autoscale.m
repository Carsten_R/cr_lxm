classdef autoscale < handle
%AUTOSCALE Apply autoscaling
%   Perform autoscaling of data over the first array dimension.
%
%   Usage:
%           asObj = autoscale;
%           asx   = asObj.calculateTransform(data);
%           asx   = asObj.applyTransform(data);
%           ast   = asObj.getTransformationInformation;
%
% Henrik Toft, 15-Jul-2015
% Revision: Henrik Toft, 17-Jun-2016
% Revision: Carsten Ridder, 26-09-2016

%% Properties section
properties (Constant)
   funcname  = 'autoscale'; % Transformation method file funcname
   menuname  = '&Autoscale'; % Descriptive funcname

   parmprefix = ''    % No parameters

   allowRepeated = false;           % You can not have more than one occurence in a transform-sequence
   exclusive = true;                % Allow only one transformation tagged as "exclusive" in a transform-sequence
                                    % For now these are exclusive: 'Mean center', 'Autoscale' and 'Pareto scaling'

   helptext     = char('Autoscaling subtracts from each element in a coloumn the coloumn mean and divide by the coloumn standard deviation.')

end

properties (Access = private)
  showname % is set in "get_parm"
  meanVector                  % Saves mean vector for use with prediction
  stdVector                   % Saves std vector for use with prediction
end

%% Methods section
methods
function obj = autoscale
       % Autoscale object creator function
       if ~isequal(nargin,0)
          ME = MException('autoscale:noInputRequired', ...
                          'No input variables should be specified!');
          throw(ME);
       end
end

function asx = calculateTransform(obj,data)
   % Autoscale input data using data mean and std:
   %   asx = asObj.calculateTransform(data);
   if size(data,1) == 1
      mx = data;
      sx = ones(1,size(data,2)); % !! CR, 26-09-2016 11:56:51
   else
      mx  = mean(data,1,'omitnan');     % CR, 26-09-2016 11:57:36
      sx  = std(data,0,1,'omitnan');    % CR, 26-09-2016 11:57:36
   end
   obj.meanVector = mx;
   obj.stdVector  = sx;
   asx = bsxfun(@rdivide,bsxfun(@minus,data,obj.meanVector),obj.stdVector);
end

function asx = applyTransform(obj,data)
   % Autoscale input data using previously calculated mean and std:
   %   asx = asObj.applyTransform(data);
   canApply = checkObject(obj,data);
   if canApply
      asx = bsxfun(@rdivide,bsxfun(@minus,data,obj.meanVector),obj.stdVector);
   else
      asx = [];
   end
end

function asx = inverseTransform(obj,data)
% "lxrescale": data*sx+mx;
% CR, 02-11-2016 14:55:41

   canApply = checkObject(obj,data);
   if canApply
      asx  = bsxfun(@plus,bsxfun(@times,data,obj.stdVector),obj.meanVector);
   else
      asx = [];
   end
end

function trans = getTransformationInformation(obj)
   % Return transformation vectors
   %   ast = asObj.getTransformationInformation;
   trans.funcname = obj.funcname;
   trans.menuname = obj.menuname;
   trans.showname = obj.showname;
   trans.meanVector = obj.meanVector;
   trans.stdVector = obj.stdVector;
end

function get_parm(obj)
  obj.showname = removeampersand(obj.menuname);
end


function set_parm(obj, ~)
  obj.showname = obj.menuname;
end

end

methods (Access = private)
function canApply = checkObject(obj,data)
   if isempty(obj.meanVector)
      canApply = false;
      ME = MException('autoscale:applyTransform:missingMeanVector', ...
                      'No mean vector exist!');
   elseif isempty(obj.stdVector)
      canApply = false;
      ME = MException('autoscale:applyTransform:missingStdVector', ...
                      'No std vector exist!');
   elseif ~isequal(size(data,2),length(obj.meanVector))
      canApply = false;
      ME = MException('autoscale:applyTransform:incorrectMeanVectorLength', ...
                      'Mean vector length (%d) does not match number of data columns (%d)!',length(obj.meanVector),size(data,2));
   elseif ~isequal(size(data,2),length(obj.stdVector))
      canApply = false;
      ME = MException('autoscale:applyTransform:incorrectStdVectorLength', ...
                      'Std vector length (%d) does not match number of data columns (%d)!',length(obj.stdVector),size(data,2));
   else
      canApply = true;
   end
   if ~canApply && ~isdeployed
      throw(ME)
   end
end
end
end
