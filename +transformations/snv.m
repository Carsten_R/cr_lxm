classdef snv < handle
%SNV Apply standard normal variate
%   Apply standard normal variate of data over the second array
%   dimension.
%
%   Usage:
%           snvObj = snv;
%           snvx   = snvObj.calculateTransform(data);
%           snvx   = snvObj.applyTransform(data);
%
% Henrik Toft, 15-Jul-2015

%% Properties section
properties (Constant)
   funcname  = 'snv';                            % Transformation method file funcname
   menuname  = 'Standard &Normal Variate (SNV)'; % Descriptive funcname

   parmprefix = '' % No parameters (i.e. not: 'SNV')

   helptext     = char('SNV does this: ...', ...
                       '... can be continued ...');
   allowRepeated = true;          % If false, you can not have more than one occurence in a transform-sequence
   exclusive = false;             % Allow only one transformation tagged as "exclusive" in a transform-sequence
                                  % For now these are exclusive: 'Mean center', 'Autoscale' and 'Pareto scaling'

end

properties (Access = private)
   showname % is set in "get_parm"
end

%% Methods section
methods
function obj = snv
   % Standard normal variate object creator function
   if ~isequal(nargin,0)
      ME = MException('snv:noInputRequired', ...
                      'No input variables should be specified!');
      throw(ME);
   end
end

function snvx = calculateTransform(obj,data) %#ok<INUSL>
   % Standard normal variate input data using data mean and std.
   if size(data,2) == 1
      mx = data;
     %sx = zeros(size(data));
      sx = ones(1,size(data,2)); % !! CR, 26-09-2016 12:00:27
   else
     %mx = transpose(utilities.nanmean(data'));  % B�r omskrives s� "dim" kommer med ligesom i "mean"
     %sx = transpose(utilities.nanstd(data'));   % B�r omskrives s� "dim" kommer med ligesom i "std"
      mx  = mean(data,2,'omitnan');  % CR, 26-09-2016 11:59:33
      sx  = std(data,0,2,'omitnan'); % CR, 26-09-2016 11:59:36

   end
   snvx = bsxfun(@rdivide,bsxfun(@minus,data,mx),sx);
end

function snvx = applyTransform(obj,data)
   % Standard normal variate input data using previously calculated mean and std.
   canApply = checkObject(obj,data);
   if canApply
       snvx = calculateTransform(obj,data);
   else
      snvx = [];
   end
end

function data = inverseTransform(obj,data)
% CR, dsk.2016, Middelfart, 03-11-2016 14:39:41
    ME = MException('snv:inverseTransform:cannotInverseTransform', ...
                      'This transformation can not be inversed!');
    throw(ME);
end


function trans = getTransformationInformation(obj)
   trans.funcname = obj.funcname;
   trans.menuname = obj.menuname;
   trans.showname = obj.showname;
end

function get_parm(obj)
  obj.showname = removeampersand(obj.menuname);
end

function set_parm(obj, ~)
  obj.showname = 'SNV';
end

end

methods (Access = private)
function canApply = checkObject(~,~)
   canApply = true;
end
end

end