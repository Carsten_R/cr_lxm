classdef msc < handle
%MSC Apply multiplicativ signal correction
%   Apply multiplicativ signal correction of data over the first array
%   dimension.
%
%   Usage:
%           mscObj = msc;
%           mscx   = mscObj.calculateTransform(data);
%           mscx   = mscObj.applyTransform(data);
%           msct   = asObj.getTransformationInformation;
%
% Henrik Toft, 15-Jul-2015
% Revision: Henrik Toft, 15-Jun-2016

%% Proterties section
properties (Constant)
  funcname         = 'msc';                              % Transformation method file funcname
  menuname  = '&Multiplicative Signal Correction (MSC) ...';  % Descriptive funcname

  parmprefix = 'MSC'

  helptext     = char('MSC does this: ...', ...
                      '... can be continued ...');
  allowRepeated = true;          % You can not have more than one occurence in a transform-sequence
  exclusive = false;             % Allow only one transformation tagged as "exclusive" in a transform-sequence
                                 % For now these are exclusive: 'Mean center', 'Autoscale' and 'Pareto scaling'

end

properties (Access = private)
  showname % is set in "get_parm"

  baseVector                                         % Saves base (index) vector for use with prediction
  meanVector                                         % Saves mean vector for use with prediction
%       abVector                                           %
end

%% Methods section
methods
function obj = msc
   % Multiplicative Signal Correction object creator function
   if ~isequal(nargin,0)
      ME = MException('msc:noInputRequired', ...
                      'No input variables should be specified!');
      throw(ME);
   end
end

function mscx = calculateTransform(obj,data,baseVector)
   % Calculated multiplicative signal correctied data:
   %   mscx = mscObj.calculateTransform(data);
   if isempty(baseVector)
      baseVector = 1:size(data,2);
   end
  %mx = utilities.nanmean(data(:,baseVector)); % B�r omskrives s� "dim" kommer med ligesom i "mean"
   mx  = mean(data(:,baseVector),1,'omitnan'); % CR, 26-09-2016 12:03:35
   obj.baseVector = baseVector;
   obj.meanVector = mx;
   ab = bsxfun(@mrdivide,data(:,obj.baseVector),vertcat(ones(1,length(obj.meanVector)),obj.meanVector));
   mscx = bsxfun(@rdivide,data-ab(:,1)*ones(1,size(data,2)),(ab(:,2)*ones(1,size(data,2))));
end

function mscx = applyTransform(obj,data)
   % Apply multiplicative signal correction to data using previously
   % calculated parameters:
   %   mscx = mscObj.applyTransform(data);
   canApply = checkObject(obj,data);
   if canApply
      ab = bsxfun(@mrdivide,data(:,obj.baseVector),vertcat(ones(1,length(obj.meanVector)),obj.meanVector));
      mscx = bsxfun(@rdivide,data-ab(:,1)*ones(1,size(data,2)),(ab(:,2)*ones(1,size(data,2))));
   else
      mscx = [];
   end
end

function data = inverseTransform(obj,data)
% CR, dsk.2016, Middelfart, 03-11-2016 14:39:41
    ME = MException('msc:inverseTransform:cannotInverseTransform', ...
                      'This transformation can not be inversed!');
    throw(ME);
end

function trans = getTransformationInformation(obj)
   % Return transformation vectors
   %   msct = mscObj.getTransformationInformation;
   trans.funcname = obj.funcname;
   trans.menuname = obj.menuname;
   trans.showname = obj.showname;
   trans.baseVector = obj.baseVector;
   trans.meanVector = obj.meanVector;
end

function get_parm(obj)
  obj.showname = removeampersand(obj.menuname);
end

function set_parm(obj, txt)
  obj.showname = 'MSC';
end

end

methods (Access = private)
function canApply = checkObject(obj,data)
   if isempty(obj.meanVector) || isempty(obj.baseVector)
      canApply = false;
      ME = MException('msc:applyTransform:cannotApplyTransform', ...
                      'Transformation has not previously been calculated!');
   elseif ~isequal(size(data,2),length(obj.meanVector))
      canApply = false;
      ME = MException('meancenter:applyTransform:incorrectMeanVectorLength', ...
                      'Mean vector length (%d) does not match number of columns in data (%d)!',length(obj.meanVector),size(data,2));
   else
      canApply = true;
   end
   if ~canApply && ~isdeployed
      throw(ME)
   end
end
end
end
