function LatentModel = lxCalcModel(data)
% SKAL NOK SLET IKKE BRUGES ALLIGEVEL!! (CR, 28-06-2017 16:39:22)

% LXCALCMODEL Create an latent model object (PCA, PLS, ...)
%
%  LatentModel = lxCalcModel(data)
%
%     data    either an X-matrix or
%             a structure with fields X, Y, Xt and Yt. All except data.X can be empty
%
%
% CR, 01-11-2016 09:13:33

% Eksempel:
%
% P = load('fisheriris');
% data.X = P.meas; %%%% rwmat{1} = X; rwmat{2} = []; rwmat{3} = []; rwmat{4} = [];
% data.ModelType = 'PCA';
% pcaobj = lxCalcModel(data)
% pcaobj.lxt(1).selectedTransform = pcaobj.lxt(1).transformDescriptions([2])
% pcaobj.calibrate


%  rwmat        % The four currently selected raw and un-transformed matrices from lx.data
%               %
%               %  X = rwmat{1}
%               %  Y = rwmat{2}
%               % Xt = rwmat{3}
%               % Yt = rwmat{4}
% e.g.
% rwmat{1} = rand(16,5); rwmat{2} = []; rwmat{3} = rand(7,5); rwmat{4} = [];
% pcaobj = lxCalcModel(rwmat, 'PCA')

  if isstruct(data)
  %
  else % indata is an X-matrix
    X  = data;
    clear data
    data.X  = X;
    data.Y  = [];
    data.Xt = [];
    data.Yt = [];

    data.Transform_shownames_X = {'Autoscale'};
    data.ModelType = 'PCA';

  end

%    data.X  = rwmat{1}
%    data.Y  = rwmat{2}
%    data.Xt = rwmat{3}
%    data.Yt = rwmat{4}


% Now "data" is a struct with fields X, Y, Xt and Yt. All except X can be empty

  switch data.ModelType
      case 'PCA',
           LatentModel = lxmodels.lxPCA(data);
      case 'PLS',
           LatentModel = lxmodels.lxPLS(data);
  end
