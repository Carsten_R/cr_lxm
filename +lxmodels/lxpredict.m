function Pred = lxpredict(Matrices, Model, lxt_Y); % oop, CR, 27-09-2016 14:30:50

% Y-predictions in original units (08-07-2004 13:20)
%
% CR, 07-07-2004

  switch Model.Type
    case 'PCA',  Pred = lxpcapred(Matrices{1}, Model);

    case 'PLS',  Pred = lxplspred(Matrices{1}, Model);
                 for a = 1:size(Pred.Ypred,3);
                   Pred.Ypred(:,:,a) = lxrescale(Pred.Ypred(:,:,a), lxt_Y); % oop TransData{2});
                 end

    case 'PCR',  % Model = lxpcr(Matrices{1}, Model);

    case 'SIMCA', % ???

  otherwise
    error('Ny kalibreringsmetode indf�rt. Tilret koden i LXPREDICT.M')
  end % switch CalMethod



