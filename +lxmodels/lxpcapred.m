function Pred = lxpcapred(X,PCAmodel);
% CR, 08-07-2004 17:57
% CR, 16-09-2004 13:46


%  PCAmodel =
%
%         Type: 'PCA'
%            T: [10x10 double]
%            P: [100x10 double]
%      eigvals: [10x1 double]
%       PctDes: [10x1 double]
%       PctVar: [10x1 double]
%

  NumPC  = size(PCAmodel.P,2);

  T = X*PCAmodel.P;  % projecting new objects on the model (P)
  ssX0 = sum(sum(X.^2)); % CR, 11-08-2005 11:24
  for a = 1:NumPC
    Xres       = X-T(:,1:a)*PCAmodel.P(:,1:a)'; % residual after a factors
    resXi(:,a) = sum(Xres.^2,2); % erstatter: "sum((Xres').^2)';"
    resXj(a,:) = sum(Xres.^2,1); % erstatter: "sum((Xres).^2);"   som er forkert ved vektor-input (CR, 13-09-2006 11:23)
    ssX(a)     = sum(sum(Xres.^2));
  end

  Si = sqrt(resXi);
  Sj = sqrt(resXj);

% Calculating Hi (leverage)
  Hi = lxscale(T.^2,zeros(1,NumPC),diag(PCAmodel.T'*PCAmodel.T)');
  if a>1
    Hi = cumsum(Hi')';
  end
  Hi = Hi + 1/size(PCAmodel.T,1);  % plus 1/I

% Calculating Hotellings T^2 (proportional to Hi-leverage)
  T2 = T.^2*inv(diag(PCAmodel.eigvals(1:NumPC)));
  if NumPC > 1
    T2 = cumsum(T2')';
  end
% Hi = T2; % NOTE: Hotellings T^2 is used for Hi (CR, 09-05-2005 09:47)

  Pred.Si = Si;
  Pred.Sj = Sj;
  Pred.Hi = Hi;

  Pred.T2    = T2;
  Pred.resXi = resXi;
  Pred.resXj = resXj;

  Pred.T = T;

  Pred.ssX0 = ssX0; % CR, 11-08-2005 16:14
  Pred.ssX  = ssX;  %        "

return % LN + CR, 24-10-2006 16:07

% Tilf�jet (CR, 13-09-2006 13:54)
  CalObjs = size(PCAmodel.T,1);
  CalVars = size(PCAmodel.P,1);

  for a = 1:NumPC
    dftaeller = (CalVars-a);
    taeller   = Pred.resXi(:,a)/dftaeller;

    dfnaevner = (CalVars-a)*(CalObjs-a-1);
    naevner   = PCAmodel.ssX(a)/dfnaevner;

    F = taeller/naevner;

    Pred.Ftest(:,a) = lxlimits('Fdist',F,dftaeller,dfnaevner); % The higher value, the more and outlier
  end

