classdef lxpls < lxmodels.lxModel

% 08-08-2017 10:53:37

methods

% constructor
function lxm = lxpls(DTM, calpred)

  lxm.CalMethod = 'PLS';
  lxm.UseY      = true;

  %%% if nargin == 0
  %%%   return
  %%% end
  %%%
  %%% switch calpred
  %%%   case 'calibrate', % Initialize and calculate the model. X- and Y-data are already transformed
  %%%                     lxm.ModelData = struct; % Skal evt. specificeres? (08-08-2017 10:53:28)
  %%%
  %%%                     if isfield(DTM,'X') & isfield(DTM,'Y')
  %%%                       lxm.X = DTM.X;
  %%%                       lxm.Y = DTM.Y; % 08-08-2017 10:57:25
  %%%                     else
  %%%                       lxm.X = DTM.trmat{1};
  %%%                       lxm.Y = DTM.trmat{2}; % 08-08-2017 10:54:49
  %%%                     end
  %%%                     lxm.NumPC = DTM.MaxPC;
  %%%
  %%%                     calibrate(lxm);
  %%%
  %%%                     lxm.lxt = DTM.lxt;
  %%%
  %%%   case 'predict',   123
  %%%                     lxm.ModelData = DTM.Model.ModelData;
  %%%                     predict(lxm,DTM.Xpred);
  %%%                     lxm.lxt = DTM.lxt;
  %%%
  %%%
  %%% end % switch

end % constructor



function calibrate(lxm, X, Y)
% Calibrate using PLS

% function PLSmodel = lxpls(X,Y,NumPC)

%  PLSmodel = lxpls(X,Y,NumPC);
%
%  This function calculates the PLS model vectors and matrices
%  P, Q, W, T, U and b for the given X and Y matrices and the number of
%  factors (NumPC) specified.
%
%  X and Y shall be centered (and scaled) before calling lxm function.
%
%     ssX      the square sum of all elements in X after each factor
%     StdXres  the standard deviation over the objects of each variable
%              in X after each factor
%
%  Carsten Ridder, Sept. 1997

  if nargin == 1 % Prepare for cross-validation (09-08-2017 11:19:21)
    X = lxm.trmat{1};
    Y = lxm.trmat{2};
  else
    X = lxm.lxt(1).calculateTransform(X); % Transform the raw input X-matrix
    Y = lxm.lxt(2).calculateTransform(Y); % Transform the raw input Y-matrix
  end

  NumPC = lxm.ModelSettings.MaxPC;

%  X     = lxm.X; % 08-08-2017 10:48:39
%  Y     = lxm.Y;
%  NumPC = lxm.NumPC;

  [I,J] = size(X);
  [I,K] = size(Y);

  NumPC = min(NumPC,J);
  NumPC = min(NumPC,I);

  P = zeros(J,NumPC);
  Q = zeros(K,NumPC);
  W = zeros(J,NumPC);
  T = zeros(I,NumPC);
  U = zeros(I,NumPC);
  b = zeros(1,NumPC);

  ssX0 = sum(sum(X.^2)); % CR, 11-08-2005 11:24
  ssY0 = sum(sum(Y.^2)); % CR, 11-08-2005 16:17

  PLS2_converged_global = true; % CR, 12-06-2006 08:25

  for a=1:NumPC
    [p,q,w,t,u, PLS2_converged] = plsnipals(X,Y);  % PLS2_converged added (CR, 12-06-2006 08:23)

    if ~PLS2_converged
      PLS2_converged_global = false;
    end

    b(1,a) = u'*t/(t'*t);
    X = X - t*p';
    Y = Y - b(1,a)*t*q';

    T(:,a) = t(:,1);
    U(:,a) = u(:,1);
    P(:,a) = p(:,1);
    W(:,a) = w(:,1);
    Q(:,a) = q(:,1);

    ssX(a) = sum(sum(X.^2));
    ssY(a) = sum(sum(Y.^2));

    StdXres(a,:) = std(X);

    resXi(:,a) = sum((X').^2)';  % CR, 10-08-2005 09:08
    resXj(a,:) = sum((X).^2)';   % CR, 10-08-2005 09:08
  end

  Si = sqrt(resXi);
  Sj = sqrt(resXj);

  lxm.ModelData.Type    = 'PLS';
  lxm.ModelData.P       = P;
  lxm.ModelData.Q       = Q;
  lxm.ModelData.W       = W;
  lxm.ModelData.T       = T;
  lxm.ModelData.U       = U;
  lxm.ModelData.b       = b;

  lxm.ModelData.ssX0    = ssX0;
  lxm.ModelData.ssX     = ssX;

  lxm.ModelData.ssY0    = ssY0;
  lxm.ModelData.ssY     = ssY;

  lxm.ModelData.StdXres = StdXres;

% CR, 09-05-2005 11:47
  lxm.ModelData.eigvals = diag(lxm.ModelData.T'*lxm.ModelData.T)/(I-1);          % Correct (09-09-2005 10:59)
  lxm.ModelData.PctDes  = lxm.ModelData.eigvals*100./sum(lxm.ModelData.eigvals); % Wrong, as eigvals are truncated (09-09-2005 10:59)
  lxm.ModelData.PctVar  = cumsum(lxm.ModelData.PctDes);                          % Thus, wrong too!!

% Calculating Hi (leverage)
  Hi = utilities.lxscale(T.^2,zeros(1,NumPC),diag(T'*T)');
  if a > 1
    Hi = cumsum(Hi')';
  end
  Hi = 1/I + Hi;

% Calculating Hotellings T^2 (proportional to Hi-leverage)
  T2 = lxm.ModelData.T.^2*inv(diag(lxm.ModelData.eigvals(1:NumPC)));
  if NumPC > 1
    T2 = cumsum(T2')';
  end

  lxm.ModelData.Si = Si;
  lxm.ModelData.Sj = Sj;
  lxm.ModelData.Hi = Hi;

  lxm.ModelData.T2  = T2;
  lxm.ModelData.resXi = resXi;
  lxm.ModelData.resXj = resXj;

  Siy = Si; % Skal regnes rigtigt ud! (CR, 19-04-2005 15:24)
  Hiy = Hi; % Skal regnes rigtigt ud! (CR, 19-04-2005 15:24)

  lxm.ModelData.Siy = Siy;
  lxm.ModelData.Hiy = Hiy;

  lxm.ModelData.Bscaled = CalcRegrCoefficients(lxm.ModelData);
  lxm.ModelData.Converged = PLS2_converged_global; % CR, 12-06-2006 08:28

return % LN + CR, 24-10-2006 16:07

% Tilf�jet (CR, 13-09-2006 14:35)
  CalObjs = size(lxm.ModelData.T,1);
  CalVars = size(lxm.ModelData.P,1);

  for a = 1:NumPC
    dftaeller = (CalVars-a);
    taeller   = lxm.ModelData.resXi(:,a)/dftaeller;

    dfnaevner = (CalVars-a)*(CalObjs-a-1);
    naevner   = lxm.ModelData.ssX(a)/dfnaevner;

    F = taeller/naevner;

    lxm.ModelData.Ftest(:,a) = lxlimits('Fdist',F,dftaeller,dfnaevner); % The higher value, the more an outlier
  end

end % calibrate

function predict(lxm, X, Y)
% CR, 08-08-2017 11:03:26
%
% X is the transformed X-matrix
% Y (optional) is the raw, untransformed Y-matrix
%
%             CR, 16-02-2004 11:59
% Modified by LN, 11-06-2004
% Modified by CR, 09-05-2005 12:00
% Modified by CR, 11-08-2005 16:18
% Modified by CR, 13-09-2006 12:00  (F-test predictions)

  ModelData = lxm.ModelData; % 08-08-2017 11:01:31

  [I,J] = size(X);
      K = size(ModelData.Q,1);
  MaxPC = size(ModelData.W,2);
  NumPC = min(MaxPC, lxm.ModelSettings.MaxPC); % 15-08-2017 11:15:09

  T = nan(I,NumPC); % 15-08-2017 11:15:57

  ssX0 = sum(sum(X.^2));
  for a = 1:NumPC
    T(:,a) = X*ModelData.W(:,a);      % projecting new object(s) on the model (W)
    X = X - T(:,a)*ModelData.P(:,a)'; % Kan beregnes on-the-fly, n�r vi skal bruge den (15-08-2017 11:45:34)
    resXi(:,a)  = sum(X.^2,2);
    resXj(a,:)  = sum(X.^2,1);        % CR, 13-09-2006 11:54 (giver det mening ved pr�diktion?)
    ssX(a)      = sum(sum(X.^2));     % CR, 11-08-2005 12:37
  end
  Si = sqrt(resXi);
  Sj = sqrt(resXj);

% Calculating Hi (leverage)
  Hi = utilities.lxscale(T.^2,zeros(1,NumPC),diag(ModelData.T'*ModelData.T)');
  if NumPC > 1
    Hi = cumsum(Hi')';
  end
  Hi = Hi + 1/size(ModelData.T,1);  % plus 1/I

% Calculating Hotellings T^2 (proportional to Hi-leverage)
  T2 = T.^2*inv(diag(ModelData.eigvals(1:NumPC)));
  if NumPC > 1
    T2 = cumsum(T2')';
  end

% Calculating predictions
  Ypred = zeros(I,K,NumPC);
  for a = 1:NumPC
    Ypred(:,:,a) = ModelData.b(1,a)*T(:,a)*ModelData.Q(:,a)';
  end
  Ypred = cumsum(Ypred,3);     % sum up over the PC's

  for a = 1:size(Ypred,3);
    Ypred(:,:,a) = lxm.lxt(2).inverseTransform(Ypred(:,:,a)); % 15-08-2017 15:47:27
  % Det virker ikke!
  % sx i lxm.lxt(2) = 1 og ikke det den skal v�re!!?? (sluts nu, 15-08-2017 15:54:34)
%   lav fejl her
  end

  Siy = Si; % Skal regnes rigtigt ud! (CR, 19-04-2005 15:24)
  Hiy = Hi; % Skal regnes rigtigt ud! (CR, 19-04-2005 15:24)

%  lxm.PredData.Ypred = Ypred;
%  lxm.PredData.Si    = Si;
%  lxm.PredData.Sj    = Sj;
%  lxm.PredData.Hi    = Hi;
%
%  lxm.PredData.T2    = T2;
%  lxm.PredData.resXi   = resXi;
%  lxm.PredData.resXj   = resXj;
%
%  lxm.PredData.Siy   = Siy;
%  lxm.PredData.Hiy   = Hiy;
%
%  lxm.PredData.T     = T;
%
%  lxm.PredData.ssX0  = ssX0;
%  lxm.PredData.ssX   = ssX;

  lxm.ObjPredStats.Ypred = Ypred;
  lxm.ObjPredStats.T     = T;
  lxm.ObjPredStats.Hi    = Hi;
  lxm.ObjPredStats.T2    = T2;
  lxm.ObjPredStats.resXi = resXi;
  lxm.ObjPredStats.Si    = Si;
  lxm.ObjPredStats.Siy   = Siy;
  lxm.ObjPredStats.Hiy   = Hiy;

  lxm.VarPredStats.Sj    = Sj;
  lxm.VarPredStats.resXj = resXj;

  lxm.SumPredStats.ssX0  = ssX0;
  lxm.SumPredStats.ssX   = ssX;

  if nargin == 3 && ~isempty(Y) %%% isequal(size(Y),size(Ypred))
    % Calculate prediction statistics for Y (15-08-2017 14:02:42)
    lxm.SumPredStats.ypredstats = utilities.lxmeaspredstats(Y, Ypred);
  else
    lxm.SumPredStats.ypredstats = struct; % isempty(fieldnames(lxm.ObjPredStats.ypredstats)) == "true"
  end


% UDKOMMENTERET 15-08-2017 13:11:52
% return % LN + CR, 24-10-2006 16:07
% Tilf�jet (CR, 13-09-2006 13:54 + (igen) 15-08-2017 13:12:04)

  CalObjs = size(ModelData.T,1);
  CalVars = size(ModelData.P,1);

  for a = 1:NumPC
    dftaeller = (CalVars-a);
    taeller   = lxm.ObjPredStats.resXi(:,a)/dftaeller;

    dfnaevner = (CalVars-a)*(CalObjs-a-1);
    naevner   = ModelData.ssX(a)/dfnaevner;

    F = taeller/naevner;

    lxm.ObjPredStats.Ftest(:,a) = utilities.lxlimits('Fdist',F,dftaeller,dfnaevner); % The higher value, the more and outlier
  end


end % predict

end % methods
end % classdef


% -------------------------------------------------------------------
function B = CalcRegrCoefficients(ModelData);

  [J, NumPC] = size(ModelData.P);
  K = size(ModelData.Q,1);

  B = zeros(J+1,K,NumPC);
  for a = 1:NumPC
    B(1:end-1,:,a) = ModelData.W(:,1:a)*inv(ModelData.P(:,1:a)'*ModelData.W(:,1:a))*diag(ModelData.b(1:a))*ModelData.Q(:,1:a)';
  end

return

% CR, 06-10-2004 16:02
% y_hat = b0 + b1*x1 + b2*x2 + .... + bJ*xJ
%
% The predictions of X = [eye(J); zeros(1,J)] will be (the scaled) regression coefficients.
% Remember to subtract b0 from all bJ's and rescale to original Y-units

  [J, NumPC] = size(ModelData.P);
  K = size(ModelData.Q,1);

%  B = zeros(J+1,K,NumPC);
%  X = zeros(1,J);
%  lxm.PredData = lxplspred(X,ModelData);
%  for a = 1:NumPC
%    B(end,:) = lxm.PredData.Ypred(:,:,a);
%  end

  B = zeros(J+1,K,NumPC);
  X = [eye(J); zeros(1,J)];
  lxm.PredData = lxplspred(X,ModelData);
  for a = 1:NumPC
    B(:,:,a) = lxm.PredData.Ypred(:,:,a);
    B(1:end-1,:,a) = B(1:end-1,:,a) - repmat(B(end,:,a),J,1);
  end

% Skal lige checkes!!!
end % CalcRegrCoefficients

% -------------------------------------------------------------------
function [p,q,w,t,u, PLS2_converged] = plsnipals(x,y)
  %  This program does the nipals algorithm for PLS
  %  It is generally run as a subprogram of PLS, since it calculates
  %  only one latent variable. Format is:
  %  [p,q,w,t,u] = plsnipals(x,y)

  %  Copyright
  %  BMW, 1991

  PLS2_converged = true; % CR, 12-06-2006 08:22

  [my,ny] = size(y);
  if ny>1
    [val,idx]=max(std(y));
  else
    idx=1;
  end

  u = y(:,idx);
  conv = 1;
  told = x(:,1);
  count = 1.0;
  %  Specify the conversion tolerance
  while conv > .000001
    count = count + 1;
    w = (u'*x)';
    w = (w'/norm(w'))';
    t = x*w;
    if ny == 1
      q = 1;
      break
    end
    q = (t'*y)';
    q = (q'/norm(q'))';
    u = y*q;
    conv = norm(told - t);
    told = t;
    if count >= 100
      % lxlogerror('No convergence (PLS2)')
      PLS2_converged = false;
      break
    end
  end
  p = (t'*x/(t'*t))';

  %  p_norm=norm(p);
  %  t = t*p_norm;
  %  w = w*p_norm;
  %  p = p/p_norm;

end % plsnipals


