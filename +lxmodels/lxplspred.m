function Pred = lxplspred(X,Model,NumPC);

% function [Ypred,X] = lxplspred(X,Model,NumPC);
%
%
% CR, 16-02-2004 11:59
% Modified by LN, 11-06-2004
% Modified by CR, 09-05-2005 12:00
% Modified by CR, 11-08-2005 16:18
% Modified by CR, 13-09-2006 12:00  (F-test predictions)

  [I,J] = size(X);
  [NumYvars,nq] = size(Model.Q);
  [mw,MaxPC] = size(Model.W);

  if nargin == 3
    NumPC = min(NumPC, MaxPC); % Calculate and return predictions for NumPC only
  else
    NumPC = MaxPC;             % Calculate and return predictions for all PC's
  end

  ssX0 = sum(sum(X.^2)); % CR, 11-08-2005 12:37
  for a = 1:NumPC
    T(:,a) = X*Model.W(:,a);    % projecting new object(s) on the model (W)
    X = X - T(:,a)*Model.P(:,a)';
    resXi(:,a)  = sum(X.^2,2);  % erstatter "sum((X').^2)'" (gav ikke fejl, men p�nere) CR, 13-09-2006 11:59
    resXj(a,:)  = sum(X.^2,1);  % CR, 13-09-2006 11:54 (giver det mening ved pr�diktion?)
    ssX(a)      = sum(sum(X.^2)); % CR, 11-08-2005 12:37
  end
  Si = sqrt(resXi);
  Sj = sqrt(resXj);

% Calculating Hi (leverage)
  Hi = lxscale(T.^2,zeros(1,NumPC),diag(Model.T'*Model.T)');
  if a>1
    Hi = cumsum(Hi')';
  end
  Hi = Hi + 1/size(Model.T,1);  % plus 1/I

% Calculating Hotellings T^2 (proportional to Hi-leverage)
  T2 = T.^2*inv(diag(Model.eigvals(1:NumPC)));
  if NumPC > 1
    T2 = cumsum(T2')';
  end

% Calculating predictions
  Ypred = zeros(I,NumYvars,NumPC);
  for a = 1:NumPC
    Ypred(:,:,a) = Model.b(1,a)*T(:,a)*Model.Q(:,a)';
  end
  Ypred = cumsum(Ypred,3);     % sum up over the PC's

  if nargin == 3
    Ypred = Ypred(:,:,NumPC);  % Calculate and return predictions for NumPC only
  end

  Siy = Si; % Skal regnes rigtigt ud! (CR, 19-04-2005 15:24)
  Hiy = Hi; % Skal regnes rigtigt ud! (CR, 19-04-2005 15:24)

  Pred.Ypred = Ypred;

  Pred.Si    = Si;
  Pred.Sj    = Sj;
  Pred.Hi    = Hi;

  Pred.T2    = T2;
  Pred.resXi   = resXi;
  Pred.resXj   = resXj;

  Pred.Siy   = Siy;
  Pred.Hiy   = Hiy;

  Pred.T     = T;

  Pred.ssX0  = ssX0;
  Pred.ssX   = ssX;

return % LN + CR, 24-10-2006 16:07

% Tilf�jet (CR, 13-09-2006 13:54)
  CalObjs = size(Model.T,1);
  CalVars = size(Model.P,1);

  for a = 1:NumPC
    dftaeller = (CalVars-a);
    taeller   = Pred.resXi(:,a)/dftaeller;

    dfnaevner = (CalVars-a)*(CalObjs-a-1);
    naevner   = Model.ssX(a)/dfnaevner;

    F = taeller/naevner;

    Pred.Ftest(:,a) = lxlimits('Fdist',F,dftaeller,dfnaevner); % The higher value, the more and outlier
  end

  