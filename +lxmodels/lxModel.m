classdef lxModel < handle % & matlab.mixin.CustomDisplay
% lxModel Abstract class for creating latent models (PCA, PLS, ...)
%
%   lxModels properties:
%      Number                - Unique model number
%      Name                  - The name of the model
%      Comment               - A user-set model comment/note
%      DataSetName           - Name of dataset on which the model is based
%      CalMethod             - Model type: PCA or PLS. See lxm.ModelTypes for options.
%%%%      ValMethod             - Validation type. See lxm.ValidationTypes for options.
%      UseY                  - Does this model type use an Y-matrix?
%
%      MissStats             - structure containing information about missing data
%
%      rwmat                 - Content:   X = rwmat{1}
%                                         Y = rwmat{2} can be empty
%                                        Xt = rwmat{3} do.
%                                        Yt = rwmat{4} do.
%
%      trmat                 - The four basic rwmat-matrices in their transformed forms, i.e. ready for e.g. plots and modeling
%
%      ModelSettings         - Structure containing ValidationType, crossval-settings, etc.
%      ModelData             - Structure containing model matrices (P, T, eigvals, resXi, etc.). The specific content will depend on the CalMethod
%      SubModels             - Models for each individual crossvalidation segment
%
%      ObjPredStats          - A model dependent structure holding prediction statistics for objects
%      VarPredStats          - A model dependent structure holding prediction statistics for variables
%      SumPredStats          - A model dependent structure holding summary prediction statistics
%                              The <..>PredStats structures are calculated and filled in the "predict"-methods
%
%%%%      PredData              - Structure containing predictions (resXi, resXj, T, T2, ssX0, Si, Sj, Hi, etc.). The specific content will depend on the CalMethod.
%      PredVal               - Validation data
%
%      IndexIntoDataSet      - Corresponds to lx.IndexIntoDataSet [logicals with same size as original global dataset]
%      ModelTypes            - Available model types, e.g. ('PCA';'PLS'}, later: 'PCR';'MLR';'SIMCA','ASCA', ...
%      ValidationTypes       - Available validation types
%
%      lxt                   - lxTransform: Array of transformationsobjects; lxt(1): X, lxt(2): Y
%
%      output                - diverse validerings-output; skal lige have et bedre navn!? (09-08-2017 16:08:57)
%
%   lxModels methods:
%      calibrate             - Calibrate using PCA or PLS
%      predict               - Predict using PCA or PLS
%
% CR, 01-11-2016 11:48:35

% Branch "C:\LX3_model_code\" oprettet (CR, 17-01-2017 14:20:24)

properties % (GetAccess=public, SetAccess=protected) % (SetAccess = protected, GetAccess = protected)

% ==============================================================
%
  GeneralInfo = struct('guid','', ...             % A global unique id
                       'Number', [], ...          % A model has an unique number in LatentiX-context
                       'Name','', ...             % The name of the model
                       'DataSetName','', ...      % Name of dataset on which the model is based
                       'Comment',''),             % A user-set model comment/note


  CalMethod                      % PCA, PLS, ...
%%%  ValMethod
  UseY                           % Does this model type use an Y-matrix?

%  ModelInfo  = struct('CalMethod', '', ...        % PCA, PLS, ...
%                     'rwmat', cell(4,1), ...          % The rawdata. X = rwmat{1}, Y = rwmat{2}, Xt = rwmat{3}, Yt = rwmat{4}
%                     'trmat', cell(4,1));             % The four rwmat-matrices in their transformed forms





%NumPC Number of principal components to calculate
% NumPC

  rwmat                          % Content:   X = rwmat{1}
                                 %            Y = rwmat{2} can be empty
                                 %           Xt = rwmat{3} do.
                                 %           Yt = rwmat{4} do.

  trmat                          % The four basic rwmat-matrices in their transformed forms, i.e. ready for e.g. plots and modeling

  lxt = [lxTransform, lxTransform]; % X: lxt(1) and Y: lxt(2); array of "lxTransform"-objects (26-09-2016 11:18:56)

  output                         % - diverse validerings-output; skal lige have et bedre navn!? (09-08-2017 16:08:57)

%MissStats
  MissStats

  ModelData = struct             % Structure containing model matrices (P, T, eigvals, resXi, etc.). The specific content will depend on the CalMethod
  SubModels = struct             % Sub-models e.g. for each individual crossvalidation segment or for a test set

  ObjPredStats   = struct        % A model dependent structure holding prediction statistics for objects
  VarPredStats   = struct        % A model dependent structure holding prediction statistics for variables
  SumPredStats   = struct        % A model dependent structure holding summary prediction statistics

%%%  PredData  = struct             % Structure containing predictions (resXi, resXj, T, T2, ssX0, Si, Sj, Hi, etc.). The specific content will depend on the CalMethod.
  PredVal   = struct             % Validation data
  ModelSettings  = struct             % Structure containing ValidationType, crossval-settings, etc.
  IndexIntoDataSet = cell(4,2)   % Corresponds to lx.IndexIntoDataSet [logicals with same size as original global dataset]



  ModelTypes = {'PCA'; 'PLS'};   %  'PCR';'MLR';'SIMCA','ASCA', ...

  ValidationTypes = {'No validation'; ...
                     'CV: Full'; ...
                     'CV: Syst123'; ...
                     'CV: Syst111'; ...
                     'CV: Random'; ...
                     'CV: Random (repeated)'; ...
                     'CV: Sets'; ...
                     'CV: Variable ...'; ...
                     'Test set'};

end

methods (Abstract)

%  InitializePredData(lxm)    % Prepare vectors and matrices to save crossval-results etc.
%  validate(lxm)              % Calling calibrate and predict depending on validation method

   calibrate(lxm);            % Calibrate using PCA or PLS
   predict(lxm);              % Predict using PCA or PLS

end % methods (Abstract)

methods

function varargout = calc_model(lxm);
% 10-08-2017 11:42:52

  if isempty(lxm.trmat)
    calc_trmat(lxm)
  end

% Calculate the final model using the transformed data "trmat" (CR, 30-06-2017 11:38:55)
  calibrate(lxm) % 08-08-2017 15:11:49

% Prediction of the calibration set
  predict(lxm, lxm.trmat{1}, lxm.rwmat{2}); % 08-08-2017 15:44:45


  switch lxm.ModelSettings.Validation
    case 'No validation',    % Do nothing
    case 'Test set',                 varargout{1} = testval(lxm);
    case {'CV: Full',
          'CV: Syst123',
          'CV: Syst111',
          'CV: Random',
          'CV: Random (repeated)',
          'CV: Sets',
          'CV: Variable ...',
          'Category variable'},      lxm.crossvalidate

  otherwise
    disp('Kludder i navngivningen i lxModel')
  end % switch lxm.ModelSettings.Validation


end % calc_model

function crossvalidate(lxm)
% CR, 09-08-2017 14:25:59

  [I,J] = size(lxm.rwmat{1}); % number of objects and variables in selected Xcal-matrix
                              % (always matrix number one (1))
  K = size(lxm.rwmat{2},2);

  lxm.ModelSettings.MaxPC = max(lxm.ModelSettings.MaxPC,lxm.ModelSettings.GlobalMinPC);

% SegMat - Logical matrix corresponding to test and training set resp.

  switch lxm.ModelSettings.Validation
    case 'CV: Full',          CVon = true;  SegMat = lxgetsegs(I,I,0);
    case 'CV: Syst123',       CVon = true;  SegMat = lxgetsegs(I,lxm.ModelSettings.NumSegs,0);
    case 'CV: Syst111',       CVon = true;  SegMat = lxgetsegs(I,lxm.ModelSettings.NumSegs,1);
    case 'CV: Random',        CVon = true;  SegMat = lxgetsegs(I,lxm.ModelSettings.NumSegs,2);

    case 'CV: Random (repeated)',
                              NumRuns = varargin{6}; % Called from LXREPEATEDCV
                              CVon = true;  SegMat = lxgetsegs(I,lxm.ModelSettings.NumSegs,2); % v2.00, 07-01-2008 15:58

    case 'CV: Sets',          %CVon = true;
                              %SegMat = lxgetsegs(I,handles.CatVar.vector,2); % oop senere! 07-05-2015 14:06:42
                              %lxm.ModelSettings.NumSegs = size(SegMat,2);
                              %set(handles.etNumSegs,'String',int2str(lxm.ModelSettings.NumSegs));  % oop senere! 07-05-2015 14:06:42

    case 'CV: Variable ...',  %CVon = true;
                              %SegMat = lxgetsegs(I,handles.CatVar.vector,2); % oop senere! 07-05-2015 14:06:42
                              %lxm.ModelSettings.NumSegs = size(SegMat,2);
                              %set(handles.etNumSegs,'String',int2str(lxm.ModelSettings.NumSegs)); % oop senere! 07-05-2015 14:06:42

    case 'Category variable', CVon = true;  SegMat = [];  % kommer senere ...

  otherwise
    disp('fejl')
  end % switch lxm.ModelSettings.Validation

% Adjust the number of crossval-segments
  lxm.ModelSettings.NumSegs = min([I lxm.ModelSettings.NumSegs]); % The maximal number of crossval-segments

% Adjust the number of PC's
  FewestCalObjs = min(sum(~SegMat));
  lxm.ModelSettings.MaxPC   = min([FewestCalObjs J lxm.ModelSettings.MaxPC]); % The maximal number of components

% =============================================================================

  lxm.PredVal.ssX0 = 0;                % CR, 11-08-2005 16:22
  lxm.PredVal.ssX  = zeros(1,lxm.ModelSettings.MaxPC);   % CR, 11-08-2005 16:22
  lxm.PredVal.ssY0 = zeros(K+1,1);     % LN+CR
  lxm.PredVal.ssY  = zeros(K+1,lxm.ModelSettings.MaxPC); %

  Converged = size(SegMat,2); % Counting the number of segments where the PLS2-algorithm has converged (CR, 12-06-2006 08:52)

  [Xcal, Ycal] = deal([]);

  lxt_seg(1) = lxTransform;
  lxt_seg(1).selectedTransform = lxm.lxt(1).selectedTransform;
  % Den g�r ikke med SG .../savgol (CR, 30-09-2016 08:07:41)
  lxt_seg(2) = lxTransform;
  lxt_seg(2).selectedTransform = lxm.lxt(2).selectedTransform;

% Create a model object "SubModels" to hold segment-models (09-08-2017 15:04:01)
  eval(['lxm.SubModels = ',class(lxm),';']) % Flyttet uden for l�kken (10-08-2017 13:57:39)
  for seg = 1:size(SegMat,2)

    % CALIBRATION
      Xcal = lxm.rwmat{1}(~SegMat(:,seg),:);
      temp = lxt_seg(1).calculateTransform(Xcal);

        % Check for constant variables leading to e.g. zero standard deviation (CR, 09-08-2005 14:20)
          if isempty(find(isinf(temp))) & isempty(find(isnan(temp)))
            Xcal = temp;
          else
            if 1 % show_wait_bar
              disp(['WARNING: Invalid X-scaling in segment #',int2str(seg),'. Global scaling is used.'])
            end
            lxt_seg(1)= lxm.lxt(1); % EJ TESTET!! Men s� simpelt burde det v�re!? (CR, 28-09-2016 10:50:43)
            Xcal = lxt_seg(1).calculateTransform(Xcal);
          end

      if lxm.UseY
          Ycal = lxm.rwmat{2}(~SegMat(:,seg),:);
          temp = lxt_seg(2).calculateTransform(Ycal); % CR, 28-09-2016 10:21:42

            % Check for constant variables leading to e.g. zero standard deviation (CR, 09-08-2005 14:20)
              if isempty(find(isinf(temp))) & isempty(find(isnan(temp)))
                Ycal = temp;
              else
                if show_wait_bar
                  disp(['WARNING: Invalid Y-scaling in segment #',int2str(seg),'. Global scaling is used.'])
                end
                lxt_seg(2) = lxm.lxt(2); % EJ TESTET!!
                Ycal = lxt_seg(2).calculateTransform(Ycal); % CR, 28-09-2016 10:22:14
              end

        % For calculation of statistics for 0'th component (LN + CR aftale 8/7-04)
        % Eventuel median i stedet for mean
          mYcal = utilities.lxrescale(mean(Ycal), lxt_seg(2));
          Ypred0(SegMat(:,seg),:) = repmat(mYcal,size(find(SegMat(:,seg)))); % oop
      end % if lxm.UseY

      lxm.SubModels(seg).lxt = lxm.lxt;
      lxm.SubModels(seg).ModelSettings = lxm.ModelSettings;
      calibrate(lxm.SubModels(seg), Xcal, Ycal)

      if isfield(lxm.SubModels(seg),'Converged')
        Converged = Converged - lxm.SubModels(seg).Converged; % CR, 12-06-2006 08:51
      end

    % PREDICTION
         Xval = lxm.rwmat{1}(SegMat(:,seg),:);
         Xval = lxt_seg(1).applyTransform(Xval); % CR, 28-09-2016 10:42:19
      predict(lxm.SubModels(seg), Xval)

%     Predseg = lxpredict({Xval}, SubModels, lxt_seg(2)); % CR, 28-09-2016 11:40:52

      lxm.PredVal.ssX0 = lxm.PredVal.ssX0 + lxm.SubModels(seg).PredData.ssX0; % CR, 11-08-2005 16:26
      lxm.PredVal.ssX  = lxm.PredVal.ssX  + lxm.SubModels(seg).PredData.ssX;

      if lxm.UseY
          % Calculating ssY0 and ssY
            Ytemp = lxm.rwmat{2}(SegMat(:,seg),:);
            Ytemp = lxt_seg(2).applyTransform(Ytemp); % CR, 28-09-2016 10:42:45
            for k=1:K
              lxm.PredVal.ssY0(k) = lxm.PredVal.ssY0(k) + sum(sum(Ytemp(:,k).^2)); % LN+CR, 15-08-2005 11:24
            end
            lxm.PredVal.ssY0(K+1,:) = sum(lxm.PredVal.ssY0(1:K,:),1);
            for a=1:lxm.ModelSettings.MaxPC
              temp = lxm.SubModels(seg).PredData.Ypred(:,:,a);
              temp = Ytemp - lxt_seg(2).applyTransform(temp); % CR, 28-09-2016 10:43:29
              for k=1:K
                lxm.PredVal.ssY(k,a)  = lxm.PredVal.ssY(k,a) + sum(sum(temp(:,k).^2));
              end
            end
            lxm.PredVal.ssY(K+1,:) = sum(lxm.PredVal.ssY(1:K,:),1);

          % Inserting predictions at the proper row indices
            Ypred(SegMat(:,seg),:,1:lxm.ModelSettings.MaxPC) = lxm.SubModels(seg).PredData.Ypred(:,:,1:lxm.ModelSettings.MaxPC);

            lxm.PredVal.Siy(SegMat(:,seg),:)   = lxm.SubModels(seg).PredData.Siy(:,1:lxm.ModelSettings.MaxPC); % 19-04-2005 15:23
            lxm.PredVal.Hiy(SegMat(:,seg),:)   = lxm.SubModels(seg).PredData.Hiy(:,1:lxm.ModelSettings.MaxPC); % 19-04-2005 15:23
      end % if lxm.UseY

      lxm.PredVal.Si(SegMat(:,seg),:)    = lxm.SubModels(seg).PredData.Si(:,1:lxm.ModelSettings.MaxPC);
      lxm.PredVal.Hi(SegMat(:,seg),:)    = lxm.SubModels(seg).PredData.Hi(:,1:lxm.ModelSettings.MaxPC);
    % CR, 09-05-2005 11:19
      lxm.PredVal.resXi(SegMat(:,seg),:) = lxm.SubModels(seg).PredData.resXi(:,1:lxm.ModelSettings.MaxPC);
      lxm.PredVal.T2(SegMat(:,seg),:)    = lxm.SubModels(seg).PredData.T2(:,1:lxm.ModelSettings.MaxPC);

    % CR, 13-09-2006 14:51
    % lxm.PredVal.Ftest(SegMat(:,seg),:) = lxm.SubModels(seg).PredData.Ftest(:,1:lxm.ModelSettings.MaxPC);

      lxm.PredVal.T(SegMat(:,seg),:)     = lxm.SubModels(seg).PredData.T(:,1:lxm.ModelSettings.MaxPC);

  end % for seg = 1:size(SegMat,2)

  % Calculate the final model using the transformed data "trmat" (CR, 30-06-2017 11:38:55)
    calibrate(lxm) % 08-08-2017 15:11:49

  % Prediction of the calibration set
    predict(lxm, lxm.trmat{1}); % 08-08-2017 15:44:45

    PredCal = lxm.PredData;
    lxm.output = calc_statistics(PredCal, lxm.PredVal, lxm.UseY);



% =============================================================================
end % crossvalidate

function SubModel = testval(lxm, X, Y, tstobj)
% X and Y are raw, untransformed matrices; tstobj is a logical matrix of
% ones (test set objects) and zeros (training set objects)
%
% Tempor�r (?) metode, som skal blive til den endelige "testsetvalidate"-metode og som skal
% bruges af "crossvalidate"-metoden
%
% CR, 14-08-2017 15:00:01
  if nargin == 1
    X = [lxm.rwmat{1}; lxm.rwmat{3}];
    Y = [lxm.rwmat{2}; lxm.rwmat{4}];

    IX  = size(lxm.rwmat{1},1);
    IXt = size(lxm.rwmat{3},1);
    tstobj = logical([zeros(IX,1); ones(IXt,1)]);
  end

% SubModel = testval(lxm, X, Y, tstobj)

% initialization of a sub-model to hold all calculations
  eval(['SubModel = ',class(lxm),';'])
  SubModel.lxt = lxm.lxt;
  SubModel.ModelSettings = lxm.ModelSettings;

% transformation & calibration
  Xcal = SubModel.lxt(1).calculateTransform(X(~tstobj,:));
  Ycal = SubModel.lxt(2).calculateTransform(Y(~tstobj,:));
  calibrate(SubModel, Xcal, Ycal)

% transformation & prediction
  Xtst = SubModel.lxt(1).applyTransform(X(tstobj,:));
% Ytst = SubModel.lxt(2).applyTransform(Y(tstobj,:)); % Y skal IKKE skaleres! Derimod skal Ypred tilbagetransformeres!! (15-08-2017 15:30:14)
  predict(SubModel, Xtst, Y(tstobj,:))
% or:
%  predict(SubModel, Xtst)
%  SubModel.ObjPredStats.ypredstats = utilities.lxmeaspredstats(Ytst, SubModel.ObjPredStats.Ypred);

end % testval

function testsetvalidate(lxm, trmat)
% CR, 10-08-2017 14:42:43

% Taget fra lxcalculate (indtil videre)

% Predictions of zero'th component for the calibration set is set to
% the mean of the transformed data rescaled to original units.
% This is a scalar value, which is "repmat'ed" to the proper size
% depending on the validation method used.
%%% Pred0means = utilities.lxrescale(mean(lxm.trmat{2}), lxm.lxt(2)); % oop % Pred0means = lxrescale(mean(handles.WorkSet.Matrix(2).data),handles.WorkSet.Matrix(2).TransData);
% Tager lige nu (10-08-2017 15:01:45) bare mean af rwmat{2} ("lxrescale" er da en m�rkelig ting!?!?!)
  Pred0means = mean(lxm.rwmat{2});

  X  = twmat{1};
  Y  = twmat{2};
  Xt = twmat{3};
  Yt = twmat{4};


  Xval = trmat{3}; % Properly transformed Xt-matrix

  It = size(Xval,1);
  if It < 3
    warndlg('PLS test set validation requires at least 3 test objects','Too few objects!','modal')
    return
  end

  Ypred0 = repmat(Pred0means, It, 1);    % Adjusting size of Ypred0 to the test set

  predict(lxm, Xval)
  Pred   = lxpredict({Xval}, lxm.Model, lxm.lxt(2));
  Ypred  = Pred.Ypred;

  PredVal.ssX0 = Pred.ssX0; % CR, 11-08-2005 16:28
  PredVal.ssX  = Pred.ssX;

  PredVal.Si = Pred.Si;
  PredVal.Hi = Pred.Hi;
% CR, 09-05-2005 11:19
  PredVal.resXi = Pred.resXi;
  PredVal.T2  = Pred.T2;

% CR, 13-09-2006 14:55
% PredVal.Ftest = Pred.Ftest;

  PredVal.Siy = Pred.Siy;
  PredVal.Hiy = Pred.Hiy;

  PredVal.T  = Pred.T;


  PredVal.ssY0 = zeros(K+1,1);     % LN+CR,
  PredVal.ssY  = zeros(K+1,lxm.ModelSettings.MaxPC); %

% Calculating ssY0 and ssY

  Ytemp = trmat{4}; % oop % Ytemp = handles.WorkSet.Matrix(4).data; % Already transformed data
  for k=1:K
    PredVal.ssY0(k) = PredVal.ssY0(k) + sum(sum(Ytemp(:,k).^2)); % LN+CR, 15-08-2005 11:24
  end
  PredVal.ssY0(K+1,:) = sum(PredVal.ssY0(1:K,:),1);

  for a=1:lxm.ModelSettings.MaxPC
    temp = Pred.Ypred(:,:,a);
    temp = Ytemp - lxm.lxt(2).applyTransform(temp); % CR, 29-09-2016 20:02:13
    for k=1:K
      PredVal.ssY(k,a)  = PredVal.ssY(k,a) + sum(sum(temp(:,k).^2));
    end
  end

  PredVal.ssY(K+1,:) = sum(PredVal.ssY(1:K,:),1);

% Preparing the output
  Yval = lxm.rwmat{4}; % oop % Yval = handles.DataSet.data(handles.WorkSet.Matrix(4).Idx{1}, handles.WorkSet.Matrix(4).Idx{2});

  % -----------------------
  % Calculating statistics
  % -----------------------

  % VALIDATION STATISTICS
  % Inserting Ypred0 into Ypred at index #1 (CR, 03-08-2004 12:13)
  Ypred(:,:,2:end+1) = Ypred;
  Ypred(:,:,1)       = Ypred0;  % PC#0 is at the first index (cf. LXMEASPRED.M)
  for a = 1:lxm.ModelSettings.MaxPC+1
    valstats(a) = lxmeaspredstats(Yval, Ypred(:,:,a));
  end
  valstats(1).slope     = repmat(NaN,1,K); % CR, 12-04-2006 12:26 ----> repmat (CR, 28-04-2006 14:32)
  valstats(1).intercept = repmat(NaN,1,K); % Values for PC# 0 obscures the other PC's
  stats.val = valstats;

  % CALIBRATION STATISTICS
  % Get the un-transformed Ycal
  Ycal = lxm.rwmat{2}; % oop % Ycal = handles.DataSet.data(handles.WorkSet.Matrix(2).Idx{1}, handles.WorkSet.Matrix(2).Idx{2});

  if strmatch(lxm.ModelSettings.Validation,'Test set')
    % The variable Ypred0 has the size of the test set, i.e.
    % have to "re-calculate Ypred0" to get the calibration error
      calstats(1) = lxmeaspredstats(Ycal, repmat(Pred0means,I,1));
  else
    % The variable Ypred0 has already the size of the calibration set
      calstats(1) = lxmeaspredstats(Ycal, Ypred0);
  end
  calstats(1).slope      = repmat(NaN,1,K); % CR, 12-04-2006 12:28
  calstats(1).intercept  = repmat(NaN,1,K); % Values for PC# 0 obscures the other PC's

  for a = 1:lxm.ModelSettings.MaxPC
    calstats(a+1) = lxmeaspredstats(Ycal, YpredCal(:,:,a));  % note the "a+1"
  end

  % oop: her kunne vi passende afskaffe denne udokumenterede metode! (CR, 08-05-2015 15:43:27)
  AngleON = 1;  % Tilf�j lxangle(lxm.Model.P,lxm.Model.W); til calstats (CR, 12-04-2006 12:58)
  if AngleON
    calstats(1).angPW = repmat(NaN,1,K);
    for a = 1:lxm.ModelSettings.MaxPC
      calstats(a+1).angPW = repmat(lxangle(lxm.Model.P(:,a),lxm.Model.W(:,a)),1,K);
    end
  end

  % Tilf�j (CR, 12-04-2006 13:30)
  BstatsON = 1; % Coded CR, 03-08-2006 12:58
  if ~isempty(lxm.Model.B) && BstatsON
      B = lxm.Model.B; % What about lxm.Model.Bscaled?
      calstats(1).B0      = my;      % intercept/offset
      calstats(1).Bsum    = my;      % sum of regression coefficients
      calstats(1).B2sum   = my.^2;   % sum of squared regression coefficients
     %calstats(1).Bmaxabs = abs(my); % highest absolute regression coefficient (sl�et fra CR, 15-08-2006 11:21)
      for a = 1:lxm.ModelSettings.MaxPC
        calstats(a+1).B0      = B(end,:,a);
        calstats(a+1).Bsum    = sum(B(1:end-1,:,a));
        calstats(a+1).B2sum   = sum((B(1:end-1,:,a)).^2);
       %calstats(a+1).Bmaxabs = max(abs(B(1:end-1,:,a)));
      end
  end % if BstatsON

  stats.cal = calstats;

  % Inserting Ypred0 into YpredCal at index #1 (CR, 03-08-2004 12:58)
  YpredCal(:,:,2:end+1) = YpredCal;
  if CVon
  % Adjusting the size of YpredCal. If CVon, then lxm.ModelSettings.MaxPC might have changed
    YpredCal = YpredCal(:,:,1:lxm.ModelSettings.MaxPC+1);
    YpredCal(:,:,1) = Ypred0;
  else
    YpredCal(:,:,1) = repmat(Pred0means,I,1);
  end

end % testsetvalidate

% ---------------------------------------
function [Xm, MissStats] = lxmisssvd(lxm, X, OptPC)
%function [Xm, MissStats] = lxmisssvd(lx, X, OptPC);
%
% oop (CR, 28-09-2016 11:25:01)
%
% CAAs comment: It handles missing values NaNs (very dispersed, less than 15%)
%
% OptPC as optional input ()


%  OPSKRIFT P� PRINCIP:
%
%  Fra: Lars N�rgaard [mailto:Lars.Noergaard@mli.kvl.dk]
%  Sendt: ma 27-02-2006 23:05
%  Til: Carsten Ridder
%  Cc: claus@andersson.dk
%  Emne: Missing
%
%
%  Hej Carsten,
%
%  Ja, som det fremgik af tidligere mail, mener jeg virkelig, at det er en revolution, du har lavet p� plotsiden.
%
%  Nu er der faktisk kun en st�rre ting tilbage, nemlig missing. Vedlagt et bud baseret p� Claus' m-fil fra 1995.
%  Claus, hvis du erindrer noget om problemer etc. med algoritmen m� du gerne sige til. Umiddelbart ser det fornuftigt ud.
%
%  Jeg forestiller mig f�lgende 'strategi':
%  PCA, No val, CV og Test Set: missing estimeres samlet for X inden beregning
%  PCA, Prediction: hvis missing i Prediction set, s� estimer for Cal+Pred set samlet inden Prediction
%
%  PLS, No val, CV og Test Set: missing estimeres samlet for X og samlet for Y (X og Y hver for sig) inden beregning.
%  Hvis Y er en vektor, estimeres ikke, men udelades i stedet.
%
%  PLS, Prediction: hvis missing i Prediction set for X, s� estimer samlet for X (Cal+Pred set) inden Prediction
%
%  Bem�rk at missing estimationen laves uafh�ngigt af og inden de klassiske ting som forbehandling etc.
%  Algoritmen er baseret p� auto-skalerede data, men det er udelukkende lokalt til estimationen;
%  resultatet er de r� uforbehandlede data.
%
%  Om metoden er god eller ej ved jeg ikke, men den er �n vej igennem, Det vigtigste er at missing h�ndteres,
%  s� brugeren ikke st�der p� alvorlige problemer; i princippet er det underordnet om der regnes fuldst�ndig
%  forkert bare missing h�ndteres! Den vil jeg ikke citeres for... og jeg mener det selvf�lgelig ikke!
%
%  Mange hilsner
%  Lars

disp('I lxm.lxmisssvd')

  Xm    = X;  % CR, 29-09-2016 16:17:17
  MissStats = []; % CR, 29-09-2016 16:17:17

  if ~any(isnan(X(:)) | isinf(X(:)))
    disp('LXMISSSVD: No missing data found!')
    return % CR, 29-09-2016 16:23:06
  end

  Xoriginal = X; % CR, 08-09-2006 12:16  ("original untransformed matrices")

  [I,J] = size(X);

  MaxPC = min(I,J);  % v2.00, 26-01-2008 12:19
  if nargin == 2 % oop % 1
    OptPC = MaxPC;
  end

  ConvLim = 1e-12;
% WarnLim = 1e-4;
  ConvLimMiss = 100*ConvLim;
  itmax = 100;

% 'prepro' is either auto, mean or none
% I have decided to use auto based on a very small test...

% prepro = 'Mean center';
  prepro = 'Autoscale';

% OPTIONS STATES 15% PERCENT MISSING IS A MAXIMUM
  lxt = lxTransform;
  lxt.selectedTransform = {prepro};
  X = lxt.calculateTransform(X); % CR, 28-09-2016 11:18:13


% Handles missing data
  X(find(isinf(X))) = NaN;  % CR, 01-03-2006 12:44
  MissIdx = find(isnan(X));
  [i,j] = find(isnan(X));

  MissStats.ijMatrix = [i, j];
  MissStats.rkMiss = zeros(I,1);
  for rk = unique(i)'
    MissStats.rkMiss(rk) = 100*length(find(isnan(X(rk,:))))/J;
  end

  MissStats.sjMiss = zeros(J,1);
  for sj = unique(j)'
    MissStats.sjMiss(sj) = 100*length(find(isnan(X(:,sj))))/I;
  end

  MissStats.PctMissing = 100*length(MissIdx)/numel(X);

  mnx = mean(X,1,'omitnan')/3; % CR, 02-11-2016 14:37:02
  mny = mean(X,2,'omitnan')/3; % CR, 02-11-2016 14:37:21

  n = size(i,1);
  for k = 1:n,
    i_ = i(k);
    j_ = j(k);
    X(i_,j_) = mny(i_) + mnx(j_);  % First estimate of missing value
  end;
  mnz = (nanmean(mnx)+nanmean(mny))/2;
  X(isnan(X)) = mnz;  % Never no nans left!? (26-01-2008 12:21)
  [U,S,V] = svd(X,'econ');

% Xm = U*S*V';
  Xm = U(:,1:OptPC)*S(1:OptPC,1:OptPC)*V(:,1:OptPC)'; % v2.00, 26-01-2008 12:26


  X(MissIdx) = Xm(MissIdx);
  ssmisold = sum(sum( Xm(MissIdx).^2 ));
  sstotold = sum(sum(X.^2 ));
  ssrealold = sstotold-ssmisold;
  iterate = 1;
  while iterate
    [U,S,V] = svd(X,'econ');

%   Xm = U*S*V';
    Xm = U(:,1:OptPC)*S(1:OptPC,1:OptPC)*V(:,1:OptPC)'; % v2.00, 26-01-2008 12:26

    X(MissIdx) = Xm(MissIdx);
    ssmis = sum(sum( Xm(MissIdx).^2 ));
    sstot = sum(sum( X.^2 ));
    ssreal = sstot-ssmis;
    if abs(ssreal-ssrealold)<ConvLim*ssrealold && abs(ssmis-ssmisold)<ConvLimMiss*ssmisold | iterate > itmax
       %iterate = 0;
       break
    end;
    ssrealold = ssreal;
    ssmisold = ssmis;
    iterate = iterate + 1;
  end

  if iterate > itmax
    uiwait(errordlg(strvcat('Estimation of missing values not possible',' ', ...
                           ['No convergence after ',int2str(itmax),' iterations']),'Check your data!','modal'))
%    disp('fjern igen!')
    [Xm, MissStats] = deal([]);
  else
   %Xm = lxrescale(Xm, TransData);
   %Xm = utilities.lxrescale(Xm, lxt); % CR, 28-09-2016 11:24:30
    Xm = lxt.inverseTransform(Xm); % CR, 02-11-2016 15:11:57 (G�LDER KUN FOR Autoscale lige nu!)
    MissStats.Estimates = sparse(I,J);
    MissStats.Estimates(MissIdx) = Xm(MissIdx);
    lxm.MissStats = MissStats; % oop, 02-11-2016 15:19:42

%  Skal de oprindelige ikke-NaN v�rdier i X ikke overf�res helt u�ndrede?? CR, 01-09-2006 13:07
%    X = lxrescale(X, TransData);
%    X(MissIdx) = Xm(MissIdx);
%
%  Indf�rer dette (08-09-2006 12:21):
    Xoriginal(MissIdx) = Xm(MissIdx);  % CR, 08-09-2006 12:21
    Xm = Xoriginal;                    % CR, 08-09-2006 12:21
  end
end % lxmisssvd


% ---------------------------
function calc_trmat(lxm)

%  ESTIMATE MISSING - START

  [I,J] = size(lxm.rwmat{1});             % number of objects and variables in selected Xcal-matrix

  X = [ [lxm.rwmat{1}, lxm.rwmat{2}];
        [lxm.rwmat{3}, lxm.rwmat{4}] ];

  %Tester  %X(1,2) = nan;  %X(8,3) = nan

  [X, lxm.MissStats] = lxmisssvd(lxm,X);
  if ~isempty(lxm.MissStats)
    % De enkelte tr-objs (meancenter, autoscale, osv.) SKAL v�re handle-underklasser!
    % Og lxTransform fungerer som b�de value og handle ...
    % CR, 29-09-2016 21:03:10

    % Replace raw data (lxm.rwmat) with estimated data (no missing)
    % note: some matrices can be empty of varying sizes, but the transform-methods works anyway.
      lxm.rwmat{1} = X(  1:I  ,   1:J);
      lxm.rwmat{2} = X(  1:I  , J+1:end);
      lxm.rwmat{3} = X(I+1:end,   1:J);
      lxm.rwmat{4} = X(I+1:end, J+1:end);

  end % if ~isempty(lxm.MissStats) % Flyttet herop, s� trmat altid beregnes her (og ikke som en metode i @lxdata!) 28-06-2017 16:50:09

%  ESTIMATE MISSING - END

  lxm.trmat{1} = lxm.lxt(1).calculateTransform(lxm.rwmat{1});
  lxm.trmat{2} = lxm.lxt(2).calculateTransform(lxm.rwmat{2});
  lxm.trmat{3} = lxm.lxt(1).applyTransform(lxm.rwmat{3});
  lxm.trmat{4} = lxm.lxt(2).applyTransform(lxm.rwmat{4});

%[PX, PY] = getTransOverview(lxm.lxt);

% Now this is true:
%    lxm.rwmat{m} (m = 1:4) is raw data without missing values
%    lxm.trmat{m} (m = 1:4) is properly transformed data without missing values

end % calc_trmat

end % methods

end % classdef

% ----------------------
function output = calc_statistics(PredCal, PredVal, UseY)
% 09-08-2017 15:52:35

  output.PredCal = PredCal;  % 31-03-2005 13:59
  output.PredVal = PredVal;  % 06-04-2005 16:03

% --------------------------
% CALIBRATION STATISTICS (SKAL OVERF�RES TIL lxModel!!!! 08-08-2017 14:58:46)
% (g�res i dag, 09-08-2017 15:54:19)
% --------------------------

% X-matrix
  output.resXcal    = [PredCal.ssX0, PredCal.ssX]'; % For 0:lxm.ModelSettings.MaxPC
  output.expvarXcal = 100*(1 - output.resXcal/output.resXcal(1));  % ssX0 is the first element   (% lxm.Model.PctDes = diff(expvarXcal);  % CR, 09-09-2005 10:35)

% Y-matrix
  % oop udkommenteret (11-05-2015 15:03:21)
  % if UseY
  %   resY    = [PredCal.ssY0(end,:), PredCal.ssY(end,:)];
  %   output.expvarYcal = 100*(1 - resY/resY(1));
  % else
  %   output.expvarYcal = NaN;
  % end

% Y-matrix
  if UseY
     K = sum(lxm.IndexIntoDataSet{2,2}); % oop % Number of variables in Ycal-matrix (always matrix number two (2))
     outdata = GetYstats(K, PredCal);
     output.resYcal    = outdata.resY;
     output.expvarYcal = outdata.expvarY;
  end

% --------------------------
% VALIDATION STATISTICS
% --------------------------
  output.resXval    = [PredVal.ssX0, PredVal.ssX]'; % For 0:lxm.ModelSettings.MaxPC
  output.expvarXval = 100*(1 - output.resXval/output.resXval(1));  % ssX0 is the first element

  if UseY
     outdata = GetYstats(K, PredVal);
     output.resYval    = outdata.resY;
     output.expvarYval = outdata.expvarY;

     output.MeasPred.Ycal     = lxm.rwmat{2}; % CR, 28-09-2016 11:56:24
     output.MeasPred.YpredCal = YpredCal;

     output.MeasPred.Yval     = Yval;   % rows: I eller It
     output.MeasPred.YpredVal = Ypred;  % rows: I eller It

     output.MeasPred.stats    = stats;  % use of stats (e.g. in LXMEASPRED.M):
                                        %    FN = fieldnames(stats.val);
                                        %    for f=1:length(FN)
                                        %      cat(1,stats.(FN{f}));
                                        %    end
                                        % or:
                                        %    RMSE = cat(1,stats.val.RMSE)

  end



end % calc_statistics

% ---------------------------------------
function SegMat = lxgetsegs(NumObjs,NumSegs,OPTIONS);

%  function SegMat = lxgetsegs(NumObjs,NumSegs,OPTIONS);
%
%  This function returns a logical matrix of ones and zeros corresponding
%  to test and training set resp.
%
%  NumObjs and NumSegs are the number of objects and cross validation segments, resp.
%
%  If NumSegs is non-scalar, but a vector of length NumObjs, the ones are distributed
%  according to this vector (= category variable)
%
%  if OPTIONS==0 the ones are distributed evenly over the coloumns
%     (or nargin==2)
%  if OPTIONS==1 the ones are gathered in groups
%  if OPTIONS==2 the ones are distributed randomly
%
%  e.g.:
%
%    lxgetsegs(7,3) or lxgetsegs(7,3,0):
%
%     1     0     0
%     0     1     0
%     0     0     1
%     1     0     0
%     0     1     0
%     0     0     1
%     1     0     0
%
%    lxgetsegs(7,3,1):
%
%     1     0     0
%     1     0     0
%     1     0     0
%     0     1     0
%     0     1     0
%     0     0     1
%     0     0     1
%
%    lxgetsegs(7,3,2):  sum(lxgetsegs(7,3,2)) always equals 3 2 2
%
%
%  Carsten Ridder, April 1996
%  Modified LN, July 2004

  if length(NumSegs) > 1
    CatVar  = NumSegs;
    classes = unique(NumSegs);
    NumSegs = size(classes,1);
  else
    CatVar  = [];
  end

  if NumSegs==1
    SegMat = logical(ones(NumObjs,NumSegs));
    return
  else
    SegMat = logical(zeros(NumObjs,NumSegs));
  end

  if nargin==2
    OPTIONS = 0;
  end

  SegMatNy = [];

  if isempty(CatVar)
  	if OPTIONS == 0
        for N = 1:NumSegs
          omr = N:NumSegs:NumObjs;
          SegMat(N:NumSegs:NumObjs,N) = 1;
        end
  	elseif OPTIONS==1
        grstr = floor(NumObjs/NumSegs);
        NumObjSeg = grstr*ones(1,NumSegs);
        LeftOver = rem(NumObjs,NumSegs);
        for i = 1:LeftOver
          NumObjSeg(i) = NumObjSeg(i)+1;
        end
        start = 1;
        for N = 1:NumSegs
          grend = start+NumObjSeg(N)-1;
          SegMat(start:grend,N) = 1;
          start = grend+1;
        end
  	elseif OPTIONS==2
        grstr = floor(NumObjs/NumSegs);
        SegMat = lxgetsegs(NumObjs,NumSegs,0);
        for i = 1:NumSegs-1
          SegMatNy = [SegMatNy; mix(SegMat(samp2idx(i,grstr),:)')'];
        end
        LeftOver = rem(NumObjs,NumSegs);
        SegMatNy = [SegMatNy; mix(SegMat(i*grstr+1:NumObjs,:)')'];
        SegMat = SegMatNy;
      end
  else % CatVar not empty
      for N = 1:NumSegs
        ix = find(CatVar == classes(N));
        SegMat(ix,N) = 1;
      end
  end

  if ~isempty(find(sum(SegMat)==0))
    %keyboard
    error % disp('ERROR: empty column')
  end

  if sum(sum(SegMat')-ones(1,NumObjs))~=0
    error % disp('ERROR: one object is used more than once')
  end

  SegMat = logical(SegMat);

end % lxgetsegs


% ----------------------------------------
function out = mix(in);

%  function out = mix(in);
%  Columns in 'in' are mixed randomly
%  Carsten Ridder, May 1996

  [i,j] = size(in);
  temp = rand(1,j);
  [y,l] = sort(temp);
  out = in(:,l);
end


% ----------------------------------------
function [Idx,NumSamps,NumAndRep]=samp2idx(SampNums,Rep);

   %  Extract indices corresponding to sample numbers in matrices containing replicates
   %
   %  function [Idx,<NumSamps>,<NumAndRep>]=samp2idx(SampNums,Rep);
   %
   %  When you have a vector or a matrix which contains replicate values, and
   %  you want to extract some specific sample numbers, you call this function
   %  stating the sample numbers (SampNums) you want to extract, and the
   %  replicate number (Rep). The function then returns the indices (Idx) which
   %  correspond to the sample numbers, the number of samples (NumSamps) and a matrix
   %  NumAndRep, in which the first coloumn is 1:NumSamps, and the second coloumn is the number
   %  of replicates for the individual samples.
   %
   %  Example:
   %
   % Sam. idx  Rep
   %
   %  1   1     3
   %      2     3
   %      3     3
   % -------------
   %  2   4     3
   %      5     3
   %      6     3
   % -------------
   %  3   7     4
   %      8     4
   %      9     4
   %     10     4
   % -------------
   %  4  11     3
   %     12     3
   %     13     3
   % -------------
   %  5  14     2
   %     15     2
   %
   %   Idx=samp2idx([1 3],Rep)    ->  Idx=[1 2 3 7 8 9 10]'
   %   Idx=samp2idx([3 1],Rep)    ->  Idx=[7 8 9 10 1 2 3 ]'    (by April 1996)
   %   Idx=samp2idx(4,Rep)        ->  Idx=[11 12 13]'
   %   Idx=samp2idx([5 2],Rep)    ->  Idx=[14 15 4 5 6]'
   %
   %   From to day (5/12-1996), Rep can be a vector (e.g. returned by READCSV.M). If Rep
   %   is the scalar "1", i.e. Rep=1, the indices return in Idx are set equal to SampNums, and
   %   NumSamps and NumAndRep are set to [1] and [1 1] resp. If Rep is a scalar > 1, e.g. Rep=2,
   %   then NumSamps and NumAndRep are empty.
   %
   %
   %   Carsten Ridder, Jan.  1996
   %                   April 1996
   %                   Dec.  1996
   %                   Oct.  1997


     Idx=[];
     NumSamps=[];
     NumAndRep=[];

     if length(Rep)==1 & Rep==1
       Idx=SampNums;
       NumSamps=1;
       NumAndRep=[1 1];
       return
     end

     if length(Rep)==1 & Rep~=1
         if isempty(SampNums)
           NumSamps=[];
           error % disp('The number of samples can not be calculated')
           return
         end
   %     SampNums=makecol(SampNums)';
         SampNums=SampNums(:)';
         IdxRep=SampNums*Rep;
         Idx=[];
         for i=1:Rep
           Idx(i,:)=IdxRep-(i-1);
         end
         Idx=flipud(Idx);
         Idx=Idx(:);
         NumSamps=[];
     else
        % For�ger hastigheden MANGE gange afh�ngig af length(Rep) (CR, 31-10-2001 10:30)
        %
        % if nargout>2        % For�ger hastigheden 10-70 gange afh�ngig af length(Rep) (CR, 31-10-2001 10:30)
        %   Num=length(Rep);
        %   i=1;
        %   j=1;
        %   while i<=Num
        %     NumAndRep(j,:)=[j Rep(i)];
        %     i=i+Rep(i);
        %     j=j+1
        %   end
        % end

   %     NumSamps=GetNumSamps(Rep);
   %     NumAndRep=[[1:NumSamps]' meanrep(Rep,Rep)]

         [NumSamps,NumAndRep]=GetNumSamps(Rep);
         ny=1;
         if ny
         % Ny metode (CR, 31-10-2001)
               if isempty(SampNums)
                 Idx=[];
                 return
               end
               SampNums=SampNums(:)';
               Idx=cell(length(SampNums),1);
               WhereIdx=cumsum(NumAndRep(:,2));
               start=WhereIdx(SampNums)-NumAndRep(SampNums,2)+1;
               slut=start+NumAndRep(SampNums,2)-1;
               for i=1:length(SampNums)
                  Idx{i}=start(i):slut(i);
               end
               Idx=cat(2,Idx{:})';
         else
               Idx=[];
               for i=1:length(SampNums)
                 start=sum(NumAndRep(1:SampNums(i)-1,2))+1;
                 slut=start+NumAndRep(SampNums(i),2)-1;
                 Idx=[Idx; [start:slut]'];
               end
         end
     end
  end % samp2idx


% Testet med
% S=9000; Rep=repdoble(4,S*4); tic,[Idx,NumSamps,NumAndRep]=samp2idx(1:S,Rep);crtoc
% Ny metode tager 0.593 sekunder, og den gamle tog 11.858 sekunder eller 20 gange hurtigere


% Gammel metode:
%
%      Idx=[];
%      for i=1:length(SampNums)
%        start=sum(NumAndRep(1:SampNums(i)-1,2))+1;
%        slut=start+NumAndRep(SampNums(i),2)-1;
%        Idx=[Idx; [start:slut]']
%      end



%classdef lxModel < handle
%% Carsten Ridder, 26-04-2015 11:36:10
%
%properties
%
%  Number                        % Unique model number
%  Name                          % The name of the model
%  Comment                       % A user-set model comment/note (CR, 13-10-2005 13:50)
%  DataSetName                   % Name of dataset on which the model is based
%  ModelType = 'PCA'                  % PCA, PLS, ...
%
%  ModelSettings  = struct            % Structure containing ValidationType, crossval-settings, etc.
%  ModelData = struct            % Structure containing model matrices (P, T, eigvals, resXi, etc.). The specific content will depend on the ModelType
%  IndexIntoDataSet = cell(4,2)  % Corresponds to lx.IndexIntoDataSet [logicals with same size as original global dataset]
%
%  ModelTypes = {'PCA'; ...
%                'PLS'}; %  'PCR';'MLR';'SIMCA','ASCA', ...
%
%  ValidationTypes = {'No validation'; ...
%                    'CV: Full'; ...
%                    'CV: Syst123'; ...
%                    'CV: Syst111'; ...
%                    'CV: Random'; ...
%                    'CV: Random (repeated)'; ...
%                    'CV: Sets'; ...
%                    'CV: Variable ...'; ...
%                    'Test set'};
%
%  TableHeader                   % The properties to show in a uitable e.g. in a GUI
%
%  lxt = lxTransform             % Array of HT's transformationsobjekt; lxt(1): X, lxt(2): Y (CR, 26-09-2016 12:35:36)
%
%  RecalcTransformationNeeded = false % If "true", the Transdata-vector must be calculated for the model later in order to
%                                     % transfer the transformation vectors (mx. sx, etc.) to the lxt-subclasses
%                                     % Used for backwards compatibility to pre-oop LatentiX-files.
%                                     % CR. 26-09-2016 13:40:57
%
%
%
%%  Model = struct('ModelSettings', struct, ...            % Structure containing ValidationType, crossval-settings, etc.
%%                 'ModelData',     struct);          % Structure containing model matrices (P, T, eigvals, resXi, etc.). The specific content will depend on the ModelType
%
%% 'IndexIntoDataSet', cell(4,2), ... % Equals lx.IndexIntoDataSet [logicals with same size as original global dataset]
%
%
%    % From "lxalign":
%    %
%    %
%    %Valid                        % False if data in the DataSet has been changed
%    %CurrentDataSet               % False if a model from one dataset is loaded into another dataset
%    %
%    %
%    %Validation                   % No validation, Full cross, Syst111, ....
%    %MaxPC                        % Max number of PC's to use in the model
%    %OptimalDimension             % The Optimal Dimension for the model
%    %Included                     % Defined object and variable sets "included" in the model
%    %Transform                    % Transformations performed before modelling
%    %NumRandomRepeat              % The number of repetitions in "CV: Random (repeated)" (v2.00, 07-01-2008 16:36)
%    %TransData                    % Transformation data required for future use
%    %Labels                       % Object and variable labels - selection status
%    %MatRange                     % Matrix numbers (X/Y/Xt/Yt) dependent on model- and validation type (14-09-2004 12:39)
%    %Index                        % Object and variable labels - selection status
%    %IndexIntoDataSet             % Logical arrays same size as original dataset with ones for selection (see note)
%    %Name                         % User-specified name
%    %ModelID                      % 26-04-2005 09:11
%    %CatVar                       % Information about category variables (CR, 09-08-2005 15:41)
%    %plotdata
%    %Pred
%
%  end
%
%methods
%
%% Constructor
%function lxm = lxModel(varargin)
%
%  for m = 1:2
%    lxm.lxt(m) = lxTransform; % 27-09-2016 11:46:57
%  end
%
%  lxm.ModelSettings.MaxPC = 12;                         % Max number of PC's to use in the model
%  lxm.ModelSettings.OptimalDimension = 1;               % The optimal number of PC's for the model
%  lxm.ModelSettings.Validation = 'No validation';
%  lxm.ModelSettings.NumSegs = 4;                        % in cross-validation (CV)
%  lxm.ModelSettings.NumRandomRepeats = 20;
%
%% Vi overtager i f�rste omgang felterne fra pLX3 (LATENTIX pre-version 3), s� lxpca, lxpls, etc. kan bruges direkte.
%% Senere kan vi overveje at rense lidt ud i det
%% CR, 06-05-2015 15:26:42
%  lxm.ModelData = struct('P', [], ...          %
%                    'Q', [], ...          % only PLS
%                    'W', [], ...          % only PLS
%                    'T', [], ...          %
%                    'U', [], ...          % only PLS
%                    'b', [], ...          % only PLS
%                    'ssX0', [], ...       %
%                    'ssX', [], ...        %
%                    'ssY0', [], ...       % only PLS
%                    'ssY', [], ...        % only PLS
%                    'StdXres', [], ...    % only PLS (why?)
%                    'eigvals', [], ...    %
%                    'PctDes', [], ...     %
%                    'PctVar', [], ...     %
%                    'Si', [], ...         %
%                    'Sj', [], ...         %
%                    'Hi', [], ...         %
%                    'T2', [], ...         %
%                    'resXi', [], ...      %
%                    'resXj', [], ...      %
%                    'Siy', [], ...        % only PLS
%                    'Hiy', [], ...        % only PLS
%                    'Bscaled', [], ...    % only PLS
%                    'Converged', [], ...  % only PLS
%                    'B', [], ...          % only PLS
%                    'output', struct);    %
%
%  lxm.TableHeader = {'No','ModelType','PC','expvarXcal','expvarXval','expvarYcal','expvarYval','Name'};
%
%% E.g.
%%  pre-LX3:
%%
%%         Type: 'PCA'
%%            P: [129x12 double]
%%            T: [97x12 double]
%%      eigvals: [12x1 double]
%%       PctDes: [12x1 double]
%%       PctVar: [12x1 double]
%%           Si: [97x12 double]
%%           Sj: [12x129 double]
%%           Hi: [97x12 double]
%%           T2: [97x12 double]
%%        resXi: [97x12 double]
%%        resXj: [12x129 double]
%%         ssX0: 12384
%%          ssX: [2.9801e+03 1.8259e+03 1.4407e+03 1.1398e+03 944.3376 776.5787 639.5019 534.2640 462.8355 412.5366 363.2135 318.8865]
%%       output: [1x1 struct]
%%
%%         Type: 'PLS'
%%            P: [128x12 double]
%%            Q: [1 1 1 1 1 1 1 1 1 1 1 1]
%%            W: [128x12 double]
%%            T: [97x12 double]
%%            U: [97x12 double]
%%            b: [0.4369 0.1617 0.0746 0.1230 0.0638 0.0820 0.0542 0.0424 0.0323 0.0177 0.0323 0.0323]
%%         ssX0: 12288
%%          ssX: [2.9792e+03 2.3570e+03 1.5567e+03 1.3311e+03 1.0722e+03 937.8505 814.2353 705.4249 616.1668 489.9824 427.5663 386.1264]
%%         ssY0: 1.7945e+03
%%          ssY: [17.8984 8.4714 4.7685 2.0249 1.3239 0.6858 0.4143 0.2912 0.2191 0.1977 0.1618 0.1294]
%%      StdXres: [12x128 double]
%%      eigvals: [12x1 double]
%%       PctDes: [12x1 double]
%%       PctVar: [12x1 double]
%%           Si: [97x12 double]
%%           Sj: [12x128 double]
%%           Hi: [97x12 double]
%%           T2: [97x12 double]
%%        resXi: [97x12 double]
%%        resXj: [12x128 double]
%%          Siy: [97x12 double]
%%          Hiy: [97x12 double]
%%      Bscaled: [129x1x12 double]
%%    Converged: 1
%%            B: [129x1x12 double]
%%       output: [1x1 struct]           output =
%%                                             PredCal: [1x1 struct]
%%                                             PredVal: [1x1 struct]
%%                                            MeasPred: [1x1 struct]
%%                                               MaxPC: 12
%%                                              SegMat: [97x4 logical]
%%                                             NumSegs: 4
%%                                           MissStats: []
%%                                           ModelDate: 7.3609e+05
%%                                         MissingText: []
%%
%%
%
%end % constructor (function lxModel)
%
%end % methods
%
%
%end % classdef
%
%
%
