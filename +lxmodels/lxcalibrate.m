function DTM = lxcalibrate(DTM,calpred);
% 08-08-2017 11:27:55


  disp('Skal nok slet ikke bruges. CR, 18-08-2017 14:46:17')
  return

  switch DTM.CalMethod
    case 'PCA',   tempmodel = lxmodels.lxpca(DTM,calpred);
    case 'PLS',   tempmodel = lxmodels.lxpls(DTM,calpred);
  otherwise
    error('Ny kalibreringsmetode indf�rt. Tilret koden i LXCALIBRATE.M') % oop: "lx.ModelTypes" (CR, 11-05-2015 11:41:49)
  end % switch ModelType

  if isfield(DTM,'Model')
    %
    123
  else
    LatentModel = tempmodel;
  end



% GAMMEL (pr. 08-08-2017 11:27:38):
%  function LatentModel = lxcalibrate(DTM,calpred);
%  %%%function LatentModel = lxcalibrate(Matrices, ModelType, MaxPC);
%  % CR, 07-07-2004
%  % CR, 06-06-2005 MaxPC in inout
%  %
%  % Version 3 (CR, 29-06-2017 12:42:03)
%
%
%    switch DTM.CalMethod
%      case 'PCA',   tempmodel = lxmodels.lxpca(DTM,calpred);
%      case 'PLS',   tempmodel = lxmodels.lxpls(DTM,calpred);
%    otherwise
%      error('Ny kalibreringsmetode indf�rt. Tilret koden i LXCALIBRATE.M') % oop: "lx.ModelTypes" (CR, 11-05-2015 11:41:49)
%    end % switch ModelType
%
%    if isfield(DTM,'Model')
%      %
%      123
%    else
%      LatentModel = tempmodel;
%    end
