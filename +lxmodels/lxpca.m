classdef lxpca < lxmodels.lxModel
% CR, 01-11-2016
%     29-06-2017 13:39:09

methods

% constructor
function lxm = lxpca(DTM, calpred)

  lxm.CalMethod = 'PCA';
  lxm.UseY      = false;

  %%% if nargin == 0
  %%%   return
  %%% end
  %%%
  %%% switch calpred
  %%%   case 'calibrate', %% % Initialize and calculate the model. X-data is already transformed
  %%%                     %% lxm.ModelData = struct('P', [], ...          %
  %%%                     %%                        'T', [], ...          %
  %%%                     %%                        'ssX0', [], ...       %
  %%%                     %%                        'ssX', [], ...        %
  %%%                     %%                        'StdXres', [], ...    %
  %%%                     %%                        'eigvals', [], ...    %
  %%%                     %%                        'PctDes', [], ...     %
  %%%                     %%                        'PctVar', [], ...     %
  %%%                     %%                        'Si', [], ...         %
  %%%                     %%                        'Sj', [], ...         %
  %%%                     %%                        'Hi', [], ...         %
  %%%                     %%                        'T2', [], ...         %
  %%%                     %%                        'resXi', [], ...      %
  %%%                     %%                        'resXj', [], ...      %
  %%%                     %%                        'output', struct);    %
  %%%                     %%
  %%%
  %%%   case 'predict',   123
  %%%                     lxm.ModelData = DTM.Model.ModelData; % 28-07-2017 11:39:00
  %%%                     predict(lxm,DTM.Xpred);              % 07-07-2017 11:21:51
  %%%                     lxm.lxt = DTM.lxt;                   % 07-08-2017 13:17:00
  %%%
  %%% end % switch calpred

end % constructor

function calibrate(lxm, X, ~)
% Calibrate using PCA

% function lxpca(X, NumPC);
%  MANGLER lidt tekst her
%
%
%  P          is the matrix containing the loadings
%  T          is the matrix containing the scores
%
%  eigvals(n) egenv�rdier
%
%  X         is the input matrix
%  NumPC     is the number of factors calculated
%
%  Carsten Ridder, Sept. 1995

  if nargin == 1 % Prepare for cross-validation (09-08-2017 11:19:21)
    X = lxm.trmat{1};
  else
    X = lxm.lxt(1).calculateTransform(X); % Transform the raw input matrix
  end

  NumPC = lxm.ModelSettings.MaxPC;

  [P,T,eigvals,Xres,PctDes,S0] = deal([]);

  [I,J] = size(X);
%  if nargin < 2 || isempty(NumPC)
%    NumPC = min(I,J);
%  end

  if any(isnan(X(:)) | isinf(X(:)))
    X = lxm.lxmisssvd(X,NumPC); % CR, 02-11-2016 15:23:32
  end

  if isempty(find(isinf(X))) & isempty(find(isnan(X)))
    [U,S,V]   = svd(X,'econ');
    MaxPCAct = size(S,1);  % the actual number of calculated PC's
  else
    errordlg('{LXPCA.M}: NaN or Inf found in X','Error!','modal')
    %%% lxm.ModelData = []; % er sat i constructor
    return
  end

  NumPC = min(NumPC, MaxPCAct);

  lxm.ModelData.P = V(:,1:NumPC);
  lxm.ModelData.T = X*lxm.ModelData.P;

% CR, 09-09-2005 10:51
  S = diag(S);
  eigvals = S.^2/(I-1); % equals diag(lxm.ModelData.T'*lxm.ModelData.T)/(I-1) (truncated) (09-09-2005 10:59)

  PctDes  = 100*S/sum(S);
  PctVar  = cumsum(PctDes);
% Truncating AFTER calculations (09-09-2005 10:53)
  lxm.ModelData.eigvals = eigvals(1:NumPC);
  lxm.ModelData.PctDes  = PctDes(1:NumPC);
  lxm.ModelData.PctVar  = PctVar(1:NumPC);

% Calculating Si (residual variance)
  ssX0 = sum(sum(X.^2)); % CR, 11-08-2005 11:24
  for a = 1:NumPC
     Xres = X - lxm.ModelData.T(:,1:a)*lxm.ModelData.P(:,1:a)';  % residual after a PC's
    %resXi(:,a)  = sum((Xres').^2)';
    %resXj(a,:)  = sum((Xres).^2)';
    %ssX(a)      = sum(sum(Xres.^2));

     resXi(:,a) = sum(Xres.^2,2); % erstatter: "sum((Xres').^2)';"
     resXj(a,:) = sum(Xres.^2,1); % erstatter: "sum((Xres).^2);"
     ssX(a)     = sum(sum(Xres.^2));

  end
  Si = sqrt(resXi);
  Sj = sqrt(resXj);

% Calculating Hi (leverage)
  Hi = utilities.lxscale(lxm.ModelData.T.^2,zeros(1,NumPC),diag(lxm.ModelData.T'*lxm.ModelData.T)');
  if a > 1
    Hi = cumsum(Hi')';
  end
  Hi = 1/I + Hi;

% Calculating Hotellings T^2 (proportional to Hi-leverage)
  T2 = lxm.ModelData.T.^2*inv(diag(lxm.ModelData.eigvals(1:NumPC)));
  if NumPC > 1
    T2 = cumsum(T2')';
  end
% Hi = T2; % NOTE: Hotellings T^2 is used for Hi (CR, 09-05-2005 09:47)

  lxm.ModelData.Si = Si;
  lxm.ModelData.Sj = Sj;
  lxm.ModelData.Hi = Hi;

  lxm.ModelData.T2  = T2;
  lxm.ModelData.resXi = resXi;
  lxm.ModelData.resXj = resXj;

  lxm.ModelData.ssX0 = ssX0;
  lxm.ModelData.ssX  = ssX;

%return % LN + CR, 24-10-2006 16:07
%
%% Tilf�jet (CR, 13-09-2006 14:28)
%  CalObjs = size(lxm.ModelData.T,1);
%  CalVars = size(lxm.ModelData.P,1);
%
%  for a = 1:NumPC
%    dftaeller = (CalVars-a);
%    taeller   = lxm.ModelData.resXi(:,a)/dftaeller;
%
%    dfnaevner = (CalVars-a)*(CalObjs-a-1);
%    naevner   = lxm.ModelData.ssX(a)/dfnaevner;
%
%    F = taeller/naevner;
%
%    lxm.ModelData.Ftest(:,a) = lxlimits('Fdist',F,dftaeller,dfnaevner); % The higher value, the more and outlier
%  end

end % calibrate

% ---------------------------------
function predict(lxm, X, ~)
% Predict using PCA

% CR, 08-07-2004 17:57
% CR, 16-09-2004 13:46
% CR, 28-07-2017 12:20:32


%  lxm.ModelData =
%
%    ModelType: 'PCA'
%            T: [10x10 double]
%            P: [100x10 double]
%      eigvals: [10x1 double]
%       PctDes: [10x1 double]
%       PctVar: [10x1 double]
%

  ModelData = lxm.ModelData; % 28-07-2017 11:40:30

  NumPC  = size(ModelData.P,2);

  T = X*ModelData.P;  % projecting new objects on the model (P)
  ssX0 = sum(sum(X.^2)); % CR, 11-08-2005 11:24
  for a = 1:NumPC
    Xres       = X-T(:,1:a)*ModelData.P(:,1:a)'; % residual after a factors
    resXi(:,a) = sum(Xres.^2,2); % erstatter: "sum((Xres').^2)';"
    resXj(a,:) = sum(Xres.^2,1); % erstatter: "sum((Xres).^2);"   som er forkert ved vektor-input (CR, 13-09-2006 11:23)
    ssX(a)     = sum(sum(Xres.^2));
  end

  Si = sqrt(resXi);
  Sj = sqrt(resXj);

% Calculating Hi (leverage)
  Hi = utilities.lxscale(T.^2,zeros(1,NumPC),diag(ModelData.T'*ModelData.T)');
  if a>1
    Hi = cumsum(Hi')';
  end
  Hi = Hi + 1/size(ModelData.T,1);  % plus 1/I

% Calculating Hotellings T^2 (proportional to Hi-leverage)
  T2 = T.^2*inv(diag(ModelData.eigvals(1:NumPC)));
  if NumPC > 1
    T2 = cumsum(T2')';
  end
% Hi = T2; % NOTE: Hotellings T^2 is used for Hi (CR, 09-05-2005 09:47)

  lxm.PredData.Si = Si;
  lxm.PredData.Sj = Sj;
  lxm.PredData.Hi = Hi;

  lxm.PredData.T2    = T2;
  lxm.PredData.resXi = resXi;
  lxm.PredData.resXj = resXj;

  lxm.PredData.T = T;

  lxm.PredData.ssX0 = ssX0; % CR, 11-08-2005 16:14
  lxm.PredData.ssX  = ssX;  %        "

%return % LN + CR, 24-10-2006 16:07
%
%% Tilf�jet (CR, 13-09-2006 13:54)
%  CalObjs = size(ModelData.T,1);
%  CalVars = size(ModelData.P,1);
%
%  for a = 1:NumPC
%    dftaeller = (CalVars-a);
%    taeller   = lxm.PredData.resXi(:,a)/dftaeller;
%
%    dfnaevner = (CalVars-a)*(CalObjs-a-1);
%    naevner   = ModelData.ssX(a)/dfnaevner;
%
%    F = taeller/naevner;
%
%    lxm.PredData.Ftest(:,a) = lxlimits('Fdist',F,dftaeller,dfnaevner); % The higher value, the more and outlier
%  end
%
end % predict


end % methods
end % classdef

