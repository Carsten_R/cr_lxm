function [Xm, stats] = lxmisssvd(X, OptPC);
% Xm = lxmisssvd(X, OptPC); %
% oop (CR, 28-09-2016 11:25:01)
% ej mere oop (CR, 28-06-2017 16:42:32)
%
% This algorithm requires the presence of: 'nanmean.m'
% CAAs comment: It handles missing values NaNs (very dispersed, less than 15%)
%
% OptPC as optional input ()


%  OPSKRIFT P� PRINCIP:
%
%  Fra: Lars N�rgaard [mailto:Lars.Noergaard@mli.kvl.dk]
%  Sendt: ma 27-02-2006 23:05
%  Til: Carsten Ridder
%  Cc: claus@andersson.dk
%  Emne: Missing
%
%
%  Hej Carsten,
%
%  Ja, som det fremgik af tidligere mail, mener jeg virkelig, at det er en revolution, du har lavet p� plotsiden.
%
%  Nu er der faktisk kun en st�rre ting tilbage, nemlig missing. Vedlagt et bud baseret p� Claus' m-fil fra 1995.
%  Claus, hvis du erindrer noget om problemer etc. med algoritmen m� du gerne sige til. Umiddelbart ser det fornuftigt ud.
%
%  Jeg forestiller mig f�lgende 'strategi':
%  PCA, No val, CV og Test Set: missing estimeres samlet for X inden beregning
%  PCA, Prediction: hvis missing i Prediction set, s� estimer for Cal+Pred set samlet inden Prediction
%
%  PLS, No val, CV og Test Set: missing estimeres samlet for X og samlet for Y (X og Y hver for sig) inden beregning.
%  Hvis Y er en vektor, estimeres ikke, men udelades i stedet.
%
%  PLS, Prediction: hvis missing i Prediction set for X, s� estimer samlet for X (Cal+Pred set) inden Prediction
%
%  Bem�rk at missing estimationen laves uafh�ngigt af og inden de klassiske ting som forbehandling etc.
%  Algoritmen er baseret p� auto-skalerede data, men det er udelukkende lokalt til estimationen;
%  resultatet er de r� uforbehandlede data.
%
%  Om metoden er god eller ej ved jeg ikke, men den er �n vej igennem, Det vigtigste er at missing h�ndteres,
%  s� brugeren ikke st�der p� alvorlige problemer; i princippet er det underordnet om der regnes fuldst�ndig
%  forkert bare missing h�ndteres! Den vil jeg ikke citeres for... og jeg mener det selvf�lgelig ikke!
%
%  Mange hilsner
%  Lars

disp('I den forkerte lxmisssvd!!')

  Xm    = X;  % CR, 29-09-2016 16:17:17
  stats = []; % CR, 29-09-2016 16:17:17

  if ~any(isnan(X(:)) | isinf(X(:)))
    disp('LXMISSSVD: No missing data found!')
    return % CR, 29-09-2016 16:23:06
  end


  Xoriginal = X; % CR, 08-09-2006 12:16  ("original untransformed matrices")

  [I,J] = size(X);

  MaxPC = min(I,J);  % v2.00, 26-01-2008 12:19
  if nargin == 1
    OptPC = MaxPC;
  end

  ConvLim = 1e-12;
% WarnLim = 1e-4;
  ConvLimMiss = 100*ConvLim;
  itmax = 100;

% 'prepro' is either auto, mean or none
% I have decided to use auto based on a very small test...

% prepro = 'Mean center';
  prepro = 'Autoscale';

% OPTIONS STATES 15% PERCENT MISSING IS A MAXIMUM
%  [X, TransData] = lxtransform(lx, X, 'Autoscale');
  lxt = lxTransform;
  lxt.selectedTransform = {prepro};
  X = lxt.calculateTransform(X); % CR, 28-09-2016 11:18:13


% Handles missing data
  X(find(isinf(X))) = NaN;  % CR, 01-03-2006 12:44
  MissIdx = find(isnan(X));
  [i,j] = find(isnan(X));

  stats.ijMatrix = [i, j];
  stats.rkMiss = zeros(I,1);
  for rk = unique(i)'
    stats.rkMiss(rk) = 100*length(find(isnan(X(rk,:))))/J;
  end

  stats.sjMiss = zeros(J,1);
  for sj = unique(j)'
    stats.sjMiss(sj) = 100*length(find(isnan(X(:,sj))))/I;
  end

  stats.PctMissing = 100*length(MissIdx)/numel(X);

%  [rkmax,objmax] = max(stats.rkMiss);
%  [sjmax,varmax] = max(stats.sjMiss);
%  lxlogerror([num2str(stats.PctMissing,'%4.1f'),'% missing data (matrix)'])
%  lxlogerror([num2str(rkmax,'%4.1f'),'% missing data in object #',int2str(objmax),'  [',int2str(length(unique(i))),' objects with missing data]'])
%  lxlogerror([num2str(sjmax,'%4.1f'),'% missing data in variable #',int2str(varmax),'  [',int2str(length(unique(j))),' variables with missing data]'])

%  lxlogerror(' ')
%  lxlogerror('% missing data (objects)')
%  lxlogerror([int2str([1:I]'),repmat(': ',I,1), num2str(stats.rkMiss,'%4.1f')])
%
%  lxlogerror(' ')
%  lxlogerror('% missing data (variables)')
%  lxlogerror([int2str([1:J]'),repmat(': ',J,1), num2str(stats.sjMiss,'%4.1f')])

%  mnx = nanmean(X)/3;
%  mny = nanmean(X,2)/3;
%   mnx = missmean(X)/3;
%   mny = missmean(X')/3;

  mnx = nanmean(X)/3;
  mny = nanmean(X')/3;

%% **************** HT, remove missing, 2007-07-02, START udkommentering ******************
%   nxnans = find(isnan(mnx)); % CR, 07-03-2006 08:53
%   nynans = find(isnan(mny));
%
%   if ~isempty(nxnans) && ~isempty(nynans)
%         uiwait(errordlg(strvcat(['All values are missing in ', ...
%                                   int2str(length(nynans)),' objects and ', ...
%                                   int2str(length(nxnans)),' variables.'],' ', ...
%                                  'Estimation of missing values is not possible'),'Please check your data!','modal'))
%         [Xm, stats] = deal([]);
%         return
%   else
%       if ~isempty(nxnans)
%         uiwait(errordlg(strvcat(['All values are missing in ',int2str(length(nxnans)),' variables.'],' ', ...
%                                  'Estimation of missing values is not possible'),'Please check your data!','modal'))
%         [Xm, stats] = deal([]);
%         return
%       end
%
%       if ~isempty(nynans)
%         uiwait(errordlg(strvcat(['All values are missing in ',int2str(length(nynans)),' objects.'],' ', ...
%                                  'Estimation of missing values is not possible'),'Please check your data!','modal'))
%         [Xm, stats] = deal([]);
%         return
%       end
%   end
%% **************** HT, remove missing, 2007-07-02, END udkommentering ********************

  n = size(i,1);
  for k = 1:n,
    i_ = i(k);
    j_ = j(k);
    X(i_,j_) = mny(i_) + mnx(j_);  % First estimate of missing value
  end;
  mnz = (nanmean(mnx)+nanmean(mny))/2;
  X(isnan(X)) = mnz;  % Never no nans left!? (26-01-2008 12:21)
  [U,S,V] = svd(X,'econ');

% Xm = U*S*V';
  Xm = U(:,1:OptPC)*S(1:OptPC,1:OptPC)*V(:,1:OptPC)'; % v2.00, 26-01-2008 12:26


  X(MissIdx) = Xm(MissIdx);
  ssmisold = sum(sum( Xm(MissIdx).^2 ));
  sstotold = sum(sum(X.^2 ));
  ssrealold = sstotold-ssmisold;
  iterate = 1;
  while iterate
    [U,S,V] = svd(X,'econ');

%   Xm = U*S*V';
    Xm = U(:,1:OptPC)*S(1:OptPC,1:OptPC)*V(:,1:OptPC)'; % v2.00, 26-01-2008 12:26

    X(MissIdx) = Xm(MissIdx);
    ssmis = sum(sum( Xm(MissIdx).^2 ));
    sstot = sum(sum( X.^2 ));
    ssreal = sstot-ssmis;
    if abs(ssreal-ssrealold)<ConvLim*ssrealold && abs(ssmis-ssmisold)<ConvLimMiss*ssmisold | iterate > itmax
       %iterate = 0;
       break
    end;
    ssrealold = ssreal;
    ssmisold = ssmis;
    iterate = iterate + 1;
  end

  if iterate > itmax
    uiwait(errordlg(strvcat('Estimation of missing values not possible',' ', ...
                           ['No convergence after ',int2str(itmax),' iterations']),'Check your data!','modal'))
%    disp('fjern igen!')
    [Xm, stats] = deal([]);
  else
   %Xm = lxrescale(Xm, TransData);
    Xm = lxrescale(Xm, lxt); % CR, 28-09-2016 11:24:30
    stats.Estimates = sparse(I,J);
    stats.Estimates(MissIdx) = Xm(MissIdx);

%  Skal de oprindelige ikke-NaN v�rdier i X ikke overf�res helt u�ndrede?? CR, 01-09-2006 13:07
%    X = lxrescale(X, TransData);
%    X(MissIdx) = Xm(MissIdx);
%
%  Indf�rer dette (08-09-2006 12:21):
    Xoriginal(MissIdx) = Xm(MissIdx);  % CR, 08-09-2006 12:21
    Xm = Xoriginal;                    % CR, 08-09-2006 12:21
  end


