function varargout = lxt_meancenter_OLD(lx, varargin)
% LXT_MEANCENTER Mean center a matrix. A method of lxdata
%
% Perform mean centering of data over the first array dimension.
%
%  I/O
%
%  Transformation info:                 settings = lxt_meancenter(lx)
%  Calculate transformation:    [Xtr, TransData] = lxt_meancenter(lx, X)
%  Use transformation:                       Xtr = lxt_meancenter(lx, X, TransData)
%
%
% How to test the function from e.g. the command line
% ===================================================
%
% Create an empty (dummy) instance of an lxdata-object:
%
%    lx = lxdata;
%
% Fetch info about the transformation (one input and one output argument):
%
%    settings = lxt_meancenter(lx)                                   <------
%
%    settings =
%
%        showname: 'Mean &center'
%        helptext: [3x87 char]
%        funcname: 'lxt_meancenter'
%
% Create a dummy test matrix "X":
%
%    X = rand(5,3)
%
%      X =
%
%      0.4357    0.9049    0.4087
%      0.3111    0.9797    0.5949
%      0.9234    0.4389    0.2622
%      0.4302    0.1111    0.6028
%      0.1848    0.2581    0.7112
%
%     X(2,2) = nan
%
%     X =
%
%         0.4357    0.9049    0.4087
%         0.3111       NaN    0.5949
%         0.9234    0.4389    0.2622
%         0.4302    0.1111    0.6028
%         0.1848    0.2581    0.7112
%
% Mean-center X and retrieve the mean vector (two input and two output arguments):
%
%    [Xtr, TransData] = lxt_meancenter(lx, X)                        <------
%
%    Xtr =
%
%       -0.0213    0.4766   -0.1073
%       -0.1459       NaN    0.0789
%        0.4663    0.0106   -0.2538
%       -0.0268   -0.3171    0.0869
%       -0.2722   -0.1702    0.1952
%
%
%    TransData =
%
%        funcname: 'lxt_meancenter'
%              mx: [0.4570 0.4282 0.5160]
%
% Mean-center X and using the previously calculated mean vector (three input and one output arguments):
%
%    Xny = lxt_meancenter(lx, X, TransData)                          <------
%
%     Xny =
%
%        -0.0213    0.4766   -0.1073
%        -0.1459       NaN    0.0789
%         0.4663    0.0106   -0.2538
%        -0.0268   -0.3171    0.0869
%        -0.2722   -0.1702    0.1952%
%
%    isequalwithequalnans(Xtr,Xny)
%
%      ans = 1
%
%
% CR, 19-09-2016 11:47:04


% =======================================================
%
% The function name shall beging with the prefix "lxt_"
%
% Edit these two lines:

  settings.showname = 'Mean &center'; % Descriptive name optionally including "&", i.e. which letter to underscore in a GUI
  settings.helptext = char('Mean centering subtracts the coloumn mean from each element in the coloumn.', ...
                           'This corresponds to moving the origo of the coordinate system to the center of the data', ...
                           '... can be continued ...');

% Write/edit these three functions below:
%
%      calculateTransform
%      applyTransform
%      canApply
%
% =======================================================


% DO NOT EDIT:
  nin = nargin;
  funcname = getfuncname;

  settings.funcname = funcname;    % e.g. 'lxt_meancenter'; the transformation method file name

      if nin == 1, varargout{1} = settings;                                                  % Set relevant information only,                 e.g. settings = lxtransform(lx);
  elseif nin == 2, [varargout{1}, varargout{2}] = calculateTransform(varargin{1}, funcname); % Calculate the transform and set the TransData, e.g. [X, TransData] = lxt_meancenter(lx, X)
  elseif nin == 3, varargout{1} = applyTransform(varargin{1}, varargin{2});                  % Apply a previously calculated transform,      e.g. X = lxt_meancenter(lx, X, TransData);
  else
    ME = MException([funcname,':inputArguments'], 'Wrong number of input arguments: 1, 2 or 3 allowed'); throw(ME);
  end


% ------------------------------------------------
function [Xtr, TransData] = calculateTransform(X, funcname)
% Mean center input data using coloumns means of X

  mx  = mean(X,1,'omitnan');
  Xtr = bsxfun(@minus,X,mx);

  TransData.funcname = funcname; % do not edit
  TransData.mx       = mx;


% ------------------------------------------------
function Xtr = applyTransform(X, TransData)
% Mean center input data using previously calculated mean

  if canApply(X, TransData.mx)
     Xtr = bsxfun(@minus, X, TransData.mx);
  else
     Xtr = [];
  end

% -------------------------------------------------
function OK = canApply(X, mx)
% Perform check on input data

  if isempty(mx)
     OK = false;
     ME = MException('lxt_meancenter:applyTransform:cannotApplyTransform', ...
                     'Transformation has not previously been calculated!');
  elseif ~isequal(size(X,2),length(mx))
     OK = false;
     ME = MException('lxt_meancenter:applyTransform:incorrectMeanVectorLength', ...
                     'Mean vector length (%d) does not match number of columns in data (%d)!',length(mx),size(X,2));
  else
     OK = true;
  end

  if ~OK && ~isdeployed
     throw(ME)
  end


%  msgID   = 'lxt_meancenter:InputRequired';
%  msgtext = 'Input variables are mandatory!';
%
%
%      if nin == 0, ME = MException(msgID,msgtext); throw(ME);
%

