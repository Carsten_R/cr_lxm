classdef lxdata < handle
% LXDATA Class for LATENTIX data
%
% Carsten Ridder, 12-12-2014 12:29:11
%                 30-12-2014 11:57:08
%                 April 2015
%                 September 2016

properties

  filename = ''           % Filename
  pathname = ''           % ... and location of the data

  data = table            % MATLAB table-variable containing all data
  dataID                  % "Unique" identifiers for objects and variables (CR, 07-11-2016 15:40:10)

  IndexIntoDataSet = cell(4,2)  % From LATENTIX.M:
                                %
                                % IndexIntoDataSet [logicals with same size as original global dataset]
                                % E.g. IndexIntoDataSet{MatNum,dim}:
                                %                  dim = 1           dim = 2
                                % MatNum = 1   [569x1 logical]    [31x1 logical]   X
                                % MatNum = 2   [569x1 logical]    [31x1 logical]   Y
                                % MatNum = 3   [569x1 logical]    [31x1 logical]   Xt
                                % MatNum = 4   [569x1 logical]    [31x1 logical]   Yt
                                %
                                % "true" for objects/variables included


% Models = lxmodel              % array of "lxmodel"-objects (Abstract classes cannot be instantiated! 28-06-2017 16:12:26)

  Predictions                   % Predictions calculated (should NOT be included in Models!)

%%lxt = lxTransform             % X: lxt(1) and Y: lxt(2); array of "lxTransform"-objects (26-09-2016 11:18:56)
  lxt = [lxTransform, lxTransform]; % X: lxt(1) and Y: lxt(2); array of "lxTransform"-objects (26-09-2016 11:18:56)

  MSCbase = ''                  % The name of the variable set to be used as the MSC base
  SGbase  = ''                  % The name of the variable set to be used in the Savitzky-Golay transform


  selectedCalMethod         = 'PCA'
  selectedValidation        = 'No validation'
  selectedOptimalDimension  =  1
  selectedMaxPC             = 12
  selectedNumSegs           =  4
  selectedNumRandomRepeats  = 20
  selectedCatVarNum         = []
  selectedHandleMissing     =  1 % Take care of missing data
  selectedGlobalMinPC       = 12


  selectedModelNum          =  1
  selectedMatNum            =  1 % As default select the matrix X. Create a method which ensures, that an object can be placed in only one X or Y matrix (etc.)
  selectedDim               =  2;

  Sets         %     Sets:  N-dimensional cell array - Labels and indices for the sets in each dimension
  SetStyle     % SetStyle:  the plot style (Marker, Color, etc.) for the sets

% Sets skal erstattes af en tabel: SetTable
% SetStyle skal erstattes af en tabel: SetStyleTable? (CR, 07-11-2016 07:00:00)
% SetStyleTable: setid	dim	           Label	          Marker	      MarkerSize	           Color	       LineStyle	       LineWidth



  trmat        % The 4 basic matrices in their transformed forms, i.e. ready for e.g. plots and modeling (05-05-2015 16:14:44)
  rwmat        % The four currently selected raw and un-transformed matrices from lx.data
               %
               %  X = rwmat{1}
               %  Y = rwmat{2}
               % Xt = rwmat{3}
               % Yt = rwmat{4}
  curmat       % Can be used e.g. like: plot(lx.xaxis{dim},lx.curmat), where "dim" is the plot-direction (cf. lxdata/plot)end
  xaxis        % - ditto -


% Properties related to GUI's
  gui = struct            % e.g. set(handles.panelSegments, lx.gui.panelSegments)
  Transposed = 1          % plot the transposed matrix
  PlotTransformed = false % true; % When a GUI is active this property determines which data to plot: raw or transformed

% Genstart MATLAB ved: "Maximum recursion limit of 500 reached. Use set(0,'RecursionLimit',N) to change the limit. Be aware that exceeding your available stack space can crash MATLAB and/or your computer."


%stylefield = struct('Label',{''}, ...
%                    'Marker','', ...
%                    'MarkerSize',[], ...
%                    'Color',[], ...
%                    'LineStyle','', ...
%                    'LineWidth',[], ...
%                    'SetType',[]);  % SetType = 0 don't show in "Edit set styles .." in work bench
%                                    % SetType = 1 show in "Edit set styles .." in work bench

end % properties

%properties (Dependent, SetAccess = private)
%  curmat
%  xaxis
%  rwmat  % The four currently selected raw and un-transformed matrices from lx.data
%         %
%         %  X = rwmat{1}
%         %  Y = rwmat{2}
%         % Xt = rwmat{3}
%         % Yt = rwmat{4}
%end

properties (SetObservable, AbortSet) % OK med AbortSet?

% Default settings; can e.g. be changed by a GUI or in workspace
% When some of these settings are changed some action should be taken.
% E.g. when lx.selectedTransform is changed to
%
%  lx.selectedTransform{1} = removeampersand(lx.TransformTypes.transformDescriptions([5,2]));  (X and Xt)
%  lx.selectedTransform{2} = removeampersand(lx.TransformTypes.transformDescriptions(1));      (Y and Yt)
%
% the plot of the transformed data should be updated if this is selected in a GUI.


% Implementation way B (affects code in GetGUIparameters) is most efficient:
% IndexIntoDataSet           % From LATENTIX.M:
                             %
                             % IndexIntoDataSet [logicals with same size as original global dataset]
                             % E.g. IndexIntoDataSet{MatNum,dim}:
                             %                  dim = 1           dim = 2
                             % MatNum = 1   [569x1 logical]    [31x1 logical]   X
                             % MatNum = 2   [569x1 logical]    [31x1 logical]   Y
                             % MatNum = 3   [569x1 logical]    [31x1 logical]   Xt
                             % MatNum = 4   [569x1 logical]    [31x1 logical]   Yt
                             %
                             % "true" for objects/variables included


end % properties (SetObservable)

methods

% Constructor
function lx = lxdata(varargin)
% if nargin == 0, return, end

    if nargin > 0 & isa(varargin{1},'lxdata')
      lx = varargin{1};
      return
    end


% The user has decided to read data from a file/filetype
  if nargin > 0
    invar = char(varargin{1});
    [pathstr,name,ext] = fileparts(invar);

    if ~isempty(pathstr) & ~isempty(name)
      callvar = invar; % A file is specified in the input
      ext(ismember(ext,'.')) = '';
      filetype = ext;
    else
      callvar = '';
      filetype = name;
    end

    switch filetype
      case 'lxf',          read_lxf(lx,callvar);
      case {'xls','xlsx'}, read_xls(lx,callvar);
    end

  end % if nargin > 0

  allobjs = true(size(lx.data,1),1);
  no_objs = ~allobjs;

  allvars = transpose(cell2mat(varfun(@isnumeric,lx.data,'OutputFormat','cell'))); % int8, int16, double, ... NOT categorical, char, datestring, etc.
  no_vars = false(size(lx.data,2),1);

% As default select the matrix X
% Create a method which ensures, that an object can be placed in only one X or Y matrix (etc.)
  lx.IndexIntoDataSet = [ {allobjs, allvars};   % indices for objects and variables for sub-table X
                          {no_objs, no_vars};   % indices for sub-table Y
                          {no_objs, allvars};   % indices for sub-table Xt
                          {no_objs, no_vars} ]; % indices for sub-table Yt


% ==============================================================================================
% As a service to possible GUI's using the lxdata-object, some relevant properties are set below
% ==============================================================================================

% Define some DEFAULT SETTINGS for plotting etc.
% Use e.g.:  set(lx.gui.plotmat.linehandles,    lx.gui.plotstyles.NotSelected)
%            set(lx.gui.plotmat.linehandles(3), lx.gui.plotstyles.Selected)
%
% where "lx.gui.plotmat.linehandles" are set e.g. in the plot-function.
  lx.gui.plotstyles.Selected.Selected     = 'on';  % This property can e.g. be queried in "SelectLine_ButtonDownFcn" in the plot-method
  lx.gui.plotstyles.Selected.LineWidth    = 4;
  lx.gui.plotstyles.Selected.MarkerSize   = 6;
  lx.gui.plotstyles.Selected.MarkerEdgeColor = 'k';
  lx.gui.plotstyles.Selected.MarkerFaceColor = 'y';
  % etc.

  lx.gui.plotstyles.NotSelected.Selected   = 'off';
  lx.gui.plotstyles.NotSelected.LineWidth  = 0.5;
  lx.gui.plotstyles.NotSelected.MarkerSize = 4;
  lx.gui.plotstyles.NotSelected.MarkerEdgeColor = 'auto';
  lx.gui.plotstyles.NotSelected.MarkerFaceColor = 'none';
  % etc.

  lx.gui.ModelSettings = struct; % 05-05-2015 11:21:36

% Color varables
  White = [1  1  1];  % White
  BGColor = 0.9*White; % Light Grey - Background color
  % BGColor = [0.75 0.71 0.72];
  lx.gui.S.BackgroundColor = BGColor;

% Get user screen size
  SC = get(0, 'ScreenSize');
  MaxMonitorX = SC(3);
  MaxMonitorY = SC(4);

% Set the figure window size values
  MainFigScale = 0.82;          % Change this value to adjust the figure size
  MaxWindowX = round(MaxMonitorX*MainFigScale);
  MaxWindowY = round(MaxMonitorY*MainFigScale);
  XBorder = (MaxMonitorX-MaxWindowX)/2;
  YBorder = (MaxMonitorY-MaxWindowY)/2;

  lx.gui.FigurePosition = [XBorder, YBorder, MaxWindowX, MaxWindowY];

  set_selected(lx); % 22-09-2016 12:24:32

end % constructor (function lxdata)

% ---------------------------------------------------
function delete(lx)
  disp('% E.g. save lx in a MATLAB-file')
end % delete


% ---------------------------------------------------
function set_selected(lx)
% Set all "lx.selected<...>" properties (CR, 22-09-2016 12:04:57)

% Point to the correct matrix (n�dvendigt at l�gge f�rst s� rwmat er defineret!? CR, 22-09-2016 16:06:33)
  lx.IndexIntoDataSet = lx.Models(lx.selectedModelNum).IndexIntoDataSet;

% Set the transform (22-09-2016 11:52:01)
  for m = 2:-1:1
    lx.selectedMatNum = m;
%%%%% 26-09-2016 12:46:11
%%%%% lx.selectedTransform  = lx.Models(lx.selectedModelNum).Selected.Transform{lx.selectedMatNum}; % CR, 22-09-2016 14:48:00
  end

% Set the validation (22-09-2016 11:52:01)
  lx.selectedValidation = lx.Models(lx.selectedModelNum).Selected.Validation; % 22-09-2016 12:00:30

% Set the rest
  lx.selectedCalMethod        = lx.Models(lx.selectedModelNum).Type;
  lx.selectedMaxPC            = lx.Models(lx.selectedModelNum).Selected.MaxPC;
  lx.selectedOptimalDimension = lx.Models(lx.selectedModelNum).Selected.OptimalDimension;
  lx.selectedNumSegs          = lx.Models(lx.selectedModelNum).Selected.NumSegs;
  lx.selectedNumRandomRepeats = lx.Models(lx.selectedModelNum).Selected.NumRandomRepeats;
% lx.selectedCatVarNum        = ??

  lx.lxt                      = lx.Models(lx.selectedModelNum).lxt; % CR, 26-09-2016 15:15:27


end % set_selected

% ---------------------------------------------------
function set.selectedModelNum(lx,nr)

  if isempty(nr) % no models yet
      lx.selectedModelNum = 1;
      set_selected(lx)
  else
    % Check if lx.Models(nr) exist
      model_numbers = [lx.Models.Number];
      nr_idx = find(ismember(model_numbers,nr));

      if isempty(nr_idx) % ~ ismember(nr,model_numbers) % nr > numel(lx.Models)
         ME = MException('selectedModelNum:incorrectModelNumberSpecified', ...
                         'The model number does not exist!');
         throw(ME);
      else
        lx.selectedModelNum = nr_idx;
        set_selected(lx)
      end
  end

end % set.selectedModelNum

%     % ---------------------------------------------------
%     function set.selectedTransform(lx,stf)
%     % 21-09-2016 16:30:50
%     % Skal skrives om til lxt-objekter (27-09-2016 09:16:38)
%
%       stf = cellstr(stf);
%       if ~all(ismember(stf,removeampersand(lx.TransformTypes.transformDescriptions)))  % cat(1,stf{:})
%          ME = MException('selectedTransform:incorrectTransformationSpecified', ...
%                          'One or more of the transformations specified are unavailable or invalid!');
%          throw(ME);
%       else
%          lx.selectedTransform{lx.selectedMatNum} = stf;
%       end
%
%     % The code from "function lxchecktransformstring(lx)" are inserted here
%
%     % X and Xt shares transformations
%     % Y and Yt shares transformations
%
%
%       for m = 1:2
%         NewTransform = cellstr(lx.selectedTransform{m}); % The current transform settings. e.g. set by a GUI
%
%         % --------------------------
%         % Delete repeated selections
%         % --------------------------
%         if ~isempty(lx.AllowedRepeatedTransforms)
%           % S� skal der ca. "lige" skrives �n linie f�r og �n linie efter "unique"
%             A = ismember(NewTransform,lx.AllowedRepeatedTransforms); % ... (g�res ikke nu!)
%         end
%
%         NewTransform = unique(NewTransform,'stable'); % CR, 28-04-2015 09:09:15
%
%         % -------------------------------------------------------------------------------
%         % Select only one of "Autoscale", "Mean center", "Pareto scaling" a move the
%         % last selected to the end last position
%         % -------------------------------------------------------------------------------
%         [~,B] = ismember(lx.AppearOnlyOnceTransforms,NewTransform);
%         if any(B)
%           [~,idx] = max(B);
%           SelectedLast = lx.AppearOnlyOnceTransforms{idx};        % Find the last selected
%           A = ismember(NewTransform,lx.AppearOnlyOnceTransforms); % Find all AppearOnlyOnceTransforms
%           NewTransform(A) = '';                                   % ... and remove all appearences
%           NewTransform{end+1} = SelectedLast;                     % Insert the last selected at the last position
%         end
%         lx.selectedTransform{m} = NewTransform;                   % Save the ratified transform settings in the lx-object
%       % Match lx.selectedTransform to function names
%         if strcmp(NewTransform,'None')
%             lx.selectedTransFunc{m} = '';
%         else
%             tf_txt = '';
%             for i = 1:numel(NewTransform)
%               tf_txt = strvcat(tf_txt, lx.TransformTypes.availableTransform{strcmp(removeampersand(lx.TransformTypes.transformDescriptions), NewTransform{i})});
%             end
%             lx.selectedTransFunc{m} = cellstr(tf_txt);
%         end
%       end % for m = 1:2
%
%
%     % 22-09-2016 08:36:47
%
%     %%%%% ej kalde lxtransform, n�r der ikker bedt om det!?!? 23-09-2016 06:01:15
%     %%%%%  ST = dbstack;
%     %%%%%  if ~any(strcmp({ST.name},'lxdata.set_selected')) % Not Called from lxdata.set_selected
%     %%%%%     lxtransform(lx);  % Sets the lx.trmat property
%     %%%%%  end
%     %%%%%
%     % lx_oop, 28-04-2015 09:39:09
%     % Test e.g. med:
%     % lx.selectedMatNum = 1; lx.selectedTransform{lx.selectedMatNum} = {'Pareto scaling','Mean center','Pareto scaling','Standard Normal Variate (SNV)','Autoscale','Mean center'}
%     % lx.selectedMatNum = 2; lx.selectedTransform{lx.selectedMatNum} = {'MSC','BLAH-BLAH','Pareto scaling','Mean center'}
%
%
%     %Speak('Transformation has been changed')
%     %for i=1:numel(lx.selectedTransform{lx.Selected.MatNum})
%     %  Speak(char(lx.selectedTransform{lx.Selected.MatNum}{i}))
%     %end
%
%     % Call the plot-method of lxdata which reads the properties "lx.PlotTransformed", "lx.selectedTransform", etc.
%     end % set.selectedTransform


% ---------------------------------------------------
function set.selectedValidation(lx,valtype)
% 21-09-2016 16:11:18

% Set fields of GUI objects, e.g. listboxes, popup-menus, etc. in a GUI
% E.g. Switch the property 'Visible' between 'on' and 'off' in a GUI
% Use in a GUI, e.g. set(handles.panelSegments, lx.gui.panelSegments)
%
% For simplicity, the names are the ones used in LATENTIX pre-version 3

  if ~ismember(valtype,lx.Models(1).ValidationTypes)
    ME = MException('setValidation:incorrectValidationSpecified', ...
                    'The validation method specified is unavailable or mis-spelled!');
    throw(ME);
    return
  else
    lx.selectedValidation = valtype;
  end


  ModelSettings = lx.gui.ModelSettings; % Preserve old settings

  ModelSettings.popValidationMethod.Visible = 'on';
  ModelSettings.etMaxPC.Visible             = 'on';

  ModelSettings.panelSegments.Visible       = 'off';
  ModelSettings.panelRandomRepeats.Visible  = 'off';

  ModelSettings.panelCatVar.Visible         = 'off';
  ModelSettings.txtCatVar.Visible           = 'off';
  ModelSettings.txtCatVar.String            = '';


  switch valtype
      case 'No validation',
      case 'CV: Full',
      case 'Test set',
      case 'CV: Sets',

      case 'CV: Syst123',            ModelSettings.panelSegments.Visible    = 'on';

      case 'CV: Syst111',            ModelSettings.panelSegments.Visible    = 'on';

      case 'CV: Random',             ModelSettings.panelSegments.Visible    = 'on';

      case 'CV: Random (repeated)' , ModelSettings.panelSegments.Visible      = 'on';
                                     ModelSettings.panelRandomRepeats.Visible = 'on';

      case 'CV: Variable ...',       ModelSettings.txtCatVar.Visible   = 'on';
                                     ModelSettings.panelCatVar.Visible = 'on';

                                     %M�ske skal ca. dette g�res her:
                                     dim = 2; % Pick a variable as categoryvariable
                                     PickLabels = lx.data.Properties.UserData.OrigLabels{dim};
                                     if isfield(ModelSettings.txtCatVar,'UserData')
                                       InitialValue = ModelSettings.txtCatVar.UserData.LastIndex;
                                       InitialValue = min(InitialValue,numel(PickLabels));
                                     else
                                       InitialValue = 1; % min(lx.Model(lx.SelectedModel).CatVar.LastIndex,numel(PickLabels));
                                     end
                                     ListSize     = [200 600];
                                     SelectionMode = 'single';
                                     chosen = listdlg('PromptString','Select a category variable',...
                                                      'SelectionMode',SelectionMode,...
                                                      'InitialValue', InitialValue,...
                                                      'ListSize', ListSize,...
                                                      'ListString',PickLabels);

                                     if isempty(chosen)
                                         lx.selectedValidation = 'No validation';
                                     else
                                         %lx.Model(lx.SelectedModel).CatVar.Name = PickLabels{chosen}; % Nej, ikke her!
                                         %lx.Model(lx.SelectedModel).CatVar.LastIndex = chosen;
                                         ModelSettings.txtCatVar.String = PickLabels{chosen};
                                         ModelSettings.txtCatVar.UserData.LastIndex = chosen;
                                     end
                                     lx.selectedCatVarNum = chosen; % 07-05-2015 12:42:27


  end % switch selectedValidation

  lx.gui.ModelSettings = ModelSettings;

% Speak(['selectedValidation method has been changed to: ',lx.selectedValidation])

end % set.selectedValidation

% ---------------------------------------------------
function TTypes = getTransformTypes(lx)
% Read existing transformation types (CR, 27-09-2016 11:30:47)
  TTypes = lx.lxt.transformDescriptions;
end


% ---------------------------------------------------
function trmat = get.trmat(lx)
% Get the four currently selected TRANSFORMED matrices from lx.data
% The corresponding raw and un-transformed matrices is found in lx.rwmat
 %%% trmat = lxtransform(lx); % CR, 23-09-2016 06:00:21

% Transform the raw matrices, i.e. set the property "trmat"
% CR, 27-09-2016 17:27:16
  rwmat = lx.rwmat;
  for m = 1:2
    % X and Y
    if ~isempty(rwmat{m})
        trmat{m} = lx.lxt(m).calculateTransform(rwmat{m});
    else
        trmat{m} = [];
    end

    % Xt and Yt (use X and Y transformations)
    if ~isempty(rwmat{m+2})
        trmat{m+2} = lx.lxt(m).applyTransform(rwmat{m+2});
    else
        trmat{m+2} = [];
    end
  end

%%  trmat = lxCalcTransform(lx); % CR, 27-09-2016 08:35:34


end


% ---------------------------------------------------
function rwmat = get.rwmat(lx)
% Get the four currently selected RAW, i.e. un-transformed, matrices from lx.data
% Call lxtransform(lx) to set lx.trmat to the corresponding transformed forms
% CR, 30-04-2015 09:33:29 + 21-09-2016 14:57:54
  for m = 1:4
    rwmat{m} = lx.data{lx.IndexIntoDataSet{m, 1}, lx.IndexIntoDataSet{m, 2}};
  end
end

% ---------------------------------------------------
function curmat = get.curmat(lx)
% Get the currently selected raw matrix
% Can be used e.g. like: plot(lx.xaxis{dim},lx.curmat), where "dim" is the plot-direction (cf. lxdata/plot)end
  m = lx.selectedMatNum;
  curmat = lx.rwmat{m};
end

% ---------------------------------------------------
function xaxis = get.xaxis(lx)
% Can be used e.g. like: plot(lx.xaxis{dim},lx.curmat), where "dim" is the plot-direction (cf. lxdata/plot)end
  m = lx.selectedMatNum;
  xaxis  = {find(lx.IndexIntoDataSet{m, 1}), find(lx.IndexIntoDataSet{m, 2})};
end


function lx = set.MSCbase(lx,varargin)
% testing (21-09-2016 13:41:58)
  123
end % set.MSCbase

function fastplot(lx)
  figure
  subplot(211)
    plot(lx.xaxis{lx.selectedDim},lx.rwmat{lx.selectedMatNum})
    title('raw data')
    grid
    addcross;
  subplot(212)
    plot(lx.xaxis{lx.selectedDim},lx.trmat{lx.selectedMatNum})
    title('transformed data')
    grid
    addcross;

end


end % methods



end % classdef





%    % The field "Sets"
%      if isempty(LXny.Sets)
%        LXny.Sets = cell(1,D);
%        DefSets = {'All objects', 'All variables'};
%        for d = 1:length(DefSets)
%          LXny.Sets{d}.Lab = DefSets{d};
%         %LXny.Sets{d}.Idx = logical(ones(size(LXny.data,d),1));
%          LXny.Sets{d}.Idx = true(size(LXny.data,d),1);
%        end
%      end
%
%    % The field SetStyle
%      if ~isempty(LXny.SetStyle(1).Label)  % If not empty, some sets exist (e.g. old data) but the SetStyle field
%                                           % might not has been set, This is done now (backwards compatibility).
%                                           %
%                                           % If, on the other hand, no sets exist, the SetType-field is set in UpdateSetStyles
%                                           % from 29-06-2006 and onwards.
%        for d = 1:D
%          if isempty(LXny.SetStyle(d).SetType)
%
%            for i = 1:length(LXny.SetStyle(d).Label)
%              if strmatch(LXny.SetStyle(d).Label(i),'All objects','exact') | strmatch(LXny.SetStyle(d).Label(i),'All variables','exact')
%                ST = 0; % Don't show in edit style gui (tror aldrig at man kommer herind!)
%              else
%                ST = 1; % Show in edit style gui
%              end
%              LXny.SetStyle(d).SetType{i} = ST;
%            end % for i = 1:length(LXny.SetStyle(d).Label)
%
%          end % if isempty(LXny.SetStyle(d).SetType)
%        end % for d = 1:D
%      end % if ~isempty(LXny.SetStyle(d).Label)