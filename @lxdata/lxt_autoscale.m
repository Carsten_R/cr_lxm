function varargout = lxt_autoscale(lx, varargin)
% LXT_AUTOSCALE Autoscale a matrix (an lxdata-method)
%
% Perform mean centering and scaling to unit variance of data
%
%  I/O:
%
%  Transformation info:                 trf_info = lxt_autoscale(lx)
%
%  Calculate transformation:    [Xtr, TransData] = lxt_autoscale(lx, X, params) or:
%                               [Xtr, TransData] = lxt_autoscale(lx, X)
%
%  Use transformation:                       Xtr = lxt_autoscale(lx, X, TransData)
%
%  Carsten Ridder, 20-09-2016




% =======================================================
%
% The function name shall beging with the prefix "lxt_"
%
% Edit these lines:

  trf_info.transformDescriptions = '&Autoscale';   % Descriptive name optionally including "&", i.e. which letter to underscore in a GUI

  trf_info.allowRepeated = false;

  trf_info.exclusive = true;           % Allow only one transformation tagged as "exclusive" in a transform-sequence
                                       % For now these are exclusive: 'Mean center', 'Autoscale' and 'Pareto scaling'

  trf_info.helptext  = char('Autoscaling subtracts from each element in a coloumn the coloumn mean and divide by the coloumn standard deviation.', ...
                            '... can be continued ...');

% Also write/edit these three functions below:
%
%      calculateTransform
%      applyTransform
%      canApply
%
% =======================================================


% DO NOT EDIT HERE:

  funcname = getfuncname;
  trf_info.availableTransform = funcname;    % e.g. 'lxt_autoscale'; the transformation method file name

  nvarin  = numel(varargin);
  nvarout = nargout;

% Handling if params are given in input or not (case: "Calculate transformation"):
  if nvarout == 2

    if nvarin == 2             % [Xtr, TransData] = lxt_autoscale(lx, X, params)
        params = varargin{2};
    elseif nvarin == 1         % [Xtr, TransData] = lxt_autoscale(lx, X)
        params = [];
        nvarin = 2;
    end

  end

      if nvarin == 0,                                 varargout{1} = trf_info;                                              % Set relevant information only,                 e.g. trf_info = lxtransform(lx);
  elseif nvarout == 2 && nvarin == 2, [varargout{1}, varargout{2}] = calculateTransform(lx, varargin{1}, params, funcname); % Calculate the transform and set the TransData, e.g. [X, TransData] = lxt_autoscale(lx, X, params)
  elseif nvarout == 1 && nvarin == 2,                 varargout{1} = applyTransform(lx, varargin{1}, varargin{2});          % Apply a previously calculated transform,       e.g. X = lxt_autoscale(lx, X, TransData);
  else
    ME = MException([funcname,':inputArguments'], 'Wrong number of output and input-arguments (o,u): (1,1), (1,3), (2,2) or (2,3) or  allowed'); throw(ME);
  end


% =======================================================
% Edit the three functions below
% =======================================================


% ------------------------------------------------
function [Xtr, TransData] = calculateTransform(lx, X, params, funcname)
% Mean center input data using coloumns means of X
% "lx" and "params" not used here

  TransData.funcname = funcname; % do not edit
  TransData.params   = params;   % do not edit

% Calculate transformation
  mx  = mean(X,1,'omitnan');
  sx  = std(X,0,1,'omitnan');
  Xtr = bsxfun(@rdivide,bsxfun(@minus,X,mx),sx);

% Set specific fields
  TransData.mx = mx;
  TransData.sx = sx;


% ------------------------------------------------
function Xtr = applyTransform(lx, X, TransData)
% Mean center input data using previously calculated mean

  if canApply(X, TransData)
     Xtr = bsxfun(@rdivide,bsxfun(@minus, X, TransData.mx), TransData.sx);
  else
     Xtr = [];
  end


% -------------------------------------------------
function OK = canApply(X, TransData)
% Perform check on input data

  if isempty(TransData.mx) | isempty(TransData.sx)
     OK = false;
     ME = MException('lxt_autoscale:applyTransform:cannotApplyTransform', ...
                     'Transformation has not previously been calculated!');
  elseif ~isequal(size(X,2),length(TransData.mx))
     OK = false;
     ME = MException('lxt_autoscale:applyTransform:incorrectMeanVectorLength', ...
                     'Mean vector length (%d) does not match number of columns in data (%d)!',length(TransData.mx),size(X,2));
  elseif ~isequal(size(X,2),length(TransData.sx))
     OK = false;
     ME = MException('lxt_autoscale:applyTransform:incorrectMeanVectorLength', ...
                     'Standard deviation vector length (%d) does not match number of columns in data (%d)!',length(TransData.sx),size(X,2));
  else
     OK = true;
  end

  if ~OK && ~isdeployed
     throw(ME)
  end
