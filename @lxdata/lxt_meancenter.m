function varargout = lxt_meancenter(lx, varargin)
% LXT_MEANCENTER Mean center a matrix (an lxdata-method)
%
% Perform mean centering of data over the first array dimension.
%
%  I/O:
%
%  Transformation info:                 trf_info = lxt_meancenter(lx)
%
%  Calculate transformation:    [Xtr, TransData] = lxt_meancenter(lx, X, params) or:
%                               [Xtr, TransData] = lxt_meancenter(lx, X)
%
%  Use transformation:                       Xtr = lxt_meancenter(lx, X, TransData)
%
%  Carsten Ridder, 19-09-2016
%
%
%
% How to test the function from e.g. the command line
% ===================================================
%
% Create an empty (dummy) instance of an lxdata-object:
%
%    lx = lxdata;
%
% Fetch info about the transformation (one input and one output argument):
%
%    trf_info = lxt_meancenter(lx)                                   <------
%
%    trf_info =
%
%        showname: 'Mean &center'
%       exclusive: 1
%        helptext: [3x87 char]
%        funcname: 'lxt_meancenter'
%
% Create a dummy test matrix "X":
%
%    X = [0.6476    0.7093    0.4587
%         0.6790    nan       0.6619
%         0.6358    0.1194    0.7703
%         0.9452    0.6073    nan
%         0.2089    0.4501    0.6620];
%
%
% Mean-center X and retrieve the mean vector (two or three input and two output arguments):
%
%    [Xtr, TransData] = lxt_meancenter(lx, X)                        <------
%    [Xtr, TransData] = lxt_meancenter(lx, X, [])                    <------
%
%    Xtr =
%
%        0.0243    0.2378   -0.1795
%        0.0557       NaN    0.0237
%        0.0125   -0.3521    0.1321
%        0.3219    0.1358       NaN
%       -0.4144   -0.0214    0.0238
%
%
%    TransData =
%
%        funcname: 'lxt_meancenter'
%          params: []
%              mx: [0.6233 0.4715 0.6382]
%
%
%    CHECK: mean(Xtr,1,'omitnan')  =     1.0e-16 *
%                                        -0.6661    0.2776   -0.2776
%
%
% Mean-center X using the previously calculated mean vector (three input and one output arguments):
%
%     Xny = lxt_meancenter(lx, X, TransData)                         <------
%
%     Xny =
%
%         0.0243    0.2378   -0.1795
%         0.0557       NaN    0.0237
%         0.0125   -0.3521    0.1321
%         0.3219    0.1358       NaN
%        -0.4144   -0.0214    0.0238
%
%    isequalwithequalnans(Xtr,Xny)
%
%      ans = 1
%
%
% CR, 19-09-2016 11:47:04


% =======================================================
%
% The function name shall beging with the prefix "lxt_"
%
% Edit these lines:

  trf_info.transformDescriptions = 'Mean &center'; % Descriptive name optionally including "&", i.e. which letter to underscore in a GUI

  trf_info.allowRepeated = false;

  trf_info.exclusive = true;           % Allow only one transformation tagged as "exclusive" in a transform-sequence
                                       % For now these are exclusive: 'Mean center', 'Autoscale' and 'Pareto scaling'

  trf_info.helptext  = char('Mean centering subtracts the coloumn mean from each element in the coloumn.', ...
                            'This corresponds to moving the origo of the coordinate system to the center of the data', ...
                            '... can be continued ...');

% Also write/edit these three functions below:
%
%      calculateTransform
%      applyTransform
%      canApply
%
% =======================================================


% DO NOT EDIT HERE:

  funcname = getfuncname;
  trf_info.availableTransform = funcname;    % e.g. 'lxt_meancenter'; the transformation method file name

  nvarin  = numel(varargin);
  nvarout = nargout;

% Handling if params are given in input or not (case: "Calculate transformation"):
  if nvarout == 2

    if nvarin == 2             % [Xtr, TransData] = lxt_meancenter(lx, X, params)
        params = varargin{2};
    elseif nvarin == 1         % [Xtr, TransData] = lxt_meancenter(lx, X)
        params = [];
        nvarin = 2;
    end

  end

      if nvarin == 0,                                 varargout{1} = trf_info;                                              % Set relevant information only,                 e.g. trf_info = lxtransform(lx);
  elseif nvarout == 2 && nvarin == 2, [varargout{1}, varargout{2}] = calculateTransform(lx, varargin{1}, params, funcname); % Calculate the transform and set the TransData, e.g. [X, TransData] = lxt_meancenter(lx, X, params)
  elseif nvarout == 1 && nvarin == 2,                 varargout{1} = applyTransform(lx, varargin{1}, varargin{2});          % Apply a previously calculated transform,       e.g. X = lxt_meancenter(lx, X, TransData);
  else
    ME = MException([funcname,':inputArguments'], 'Wrong number of output and input-arguments (o,u): (1,1), (1,3), (2,2) or (2,3) or  allowed'); throw(ME);
  end


% =======================================================
% Edit the three functions below
% =======================================================


% ------------------------------------------------
function [Xtr, TransData] = calculateTransform(lx, X, params, funcname)
% Mean center input data using coloumns means of X
% "lx" and "params" not used here

  TransData.funcname = funcname; % do not edit
  TransData.params   = params;   % do not edit

% Calculate transformation
  mx  = mean(X,1,'omitnan');
  Xtr = bsxfun(@minus, X, mx);

% Set specific fields
  TransData.mx = mx;


% ------------------------------------------------
function Xtr = applyTransform(lx, X, TransData)
% Mean center input data using previously calculated mean

  if canApply(X, TransData)
     Xtr = bsxfun(@minus, X, TransData.mx);
  else
     Xtr = [];
  end


% -------------------------------------------------
function OK = canApply(X, TransData)
% Perform check on input data

  if isempty(TransData.mx)
     OK = false;
     ME = MException('lxt_meancenter:applyTransform:cannotApplyTransform', ...
                     'Transformation has not previously been calculated!');
  elseif ~isequal(size(X,2),length(TransData.mx))
     OK = false;
     ME = MException('lxt_meancenter:applyTransform:incorrectMeanVectorLength', ...
                     'Mean vector length (%d) does not match number of columns in data (%d)!',length(TransData.mx),size(X,2));
  else
     OK = true;
  end

  if ~OK && ~isdeployed
     throw(ME)
  end
