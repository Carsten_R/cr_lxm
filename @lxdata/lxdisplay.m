function lxdisplay(lx, varargin)
% DISP Display method for lxdata
%
% lxdisplay(lx)                                              displays this help text
% lxdisplay(lx, '')                                          displays both data, models and predictions in a new window
% lxdisplay(lx, uitblEditor, 'Data');                        attaches lx.data to a uitable-handle
% lxdisplay(lx, {uitblModels, uitblPredictions}, 'Model')    attaches lx.Models and lx.Predictions to two uitable-handles
%
% CR, 15-05-2015 09:42:09

  if nargin == 1
    help lx/lxdisplay
    return
  end

  if iscell(varargin{1})
    uitbl_exist = isa(varargin{1}{1},'matlab.ui.control.Table');
  else
    uitbl_exist = isa(varargin{1},'matlab.ui.control.Table');
  end

% Find location on which to display the data
  if uitbl_exist
    uitbl_parent = varargin{1};
    switch varargin{2}
      case 'Data',  DisplayData(lx, uitbl_parent)
      case 'Model', DisplayModelsAndPredictions(lx, uitbl_parent)
    end
  else
    % Create a figure and display both data, models and predictions
    [uitblEditor, uitblModelPred] = create_uitbl_handles(lx);
    DisplayData(lx, uitblEditor)
    DisplayModelsAndPredictions(lx, uitblModelPred)
  end

% --------------------------------------------------------------------
function DisplayData(lx, uitblEditor)
% 06-05-2015 10:22:05

% Extract the selected matrix and display it in the table editor
  m = lx.selectedMatNum;
  uitblEditor.Data       = lx.data{lx.IndexIntoDataSet{m, 1}, lx.IndexIntoDataSet{m, 2}};;
  uitblEditor.RowName    = lx.data.Properties.UserData.OrigLabels{1}(lx.IndexIntoDataSet{m,1});
  uitblEditor.ColumnName = lx.data.Properties.UserData.OrigLabels{2}(lx.IndexIntoDataSet{m,2});

  uitblEditor.Parent.Title = ['Data [',int2str(sum(lx.IndexIntoDataSet{m,1})),' x ',int2str(sum(lx.IndexIntoDataSet{m,2})),']'];


% --------------------------------------------------------------------
function DisplayModelsAndPredictions(lx, uitblModelPred)
% 11-05-2015 12:03:28

  uitblModels       = uitblModelPred{1};
  uitblPredictions  = uitblModelPred{2};

  [T, TableHeader, ColumnName, RowName]  = deal([]);
  for i = 1:numel(lx.Models)
      if isempty(lx.Models(i).Number), continue, end % No models found

      K = sum(lx.Models(i).IndexIntoDataSet{2,2}); % Number of variables in Ycal-matrix (always matrix number two (2))

      k = 0;
      k = k + 1; T{i,k} = lx.Models(i).Number; TableHeader{k} = 'No';
      k = k + 1; T{i,k} = lx.Models(i).Type;   TableHeader{k} = 'Type';

      PC = lx.Models(i).Selected.OptimalDimension;

      k = k + 1; T{i,k} = PC; TableHeader{k} = 'PC';  % PC can be edited in the uitable % PC
      k = k + 1; T{i,k} = num2str(lx.Models(i).Data.output.expvarXcal(PC+1),'%5.1f'); TableHeader{k} = 'expvarXcal'; % NOTE PC + 1, as the first element is for PC = 0 % expvarXcal
      k = k + 1; T{i,k} = num2str(lx.Models(i).Data.output.expvarXval(PC+1),'%5.1f'); TableHeader{k} = 'expvarXval'; % expvarXval

     if K > 0
       k = k + 1; T{i,k} = num2str(lx.Models(i).Data.output.expvarYcal(PC+1),'%5.1f');  TableHeader{k} = 'expvarYcal'; % expvarYcal
       k = k + 1; T{i,k} = num2str(lx.Models(i).Data.output.expvarYval(PC+1),'%5.1f');  TableHeader{k} = 'expvarYval'; % expvarYval
     else
       k = k + 1; T{i,k} = ' '; TableHeader{k} = 'expvarYcal';
       k = k + 1; T{i,k} = ' '; TableHeader{k} = 'expvarYval';
     end
     k = k + 1; T{i,k} = lx.Models(i).Name; TableHeader{k} = 'Model name';
  end
  uitblModels.Data = T;
  uitblModels.ColumnName = TableHeader; %   uitblModels.ColumnName = lx.Models(1).TableHeader;
  uitblModels.RowName = [];

% uitblModels.Parent.Title = ['Data [',int2str(sum(lx.IndexIntoDataSet{m,1})),' x ',int2str(sum(lx.IndexIntoDataSet{m,2})),']'];

% Tilf�j
% uitblPredictions. ...


% --------------------------------------------------------------------
function [uitblEditor, uitblModelPred] = create_uitbl_handles(lx);
% Create a simple GUI in a new figure
%
% CR, 15-05-2015 10:44:33

  tit = ['[',fullfile(lx.pathname,lx.filename),']  [',lx.data.Properties.Description,']   [',datestr(now),']'];
  uitbl_figure = figure('NumberTitle','off','Name',tit,'Tag','lxdata_disp_figure'); % E.g. delete(findobj('Tag','lxdata_disp_figure'))


  vbox = uiextras.VBoxFlex('Parent',uitbl_figure);

    panelEditor = uiextras.Panel('Parent', vbox,'Title','Data','BorderType','none','Padding',10);
        uitblEditor = uitable('Parent', panelEditor, 'Tag', 'uitblEditor', lx.gui.S);

    panelModels = uiextras.Panel('Parent', vbox,'Title','Models','BorderType','none','Padding',10);
        uitblModels = uitable('Parent', panelModels,'Tag','uitblModels',lx.gui.S);

    panelPredictions = uiextras.Panel('Parent', vbox,'Title','Predictions','BorderType','none','Padding',10);
        uitblPredictions = uitable('Parent', panelPredictions,'Tag','uitblPredictions',lx.gui.S);

  uitblModelPred = {uitblModels, uitblPredictions};

