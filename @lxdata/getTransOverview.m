function [PX, PY] = getTransOverview(lxt)
% Hjælpefunktion til udviklingsarbejdet af oop-LX
%
% E.g. [PX, PY] = getTransOverview(lx.lxt);
%      [PX, PY] = getTransOverview(lxt_segs);
%
%
% E.g. [PX, PY] = getTransOverview(lx.lxt);
%
%    PX{1} =
%
%        meanVector: [-0.6603 0.8966 -0.6180 1.1326 -0.6084 -0.1425]
%
%
%
%    PY{1} =
%
%        meanVector: [-0.7071 0.7071]
%         stdVector: [2.5138e-08 2.5138e-08]
%
% CR, 28-09-2016 09:26:27

% X_overview = lxt(1).getTransformationInformation;
  [PX, PY] = deal(cell(1));
  PX = extract_D(lxt(1)); % , lxt(1).getTransformationInformation)
  if numel(lxt) > 1
    PY = extract_D(lxt(2)); % ,lxt(2).getTransformationInformation;
  end

  celldisp(PX)
  celldisp(PY)

% ---------------------------------------
function P = extract_D(lxt)

  D = lxt.getTransformationInformation;

  P = cell(1);
  tl = 0;
  for i = 1:numel(D)
    if isstruct(D{i})
      tl = tl + 1;
      P{tl} = D{i};
      %FN = fieldnames(D{i});
      %for f = 1:numel(FN)
      %  fn = matlab.lang.makeValidName([lxt.selectedTransform{i},'_',FN{f}]);
      %  D{i}.(FN{f})(:)
      %end
    end
  end


