function [output, Jnovar, MatNum, dim, showtxt] = lxrepeatedcv(lx, NumRuns);
% Calculate the mean of all fields for all PC's by repeated random cross validation
% v2.00, 07-01-2008 15:28
% oop 08-05-2015 16:08:53

  output  = [];
  showtxt = '';
  Jnovar  = [];
  MatNum  = [];
  dim     = [];

% Get the selected calibration and validation methods
  CalMethod = lx.selectedCalMethod; % oop      % CalMethod = lxpopupstr(handles.popModelType);
  ValMethod = lx.selectedValidation; % oop      % ValMethod = lxpopupstr(handles.popValidation);

% Get the selected number of xval-segments
  NumSegs = lx.selectedNumSegs; % oop          % NumSegs = str2num(get(handles.etNumSegs,'String'));

% Get the selected number of PC's
  MaxPC = lx.selectedMaxPC; % oop              % MaxPC = str2num(get(handles.etMaxPC,'String'));

  hWait = waitbar(0,'...','name','Repeated random cross-validation in progress ...','CreateCancelBtn','closereq','Tag','LXwaitbar');
  show_wait_bar_in_lxcalculate = false;
  for randrun = 1:NumRuns

      if ishandle(hWait)
        waitbar(randrun/NumRuns,hWait,['Repetition ',int2str(randrun),'/',int2str(NumRuns)])
      else
        output  = []; % 09-01-2008 11:16
        return
      end

      [output, Jnovar, MatNum, dim, showtxt] = lxcalculate(lx, CalMethod, ValMethod, NumSegs, MaxPC, show_wait_bar_in_lxcalculate, NumRuns);

      statval = output.MeasPred.stats.val;

    % Initialization
      if randrun == 1

        I = size(output.SegMat,1);
        NumPCinclZero = output.MaxPC+1;

        FN = fieldnames(statval);
        NumRows = length(cat(1,statval.(FN{1})));
        SUMmat  = zeros(NumRows,length(FN));
        SUMmat2 = zeros(NumRows,length(FN));
        MINmat  =  Inf*ones(NumRows,length(FN));
        MAXmat  = -Inf*ones(NumRows,length(FN));

        RMSEpos = find(strcmp('RMSE',FN)); % Field number of 'RMSE'

        YPREDVAL = NaN*ones(I,NumPCinclZero,NumRuns); % v2.00, 19-01-2008 17:10
      % Save all predictions for each object for each PC and for each run
      % v2.00 19-01-2008 17:15
      % YPREDVAL

      % The difference between predictions when i CAL and VAL set respectively. OVERFL�DIG!!
      % Om man tr�kker CAL eller en gammel ostemad fra, f�r man samme spredning som p� YPREDVAL!
      % Calculate standard deviations over randrun's and save in a matrix the
      % same size as e.g. the Scores-matrix

        OptPC = zeros(NumPCinclZero,1);

      end

      mat = NaN*ones(NumPCinclZero,length(FN));
      for f = 1:length(FN)
        temp = cat(1,statval.(FN{f}));
        mat(:,f) = temp;
      end
      SUMmat  = SUMmat  + mat;      % For calculation of mean and standard deviation
      SUMmat2 = SUMmat2 + mat.^2;   % For calculation of standard deviation

      MINmat  = min(MINmat,mat);
      MAXmat  = max(MAXmat,mat);

%     YPREDVAL(:,:,randrun) = reshape(output.MeasPred.YpredVal,I,NumPCinclZero,1) - repmat(output.MeasPred.Yval,1,NumPCinclZero);
      YPREDVAL(:,:,randrun) = reshape(output.MeasPred.YpredVal,I,NumPCinclZero,1); % OBS! Tr�k ikke ...Yval fra mere!! (20-01-2008 16:22)

      [LocMinVal, LocMinIdx] = lxlocmin(mat(:,RMSEpos)); % 20-01-2008 12:03
      OptPC(LocMinIdx) = OptPC(LocMinIdx) + 1;

  end % for randrun = 1:NumRuns

% 20-01-2008 13:41
  avgYpredVal = mean(YPREDVAL,3);
% stdYpredVal =  std(YPREDVAL,0,3);

% output.Model.stdYpredVal = stdYpredVal(:,2:end); % Vil gerne farvel�gge med denne i f.eks. Act vs Pred plottet (20-01-2008 13:39)
                                                   % Lige nu kan det m�ske komme ind i Scores-plottet
                                                   % Den kan vist bruges i alle plots. 24-01-2008 12:50

% Replacing YpredVal from latest model run with the mean values
  yvarno = 1; % "repeated-cv" is only valid for one Y-variable
  for a = 1:NumPCinclZero
    output.MeasPred.YpredVal(:,yvarno,a)    = avgYpredVal(:,a); % 20-01-2008 13:38
  end

% Calculate standard deviations in YPREDdiffVALCAL over randrun's and save in a [I, NumPC]-matrix, i.e the
% same size as e.g. the scores-matrix. Make it available in the plot-menu.
% 19-01-2008 18:06

  if ishandle(hWait)
    close(hWait);
  else
    output = [];
    return;
  end

  MEANmat = SUMmat/NumRuns;
  STDmat  = sqrt((SUMmat2 - SUMmat.^2/NumRuns)/(NumRuns-1));
  STDmat  = real(STDmat); % 08-01-2008 17:00 (problemer med WARNINGS i plottemplate)

  RMSEpos = find(strcmp('RMSE',FN)); % Field number of 'RMSE'

%  Npos          = find(strcmp('N',FN));
%  MeanValpos    = find(strcmp('MeanVal',FN));
%  RangeMeanpos  = find(strcmp('RangeMean',FN));
%  Rangepos      = find(strcmp('Range',FN));

% Insert 'std(RMSEPCV)','min_RMSE' and 'max_RMSE' at top of FN
  FN = cellstr(strvcat('std_RMSE','min_RMSE','max_RMSE',char(FN)));

  JumpOver = {find(strcmp('N',FN)), find(strcmp('MeanVal',FN)), find(strcmp('RangeMean',FN)), find(strcmp('Range',FN))};

% Insert a dummy coloumns so fields and results still match
  MEANmat = [NaN*zeros(NumRows,1), NaN*zeros(NumRows,1), NaN*zeros(NumRows,1), MEANmat];

  OptPC = 100*OptPC/sum(OptPC); % Percentage of occurencies of minimum RMSEPCV


% Put MEANmat into output.MeasPred.stats.val
  clear statval
  for f = 1:length(FN)
    for a = 1:NumRows
      statval(a).OptPC = OptPC(a); % Add to the top
      switch f
        case 1,        statval(a).(FN{f}) = STDmat(a,RMSEpos);
        case 2,        statval(a).(FN{f}) = MINmat(a,RMSEpos);
        case 3,        statval(a).(FN{f}) = MAXmat(a,RMSEpos);
        case JumpOver, statval(a).(FN{f}) = MEANmat(a,f);       % Do not change these field names => will be removed in latentix.m
      otherwise
                       statval(a).(['avg_',FN{f}]) = MEANmat(a,f);     % Add "avg_" as prefix
      end
    end
  end

  output.MeasPred.stats.val = statval;

%return
%
%  if isempty(Output)
%    Output.labels = strvcat('RMSEP mean',
%                            'RMSEP min',
%                            'RMSEP max',
%                            'RMSEP std');
%  end
%
%
%
%  stdRMSEP=[0 std(RMSEP(:,2:MaxFak+1))];
%  [val,idx]=min(stdRMSEP(2:end));
%  errorbar(0:MaxFak,RMSEPTOT,-2*stdRMSEP,2*stdRMSEP)
%  set(gca,'NextPlot','add')
%  plot(idx,RMSEPTOT(idx+1),'r*')
%  set(gca,'NextPlot','replace')
