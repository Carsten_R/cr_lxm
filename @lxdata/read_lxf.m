function read_lxf(lx,filename)
% READ_LXF Read pre-version 3.0 LATENTIX-files
%
% Call this function from the lxdata-constructor
%
% Brug som eksempel evt.
%  P = load('C:\Users\cr\OneDrive - ridder\CR\ridder.xyz\Projects\LatentiX\kode\Ver3.0 oop\Test data\BlodParametre_sets.lxf','-mat');
%  P = load('BlodParametre_sets.lxf','-mat');
%  P.DataSet.Title = 'Islandske heste';
%
%  Samlet:
%  clear all, P = load('BlodParametre_sets.lxf','-mat'); DataSet = P.DataSet; clear P, DataSet.Title = 'Islandske heste'; lx = lxdata(DataSet);
%  lx.data.Age_group = categorical(lx.data.Age_group,[1,2,3],{'young','mid-age','old'},'Ordinal',true);
%
%  DataSet =
%
%           Title: ''                               -> lx.data.Properties.Description
%            data: [81x19 double]                   -> lx.data
%          Labels: {[81x7 char]  [19x15 char]}      -> cf. note A
%            Sets: {[1x1 struct]  [1x1 struct]}
%        SetStyle: [1x2 struct]
%          Models: {[1x1 struct]}
%     Predictions: []
%             Rep: {[81x1 double]  [19x1 double]}
%
%  Note A:  DataSet.Labels{1} -> lx.data.Properties.RowNames               (changed to unique and valid MATLAB-names)
%                                lx.data.Properties.UserData.OrigLabels{1} (original object names)
%
%           DataSet.Labels{2} -> lx.data.Properties.VariableNames           (changed to unique and valid MATLAB-names)
%                                lx.data.Properties.UserData.OrigLabels{2} (original variable names)
%
%
% E.g.
%
%  lx.data.Properties =
%
%               Description: 'Kendt blod'
%      VariableDescriptions: {}
%             VariableUnits: {}
%            DimensionNames: {'Objects'  'Variables'}
%                  UserData: [1x1 struct]
%                  RowNames: {8x1 cell}
%             VariableNames: {1x45 cell}
%
%  lx.data.Properties.UserData =
%
%                OrigLabels: {{8x1 cell}  {45x1 cell}}
%
% CR, 30-04-2015 10:32:56

  if isempty(filename)
    FileFilter = {'*.lxf','LatentiX-files pre-V3 (*.lxf)'};
    [filename,pathname] = uigetfile(FileFilter,'Select file','MultiSelect','off');
    if isnumeric(filename) & isnumeric(pathname)  % Cancel pressed
      return;
    else
      load(fullfile(pathname,filename),'-mat')
    end

  else
    load(filename,'-mat')
    [pathname,filename] = fileparts(filename);
  end

  lx.filename = filename;
  lx.pathname = pathname;

% Replace with uigetfile ...
    % load('C:\CR\LatentFive\Testdata\Greenpeace\Blodanalyser.lxf','-mat');
% load('C:\CR\LatentFive\Ver3.0 oop\Test data\H0_GroupMeans_and_Individual_PLS_paper.lxf','-mat');
% load('BlodParametre_sets.lxf','-mat');
% load('Foulum 1999-2009.lxf','-mat');
% load('C:\aRod\EU-harmoniseret forbrugerprisindeks 2009.lxf','-mat');  % TEST AF "MISSING DATA"
% load('C:\CR\LatentFive\Testdata\McDonalds.lxf','-mat');



  lx.data = array2table(DataSet.data);

% Create "unique" identifiers for objects and variables (07-11-2016 15:40:10)
  [I, J] = size(lx.data);
  lx.dataID = cell(1,2);
  IDprefix = datestr(now,'yyyymmddHHMMSS');
  lx.dataID{1} = [repmat([IDprefix,'.'],I,1) num2str([1:I]','%-15.0f')];
  lx.dataID{2} = [repmat([IDprefix,'.'],J,1) num2str([1:J]','%-15.0f')];

  lx.data.Properties.Description = DataSet.Title; % The old "Title": Char array used for naming the data set
  lx.data.Properties.DimensionNames = {'Objects' 'Variables'};

  lx.data.Properties.RowNames      = matlab.lang.makeUniqueStrings(matlab.lang.makeValidName(strrep(cellstr(DataSet.Labels{1}),' ','_'))); % changed to unique and valid MATLAB-names
  lx.data.Properties.VariableNames = matlab.lang.makeUniqueStrings(matlab.lang.makeValidName(strrep(cellstr(DataSet.Labels{2}),' ','_'))); % changed to unique and valid MATLAB-names

  for dim = 1:2
    lx.data.Properties.UserData.OrigLabels{dim} = cellstr(DataSet.Labels{dim}); % Save the original object names in UserData; these can be invalid MATLAB variable names
  end

  lx.Sets     = DataSet.Sets;     % <-- Object sets should maybe be integrated in lx.data? Variable sets can NOT be integrated. (CR, 30-04-2015 10:31:23)
  lx.SetStyle = DataSet.SetStyle; % <--

  lx.Predictions = DataSet.Predictions; % Hills, 12-05-2015 20:40:59

%% Set the fields of lxm.Data
%  FN = fieldnames(lxm.Data);
%  for i = 1:length(FN)
%    if isfield(lxm,FN{i})
%      lxm.Data.(FN{i}) = lxm.(FN{i});
%    end
%  end

  lxm = lxmodel;
  FN_Selected = fieldnames(lxm.Selected);
  FN_Data     = fieldnames(lxm.Data);


% Transfer the models to an array of lxmodel's (CR, 06-05-2015 17:49:27)
  for i = 1:numel(DataSet.Models)
      lxm(i).Number      = DataSet.Models{i}.Number;
      lxm(i).Name        = DataSet.Models{i}.Name;
      lxm(i).Comment     = DataSet.Models{i}.Comment;
      lxm(i).DataSetName = DataSet.Models{i}.DataSetName;
      lxm(i).Type        = DataSet.Models{i}.Type;

    % Hvor skal der oprettes felter i SetStyleTable? (CR, 07-11-2016 06:52:15)
    % SetStyleTable: setid	dim	           Label	          Marker	      MarkerSize	           Color	       LineStyle	       LineWidth

    % Her skal der oprettes en guid for alle objekter og variable i "lxdata.data" og oprettes
    % en tabel (navn?), med de obj. og var. der indg�r i modellen (CR, 07-11-2016 06:55:16)
    % Transfer IndexIntoDataSet
      for m = 1:size(DataSet.Models{i}.IndexIntoDataSet,1) % X, Y, Xt, Yt
        for d = 1:2   % objects, variables
          lxm(i).IndexIntoDataSet{m,d} = logical(DataSet.Models{i}.IndexIntoDataSet{m,d});
        end
      end

      FN_Models_p3 = fieldnames(DataSet.Models{i});
      FN_ModelData_p3 = fieldnames(DataSet.Models{i}.ModelData);

    % Transfer ModelData (P, T, ...)
      for j = 1:numel(FN_Data)
        FoundIdx = strmatch(FN_Data{j},FN_ModelData_p3,'exact');
        if ~isempty(FoundIdx)
          lxm(i).Data.(FN_Data{j}) = DataSet.Models{i}.ModelData.(FN_ModelData_p3{FoundIdx});
        else
         %disp([int2str(i),'/',int2str(j),': ',FN_Data{j},' not found in p3_models']) % 'StdXres' findes kun ved PLS? (22-09-2016 09:46:42)
        end
      end

    % Transfer settings (MaxPC, Transform, ...)

%    % But first change som names ...
%       % Change 'SNV' to ca. 'Standard Normal Variate (SNV)' (22-09-2016 11:24:49)
%         for m=1:2 % ikke 4! (27-09-2016 11:57:00)
%           idx = strcmpi('SNV',cellstr(DataSet.Models{i}.Transform{m}));
%           if ~any(idx), continue, end
%           correct_string = removeampersand(lx.lxt(m).transformDescriptions{~cellfun(@isempty,strfind(lx.lxt(m).transformDescriptions,'SNV'))});
%           TT = cellstr(DataSet.Models{i}.Transform{m});
%           TT{idx} = char(correct_string);
%           DataSet.Models{i}.Transform{m} = char(TT);
%         end
%
%       % Change 'MSC' to ca. 'Multiplicative Signal Correction (MSC) ...' (22-09-2016 11:24:53)
%         for m=1:2 % ikke 4! (27-09-2016 11:57:00)
%           idx = strcmpi('MSC',cellstr(DataSet.Models{i}.Transform{m}));
%           if ~any(idx), continue, end
%           correct_string = removeampersand(lx.lxt(m).transformDescriptions{~cellfun(@isempty,strfind(lx.lxt(m).transformDescriptions,'MSC'))});
%           TT = cellstr(DataSet.Models{i}.Transform{m});
%           TT{idx} = char(correct_string);
%           DataSet.Models{i}.Transform{m} = char(TT);
%         end
%
%       % Change e.g. 'SG w=9 o=2 d=1' to 'Savitzky-Golay ...'  and prepare a savgol-object (CR, 30-09-2016 08:57:07)
%         for m=1:2
%           idx = strncmpi('SG',cellstr(DataSet.Models{i}.Transform{m}),2);
%           if ~any(idx), continue, end
%           correct_string = removeampersand(lx.lxt(m).transformDescriptions{~cellfun(@isempty,strfind(lx.lxt(m).transformDescriptions,'&Savitsky-Golay ...'))});
%           TT = cellstr(DataSet.Models{i}.Transform{m});
%           SGtxt = TT{idx}; % e.g. 'SG w=9 o=2 d=1'
%           TT{idx} = char(correct_string);
%           %DataSet.Models{i}.Transform{m} = char(TT);
%
%           % Call savgol.m ...
%           tokens = regexp(SGtxt,'(\w*)\s*(.*)','tokens','once');
%           params = regexp(tokens{2},'w=(\d+)\s*o=(\d+)\s*d=(\d+)','tokens','once')
%           params = cellfun(@(x)(str2double(x)),params,'UniformOutput',false);
%           [width,order,deriv] = deal(params{:});
%           sg = transformations.savgol(width,order,deriv);
%           sg.getTransformationInformation
%
%           % og hvad g�r man s� lige med den?? (CR, 30-09-2016 10:30:39)
%
%         end


      for j = 1:numel(FN_Selected)
        FoundIdx = strmatch(FN_Selected{j},FN_Models_p3,'exact');
        if ~isempty(FoundIdx)
          lxm(i).Selected.(FN_Selected{j}) = DataSet.Models{i}.(FN_Models_p3{FoundIdx});
        else
         %disp([FN_Selected{j},' not found in p3_models']) % only 'NumSegs'; see below (22-09-2016 09:48:53)
        end
      % Retrieve NumSegs from DataSet.Models{i}.ModelData.output.NumSegs (22-09-2016 09:34:11)
        lxm(i).Selected.NumSegs = DataSet.Models{i}.ModelData.output.NumSegs; % 22-09-2016 09:37:30
      end

    % Transfer the selected transforms to the array of lxt-objects: X(m=1), Y(m=2)
    % The Transdata-vector must be calculated for each model later in order to
    % transfer the transformation vectors (mx. sx, etc.) to the lxt-sobcalsses (i.e. subclasses)
    % CR, 26-09-2016 13:40:57
      if strcmp(DataSet.Models{i}.Type,'PLS')
        mrange = 1:2;
      else
        mrange = 1;
      end

      for m = mrange
        lxm(i).lxt(m).selectedTransform = cellstr(DataSet.Models{i}.Transform{m});
      end
      lxm(i).RecalcTransformationNeeded = true; % Clear this flag when the transformations have been recalculated


    % oop. Calculate X-stats (were not calculated in the old LXCALCULATE!) (CR, 11-05-2015 15:24:26)
    % X-matrix
      PredCal = lxm(i).Data.output.PredCal;
         resXcal    = [PredCal.ssX0, PredCal.ssX]'; % For 0:MaxPC
         expvarXcal = 100*(1 - resXcal/resXcal(1));  % ssX0 is the first element   (% Model.PctDes = diff(expvarXcal);  % CR, 09-09-2005 10:35)
         lxm(i).Data.output.resXcal = resXcal;
         lxm(i).Data.output.expvarXcal = expvarXcal;

      PredVal = lxm(i).Data.output.PredVal;
         resXval    = [PredVal.ssX0, PredVal.ssX]'; % For 0:MaxPC
         expvarXval = 100*(1 - resXval/resXval(1));  % ssX0 is the first element
         lxm(i).Data.output.resXval = resXval;
         lxm(i).Data.output.expvarXval = expvarXval;

    % oop. Calculate GetYstats (were not calculated in the old LXCALCULATE!) (cr, 11-05-2015 15:24:26)
      K = sum(lxm(i).IndexIntoDataSet{2,2}); % Number of variables in Ycal-matrix (always matrix number two (2))
      Predictions = lxm(i).Data.output.PredCal;
        outdata = GetYstats(K, Predictions);
        lxm(i).Data.output.resYcal    = outdata.resY;
        lxm(i).Data.output.expvarYcal = outdata.expvarY;
      Predictions = lxm(i).Data.output.PredVal;
        outdata = GetYstats(K, Predictions);
        lxm(i).Data.output.resYval    = outdata.resY;
        lxm(i).Data.output.expvarYval = outdata.expvarY;

  end % for i = 1:numel(DataSet.Models)

  lx.Models = lxm;
  lx.selectedModelNum = min([lxm.Number]); % isempty when no models % this will call "set_selected(lx)"

%  selectedModelNum = min([lxm.Number]); % 1; % this will call "set_selected(lx)"
%  if isempty(selectedModelNum)
%    lx.selectedModelNum = 1;
%  else
%    lx.selectedModelNum = selectedModelNum;
%  end

%   % Set all "lx.selected<...>" properly (22-09-2016 12:04:57)
%
%   % Set the transform (22-09-2016 11:52:01)
%     for m = 2:-1:1
%       lx.selectedMatNum = m;
%       lx.selectedTransform  = lx.Models(lx.selectedModelNum).Selected.Transform;  % 22-09-2016 10:51:44
%     end
%
%   % Set the validation (22-09-2016 11:52:01)
%     lx.selectedValidation = lx.Models(lx.selectedModelNum).Selected.Validation; % 22-09-2016 12:00:30
%
%   % Set the rest
%     lx.selectedMaxPC            = lx.Models(lx.selectedModelNum).Selected.MaxPC;
%     lx.selectedOptimalDimension = lx.Models(lx.selectedModelNum).Selected.OptimalDimension
%     lx.selectedNumSegs          = lx.Models(lx.selectedModelNum).Selected.NumSegs;
%     lx.selectedNumRandomRepeats = lx.Models(lx.selectedModelNum).Selected.NumRandomRepeats;
%
%
%   %  lx.Models(lx.selectedModelNum).Selected  =
%   %
%   %                 MaxPC: 6
%   %      OptimalDimension: 1
%   %            Validation: 'CV: Full'
%   %               NumSegs: 4
%   %      NumRandomRepeats: []
%   %             Transform: {4x1 cell}



% G�r noget her ... (05-05-2015 14:05:28)
%  lx.Model(1).CatVar.Name = '';
%  lx.Model(1).CatVar.LastIndex = 1;


% CatVarsLoaded will be integrated as categorical variables in the table "data" (CR, 29-04-2015 11:10:59)
