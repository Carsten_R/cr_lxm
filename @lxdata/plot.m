function plot(lx, parent);
% The plot-method of lxdata which reads the properties "lx.PlotTransform", "lx.Transform", etc.
%
%
% CR, 28-04-2015 15:58:37

  if nargin < 2
    LATENTIX_GUI = false;

    fig = figure;
    set(fig,'Tag','lx_figureplot', ...
                  'Position', lx.gui.FigurePosition, ...
                  'Name', ['LATENTIX-file: ',fullfile(lx.pathname,lx.filename),' [',lx.data.Properties.Description,']'], ...
                  'Units','normalized', ...
                  'Visible', 'on', ...
                  'Color', lx.gui.S.BackgroundColor, ...
                  'Toolbar', 'figure');

    parent = fig; % Use this figure as parent (01-05-2015 07:21:50)
  else
    LATENTIX_GUI = true;
  end

% [~, curmat, akser] = getrwmat(lx);
  curmat = lx.curmat;
  akser  = lx.xaxis;

% MATLAB plots dimension #1 (rows) on x-axis and dimension #2 (coloumns) on y-axis.
% E.g. X = rand(20,4); plot(X) plot 4 curves with 20 points on the x-axis. The
% legends would then be "OrigLabels{2}", i.e. the 4 variable names. You could say
% that MATLAB has a default value "dim = 2" in a LATENTIX plot context.
%
% In chemometrics, however, we usually wants to have the variables on the x-axis and have
% the objects as curves. In order to achieve this as a default (normal) behavior,
% the matrix which is plotted herein must be transposed. Therefore lx.Transposed is set to one
% in the lxdata constructor.
%
% CR, 29-04-2015 09:57:55

  if lx.Transposed
    curmat = transpose(curmat);
    dim = 1;
    xlab = 'Variable number';
    xaxis = akser{2};
  else
    dim = 2;
    xlab = 'Object number';
    xaxis = akser{1};
  end

  if ~isfield(lx.data.Properties.UserData,'OrigLabels')
    lx.data.Properties.UserData.OrigLabels{1} = '';
    lx.data.Properties.UserData.OrigLabels{2} = '';
    disp('dyt')
  end

  ObjVarList{1} = lx.data.Properties.UserData.OrigLabels{1}(lx.IndexIntoDataSet{lx.selectedMatNum, 1});
  ObjVarList{2} = lx.data.Properties.UserData.OrigLabels{2}(lx.IndexIntoDataSet{lx.selectedMatNum, 2});

  S = lx.gui.S;
  S.BackgroundColor = [1 1 1]; % white (19-05-2015 14:28:21)

  CREATE_GUI = isempty(get(parent,'Children')); % CR, 05-05-2015 09:01:54

  if CREATE_GUI
      % Create some GUI objects
      hbox_plots = uiextras.HBoxFlex('Parent',parent);
       vbox_left = uiextras.VBoxFlex('Parent',hbox_plots);
            axes_plots = axes( 'Parent', uicontainer('Parent', vbox_left),'Tag','axes_plots'); % In order to be able to use colorbars, legends, etc. (cf. http://www.mathworks.com/matlabcentral/fileexchange/47982-gui-layout-toolbox/content//layoutdoc/Examples/colorbarexample.m)

            ms = uiextras.Panel('Parent',vbox_left); % ,'Title','','BorderType','none'); % Model settings
            [panelPlotOptions, panelLegends] = lxguiModelSettings(lx, ms, parent, 'PLOT'); % To be re-used in the "Model"-tab

           vbox_PlotOptions = uiextras.VBoxFlex('Parent',panelPlotOptions);
            if 1
                lbColorByString{1} =  {'Index','Objects sets', 'Variable'}; % 'Object',
                lbColorByString{2} =  {'Index','Variable sets','Object'};   % 'Variable'
                panel_ColorBy = uiextras.Panel('Parent', vbox_PlotOptions, 'Padding', 5, 'Title', 'Color by', 'Visible', 'on','BorderType','none');
                  hbox_ColorBy = uiextras.HBoxFlex('Parent',panel_ColorBy);
                     popColorBy     = uicontrol('Parent', hbox_ColorBy, 'Style', 'popupmenu', 'Tag', 'popColorBy','String',lbColorByString{1},S);
                     lbCBsets       = uicontrol('Parent', hbox_ColorBy, 'Style', 'list', 'Tag', 'lbCBsets','String','<no coloring>','Value',1,'Callback',@(src,evnt)lbCBsets_Callback(src,evnt,parent,dim),S );
                     %textIntervals  = uicontrol('Parent', hbox_ColorBy, 'Style', 'text', 'Tag', 'textIntervals','String','<Intervals>',S);
                     hbox_ColorBy.Sizes = [-1 -2];

                  b = uiextras.HButtonBox('Parent', vbox_PlotOptions, 'VerticalAlignment', 'top');
                    uicontrol('Parent', b, 'String', 'Raw data',    'Tag', 'pbRaw',         'Callback',@(src,evnt)onRightButtonSelection(src,evnt, parent));
                    uicontrol('Parent', b, 'String', 'Transformed', 'Tag', 'pbTransformed', 'Callback',@(src,evnt)onRightButtonSelection(src,evnt, parent));
                    uicontrol('Parent', b, 'String', 'Flip matrix', 'Tag', 'pbFlipMatrix',  'Callback',@(src,evnt)onRightButtonSelection(src,evnt, parent));
                  set(b, 'ButtonSize', [90 30], 'Spacing', 8 );

                vbox_PlotOptions.Sizes = [-3 -1];

            else % 18-05-2015 15:26:25
                switch dim
                  case 1, dim_ColorBy = 2;
                  case 2, dim_ColorBy = 1;
                end
                vbox_ColorBy = uiextras.VBoxFlex('Parent',panelPlotOptions);
                   lbCBobj_vars = uicontrol('Parent', vbox_ColorBy, 'Style', 'list', 'Tag', 'lbCBobj_vars','String',lx.Sets{dim}.Lab,'Value',1,'Callback',@(src,evnt)lbCBobj_vars_Callback(src,evnt,parent,dim),S);
                   lbCBsets = uicontrol('Parent', vbox_ColorBy, 'Style', 'list', 'Tag', 'lbCBsets','String',ObjVarList{dim_ColorBy},'Value',1,'Callback',@(src,evnt)lbCBsets_Callback(src,evnt,parent,dim),S);

                  b = uiextras.HButtonBox('Parent', vbox_ColorBy, 'VerticalAlignment', 'top');
                    uicontrol('Parent', b, 'String', 'Raw data',    'Tag', 'pbRaw',         'Callback',@(src,evnt)onRightButtonSelection(src,evnt, parent));
                    uicontrol('Parent', b, 'String', 'Transformed', 'Tag', 'pbTransformed', 'Callback',@(src,evnt)onRightButtonSelection(src,evnt, parent));
                    uicontrol('Parent', b, 'String', 'Flip matrix', 'Tag', 'pbFlipMatrix',  'Callback',@(src,evnt)onRightButtonSelection(src,evnt, parent));
                  set(b, 'ButtonSize', [90 30], 'Spacing', 8 );


            end


                lbSelected = uicontrol('Parent', panelLegends, 'Style', 'list', 'Tag', 'lbSelected','String','','Value',[],'Max',2,'Callback',@(src,evnt)lbSelected(src,evnt,parent,dim),S);


        vbox_left.Sizes = [-4 -1];
       %hbox_plots.Sizes = [-14 -3]; % [-5 -1];
       %vbox_right.Sizes = [-1 -6 -2];
  end % if CREATE_GUI


% Update handles structure
  h = guihandles(parent);                % (only if ~LATENTIX_GUI ??)
  if ~isfield(h,'axes_plots')
    h.axes_plots = gca; % Tags p� akser har det med at forsvinde i MATLAB! (05-05-2015 09:20:10)
  end
  h.lx = lx;
  h.parent = parent;
  h.LATENTIX_GUI = LATENTIX_GUI; % 01-05-2015 12:49:45
  guidata(parent,h)

  h_lines = plot(h.axes_plots, xaxis, curmat);
  addlines(h.axes_plots,'H',0,'k','-');
  addlines(h.axes_plots,'V',0,'k','-');
  xlabel(h.axes_plots, xlab)

% DEMO (CR, 06-05-2015 11:53:50):
%  c = colorbar('southoutside');
%  c.Label.String = 'PROG ng/ml (demo)';

  set(h_lines,'ButtonDownFcn',@(src,evnt)SelectLine_ButtonDownFcn(src,evnt,parent,dim))

  legtxt = lx.data.Properties.UserData.OrigLabels{dim}(lx.IndexIntoDataSet{lx.selectedMatNum, dim});
  if ~isempty(h.lbSelected)
    set(h.lbSelected,'String',legtxt)
  end

% Save for GUI-presentation, e.g.: set( h.lbObjVar, 'String', lx.gui.CurveLegends)
  lx.gui.plotmat.CurveLegends = legtxt;
  lx.gui.plotmat.linehandles  = h_lines; % e.g. h_lines(2).LineWidth = lx.gui.styles.width_fatline;

% =======================================================================================
% Callbacks
% =======================================================================================

% --------------------------------------------------------------------
function lbSelected(src, evnt, winobj, dim)
  h = guidata(winobj);

  set(h.lx.gui.plotmat.linehandles,            h.lx.gui.plotstyles.NotSelected)
  set(h.lx.gui.plotmat.linehandles(src.Value), h.lx.gui.plotstyles.Selected)

% --------------------------------------------------------------------
function SelectLine_ButtonDownFcn(src, evnt, winobj,dim)
  h = guidata(winobj);

  selection = get(gcf,'SelectionType');
  switch selection
    case 'normal', % Clear old selections
                     set(h.lx.gui.plotmat.linehandles, h.lx.gui.plotstyles.NotSelected)
                     set(gcbo,'Selected','on')
      case 'alt',    % If already selected then deselect else select
                     if strcmp(get(gcbo,'Selected'),'on')
                       set(gcbo,'Selected','off')
                     else
                       set(gcbo,'Selected','on')
                     end
      case 'open',
      case 'extend',
  end

% Find previous selection
  kids = flipud(get(get(gcbo,'Parent'),'Children'));
   idx = find(strcmp({kids.Selected},'on'));

% Clear all selections (04-05-2015 16:10:28)
  set(h.lx.gui.plotmat.linehandles, h.lx.gui.plotstyles.NotSelected)

% Highlight current selections
  set(h.lx.gui.plotmat.linehandles(idx), h.lx.gui.plotstyles.Selected)

% Show in listbox
  set(findobj(gcf,'Tag','lbSelected'),'Value',idx);

% --------------------------------------------------------------------
function onRightButtonSelection(src, evnt, winobj)
  h = guidata(winobj);

  switch src.Tag
    case 'Raw data',    % g�r det rigtige

    case 'Transformed', % g�r det rigtige

    case 'pbFlipMatrix', set(h.lx.gui.plotmat.linehandles, h.lx.gui.plotstyles.NotSelected) % 05-05-2015 09:36:12
                         set(findobj(gcf,'Tag','lbSelected'),'Value',[]); % Senere skal huskning indf�res (05-05-2015 09:37:36)
                         h.lx.Transposed = ~h.lx.Transposed;

                       % Change " ... 'Tag', 'lbCBsets','String',ObjVarList{dim_ColorBy}"
                       %        " ... 'Tag', 'popColorBy','String',lbColorByString{1}"
                       % Cluts nu (CR, 18-05-2015 16:22:24)
  end

%  if h.LATENTIX_GUI
%    parent = h.plotparent;
%  else
%    parent = gcf;
%   %clf(H.parent,'reset')
%  end

  plot(h.lx, h.parent);

% --------------------------------------------------------------------
function lbCBsets_Callback(src, evnt, winobj)
  h = guidata(winobj);
  disp('rtggagagh  g')
