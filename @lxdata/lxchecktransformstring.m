function lxchecktransformstring(lx)
% lx_oop, 28-04-2015 09:39:09
% Test e.g. med: lx.selectedTransform{1} = {'2-norm','2-norm','Pareto scaling','Mean center','Pareto scaling','SNV','Autoscale'}
%                lx.selectedTransform{2} = {'MSC','BLAH-BLAH','Pareto scaling','Mean center'}

% X and Xt shares transformations
% Y and Yt shares transformations
%
  for m = 1:2
    NewTransform = cellstr(lx.selectedTransform{m}); % The current transform settings. e.g. set by a GUI

    % --------------------------
    % Delete repeated selections
    % --------------------------
    if ~isempty(lx.AllowedRepeatedTransforms)
      % S� skal der ca. "lige" skrives �n linie f�r og �n linie efter "unique"
        A = ismember(NewTransform,lx.AllowedRepeatedTransforms); % ... (g�res ikke nu!)
    end

    NewTransform = unique(NewTransform,'stable'); % CR, 28-04-2015 09:09:15

    % -------------------------------------------------------------------------------
    % Select only one of "Autoscale", "Mean center", "Pareto scaling" a move the
    % last selected to the end last position
    % -------------------------------------------------------------------------------
    [~,B] = ismember(lx.AppearOnlyOnceTransforms,NewTransform);
    if any(B)
      [~,idx] = max(B);
      SelectedLast = lx.AppearOnlyOnceTransforms{idx};        % Find the last selected
      A = ismember(NewTransform,lx.AppearOnlyOnceTransforms); % Find all AppearOnlyOnceTransforms
      NewTransform(A) = '';                                   % ... and remove all appearences
      NewTransform{end+1} = SelectedLast;                     % Insert the last selected at the last position
    end
    lx.selectedTransform{m} = NewTransform;                           % Save the ratified transform settings in the lx-object
  end % for m = 1:2


return

% GAMMEL:

  if     lx.SelectedMatNum == 1 | lx.SelectedMatNum == 3, SelectedMatNum = 1; % X and Xt shares transformations
  elseif lx.SelectedMatNum == 2 | lx.SelectedMatNum == 4, SelectedMatNum = 2; % Y and Yt shares transformations
  end

  NewTransform = cellstr(lx.selectedTransform{SelectedMatNum}); % The current transform settings. e.g. set by a GUI

% --------------------------
% Delete repeated selections
% --------------------------
  if ~isempty(lx.AllowedRepeatedTransforms)
    % S� skal der ca. "lige" skrives �n linie f�r og �n linie efter "unique"
      A = ismember(NewTransform,lx.AllowedRepeatedTransforms); % ... (g�res ikke nu!)
  end

  NewTransform = unique(NewTransform,'stable'); % CR, 28-04-2015 09:09:15

% -------------------------------------------------------------------------------
% Select only one of "Autoscale", "Mean center", "Pareto scaling" a move the
% last selected to the end last position
% -------------------------------------------------------------------------------
  [~,B] = ismember(lx.AppearOnlyOnceTransforms,NewTransform);
  if any(B)
    [~,idx] = max(B);
    SelectedLast = lx.AppearOnlyOnceTransforms{idx};        % Find the last selected
    A = ismember(NewTransform,lx.AppearOnlyOnceTransforms); % Find all AppearOnlyOnceTransforms
    NewTransform(A) = '';                                   % ... and remove all appearences
    NewTransform{end+1} = SelectedLast;                     % Insert the last selected at the last position
  end

  lx.selectedTransform{SelectedMatNum} = NewTransform;           % Save the ratified transform settings in the lx-object
