function [panelPlotOptions, panelLegends] = lxguiModelSettings(lx, ms, winobj, Caller);
% lxguiModelSettings
% CR, 01-05-2015 14:02:15

  switch Caller
    case 'PLOT',  NotesTitle = 'Statistics'; % Der er plads og det er bare for at udnytte denne ...
                  predvisible = 'off';
    case 'MODEL', NotesTitle = 'Notes';
                  predvisible = 'on';
  end

  BGC = [1 1 1]; % 'BackgroundColor'

  hbox_main = uiextras.HBoxFlex('Parent',ms);

      vbox = uiextras.VBoxFlex('Parent',hbox_main);
          %parent = vbox%
          hbox = uiextras.HBoxFlex('Parent',vbox);
              %parent = hbox%
              vbox_left = uiextras.VBoxFlex('Parent',hbox);
                  mt = uiextras.Panel('Parent', vbox_left, 'Padding', 5, 'Tag', '', 'Title', 'Model type','BorderType','none'); % 'Model type'
                      popModelType = uicontrol('Parent', mt, 'Style', 'popupmenu', 'Tag', 'popModelType',GetGUIparameters(lx,'Model'),lx.gui.S, 'Callback',@(src,evnt)onModelSettingSelection(src,evnt, winobj)); % 'String',lx.ModelTypes
                  cp = uiextras.Panel('Parent', vbox_left, 'Padding', 5, 'Tag', '', 'Title', '','BorderType','none'); % 'Calculate'
                      uicontrol(cp, 'Style', 'checkbox','String','Predict','Value',0,'Tag','cbPredict','Visible',predvisible, 'Callback',@(src,evnt)onModelSettingSelection(src,evnt, winobj));

                     %%% %bg = uibuttongroup('Parent',cp,'Position',[0 0 0.2 1],'SelectionChangedFcn',@bselection); % ,'Position',[0 0 .2 1]
                     %%% bgV = uiextras.HButtonGroup('Parent',cp,'Buttons',{'Calibrate','Predict'},'Spacing',50,'Padding',1,'SelectionChangeFcn',@onSelectionChange); % ,'SelectedChild',1
                     %%% % bg = uiextras.VButtonBox('Parent', cp);
                     %%% % Create two radio buttons in the button group.
                     %%% %r1 = uicontrol(bg, 'Style', 'radio','String','Calibrate','Value',1,'Position',[10 250 100 30]);
                     %%% %r2 = uicontrol(bg, 'Style', 'radio','String','Predict','Value',0,'Position',[10 350 100 30]);
                     %%% %set([r1,r2], 'ButtonSize', [90 90], 'Spacing', 5 );

                   calc = uiextras.Panel('Parent', vbox_left, 'Padding', 5, 'Tag', '', 'Title', '','BorderType','none'); % 'Calculate'
                        r3 = uicontrol('Parent',calc, 'Style', 'pushbutton','Tag', 'pbCalculate', 'String','Calculate', 'Callback',@(src,evnt)onModelSettingSelection(src,evnt, winobj));

              %parent = hbox%
              vbox_middle = uiextras.VBoxFlex('Parent',hbox);
                  %1%
                  panelValidationMethod = uiextras.Panel('Parent',vbox_middle, 'Padding', 5, 'Title','Validation Method','BorderType','none'); % 'Validation Method'
                      popValidationMethod  = uicontrol('Parent', panelValidationMethod, 'Style', 'popupmenu', 'Tag', 'popValidationMethod',GetGUIparameters(lx,'Validation'),lx.gui.S, 'Callback',@(src,evnt)onModelSettingSelection(src,evnt, winobj)); % GetGUIparameters(lx,'Validation') istf. 'String',lx.ValidationTypes
                  %2%
                  hbox_right = uiextras.HBoxFlex('Parent',vbox_middle);


                      mpc = uiextras.Panel('Parent', hbox_right, 'Padding', 5, 'Tag', '', 'Title', 'Max PC','BorderType','none');
                            MaxPC = int2str(lx.Models(1).Selected.MaxPC); % Display, but also save in userdata for retrieval when input is wrong (05-05-2015 12:58:12)
                          etMaxPC = uicontrol('Parent', mpc, 'Style', 'edit','Tag', 'etMaxPC','String',MaxPC,'UserData',MaxPC,'BackgroundColor',BGC, 'Callback',@(src,evnt)onModelSettingSelection(src,evnt, winobj));

                      panelSegments = uiextras.Panel('Parent', hbox_right, 'Padding', 5, 'Title','Segments','Tag','panelSegments','BorderType','none');
                           NumSegs = int2str(lx.Models(1).Selected.NumSegs); % Display, but also save in userdata for retrieval when input is wrong (05-05-2015 12:58:12)
                          etNumSegs = uicontrol('Parent', panelSegments, 'Style', 'edit','Tag','etNumSegs','String',NumSegs,'UserData',NumSegs,'BackgroundColor',BGC, 'Callback',@(src,evnt)onModelSettingSelection(src,evnt, winobj));

                      panelRandomRepeats = uiextras.Panel('Parent', hbox_right, 'Padding', 5, 'Title','Repetitions','Tag','panelRandomRepeats','BorderType','none');
                                 NumRandomRepeats = int2str(lx.Models(1).Selected.NumRandomRepeats); % Display, but also save in userdata for retrieval when input is wrong (05-05-2015 12:58:12)
                          etNumRandomRepeats = uicontrol('Parent', panelRandomRepeats, 'Style', 'edit','Tag','etNumRandomRepeats','String',NumRandomRepeats,'UserData',NumRandomRepeats,'BackgroundColor',BGC, 'Callback',@(src,evnt)onModelSettingSelection(src,evnt, winobj));
                  %3%
                  panelCVvariable = uiextras.Panel('Parent',vbox_middle, 'Padding', 5, 'Title','Category variable','Tag','panelCatVar','BorderType','none');
                     %popCatVar  = uicontrol('Parent', panelCVvariable, 'Style', 'popupmenu', 'Tag', 'txtCatVar','String','','BackgroundColor',BGC, 'Callback',@(src,evnt)onModelSettingSelection(src,evnt, winobj));
                      txtCatVar  = uicontrol('Parent', panelCVvariable, 'Style', 'text', 'Tag', 'txtCatVar','String','','BackgroundColor',BGC, 'Callback',@(src,evnt)onModelSettingSelection(src,evnt, winobj));


          % UDKOMMENTERET 18-05-2015 13:23:59
          %%%% %parent = vbox%
          %%%% mn = uiextras.Panel('Parent', vbox, 'Padding', 5, 'Tag', '', 'Title', NotesTitle,'BorderType','none','BackgroundColor',BGC);
          %%%%        pp.HorizontalAlignment = 'left';
          %%%%        pp.FontName = 'Courier';
          %%%%     et = uicontrol('Parent', mn, 'Style', 'edit','Tag', 'NotesTitle','String','','BackgroundColor',BGC, 'Callback',@(src,evnt)onModelSettingSelection(src,evnt, winobj), pp);

          switch Caller % 18-05-2015 13:18:02
            case 'PLOT',  panelPlotOptions = uiextras.Panel('Parent',hbox_main,'Title','','BorderType','none','BackgroundColor',BGC); % 'Color by' % vbox_PlotOptions = uiextras.VBoxFlex('Parent',hbox_main);
                          panelLegends     = uiextras.Panel('Parent',hbox_main,'Title','','BorderType','none','BackgroundColor',BGC); % 'Legends' % , 'Padding', 5, 'Title','Category variable','Tag','panelCatVar','BorderType','none'
                          %uicontrol('Parent', hbox_main, 'Style', 'list', 'Tag', 'lbModelInfo','String','Legends','Value',[],'Max',2,'Callback','',lx.gui.S, 'Callback',@(src,evnt)onModelSettingSelection(src,evnt, winobj));
            case 'MODEL', NotesTitle = 'Notes';
                          uicontrol('Parent', hbox_main, 'Style', 'list', 'Tag', 'lbModelInfo','String','Model notes','Value',[],'Max',2,'Callback','',lx.gui.S, 'Callback',@(src,evnt)onModelSettingSelection(src,evnt, winobj));
                          uicontrol('Parent', hbox_main, 'Style', 'list', 'Tag', 'lbModelInfo','String','Model info', 'Value',[],'Max',2,'Callback','',lx.gui.S, 'Callback',@(src,evnt)onModelSettingSelection(src,evnt, winobj));

          end % switch Caller




  hbox_main.Sizes = [-3 -3 -2]; % [-1 -1 -1]
  vbox_left.Sizes = [-1 -1 -1];
  hbox.Sizes = [-1 -1];


% vbox.Sizes = [-1 -1];


% =======================================================================================
% Callbacks
% =======================================================================================

% --------------------------------------------------------------------
function onModelSettingSelection(src, evnt, winobj, dim)
% Adjust lx.Selected and set properties (e.g. 'Visible') of GUI-objects

  h = guidata(winobj);

  Tag = src.Tag;
  switch Tag
    case 'popModelType', h.lx.selectedCalMethod = src.String{src.Value}; % 07-05-2015 11:51:09

    case 'pbCalculate',  lxcalculate(h.lx); % 15-05-2015 11:46:32

    case 'cbPredict', % 18-05-2015 08:54:46

    case 'popValidationMethod', h.lx.Validation = src.String{src.Value};
                                FN = fieldnames(h.lx.gui.ModelSettings);
                                for i = 1:numel(FN)
                                  set(h.(FN{i}),h.lx.gui.ModelSettings.(FN{i}))
                                end
                                h.selectedValMethod = src.String{src.Value}; % 07-05-2015 11:52:29

    case 'etMaxPC',             MaxPC = str2double(src.String);
                                if isnan(MaxPC)
                                  uiwait(errordlg('Please input a number','Max PC'))
                                  src.String = src.UserData;
                                elseif MaxPC < 1
                                  uiwait(errordlg('MaxPC should be at least one (1)','Max PC'))
                                  src.String = src.UserData;
                                elseif MaxPC > min(size(h.lx.data))
                                  uiwait(errordlg('MaxPC should not exceed data size','Max PC'))
                                  src.String = src.UserData;
                                else
                                  src.UserData = int2str(MaxPC);
                                end
                                h.lx.selectedMaxPC = str2double(src.UserData); % 07-05-2015 11:53:58

    case 'etNumSegs',           NumSegs = str2double(src.String);
                                if isnan(NumSegs)
                                  uiwait(errordlg('Please input a number','NumSegs'))
                                  src.String = src.UserData;
                                elseif NumSegs < 2
                                  uiwait(errordlg('Segments should be at least two (2)','Segments'))
                                  src.String = src.UserData;
                                elseif NumSegs > size(h.lx.data,1)
                                  uiwait(errordlg('Segments should not exceed data size','Segments'))
                                  src.String = src.UserData;
                                else
                                  src.UserData = int2str(NumSegs);
                                end
                                h.lx.selectedNumSegs = str2double(src.UserData); % 07-05-2015 11:54:16

    case 'etNumRandomRepeats',  NumRandomRepeats = str2double(src.String);
                                if isnan(NumRandomRepeats)
                                  uiwait(errordlg('Please input a number','Repetitions'))
                                  src.String = src.UserData;
                                elseif NumRandomRepeats < 4
                                  uiwait(errordlg('Repetitions should be at least four (4)','Repetitions'))
                                  src.String = src.UserData;
                                else
                                  src.UserData = int2str(NumRandomRepeats);
                                end
                                h.lx.selectedNumRandomRepeats = str2double(src.UserData); % 07-05-2015 11:55:53

                               % case 'etMaxPC', 'etNumSegs' og 'etNumRandomRepeats' kan skrives en smule sammen, hvis man har lidt tid til overs (05-05-2015 13:19:18)


    case 'NotesTitle', disp('fsdf fsd fsdf')

  otherwise
    Speak(Tag)
  end

