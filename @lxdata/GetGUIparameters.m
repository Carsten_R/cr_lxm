function StrVal = GetGUIparameters(lx, item, varargin)
% StrVal = GetGUIparameters(lx, item)
%
% lx     The lxdata-object
% item   The item in lx to get the String and Values from.
%        Possible values are: 'Transform'
%                             'Validation'
%                             'Model'
%
%
%  StrVal = (a struct with fields):
%
%      String: A list of selectables
%       Value: The value currently selected (when applicable)
%
% In an application this function can e.g. be used like:
%
%  set( handles.listbox, GetGUIparameters(lx,'Transform') )
%  set( handles.listbox, GetGUIparameters(lx,'Validation') )
%  set( handles.listbox, GetGUIparameters(lx,'Model') )
%
% CR, 28-04-2015 11:48:20

% Initialize output
  str = '';
  val = [];

  StrVal.String = str;
  StrVal.Value  = val;

  str = char(removeampersand(lx.([item,'Types'])));

  switch item
    case 'Transform',  val = []; % No single element is selected. used to state all selectables
    case 'Validation', val = strmatch(lx.selectedValidation,str,'exact');
    case 'Model',      val = strmatch(lx.selectedCalMethod,str,'exact');
  end % switch item

  StrVal.String = cellstr(str);
  StrVal.Value  = val;
end % GetGUIparameters
