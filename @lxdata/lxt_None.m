function varargout = lxt_None(lx, varargin)
% LXT_NONE No transformation (a dummy transformation)
%
%  I/O:
%
%  Transformation info:                 trf_info = lxt_None(lx)
%
%  Calculate transformation:    [Xtr, TransData] = lxt_None(lx, X, params) or:
%                               [Xtr, TransData] = lxt_None(lx, X)
%
%  Use transformation:                       Xtr = lxt_None(lx, X, TransData)
%
%  <initials>, <date>




% =======================================================
%
% The function name shall begin with the prefix "lxt_"
%
% Edit these lines:

  trf_info.transformDescriptions = 'None';       % Descriptive name optionally including "&", i.e. which letter to underscore in a GUI

  trf_info.allowRepeated = false;

  trf_info.exclusive = false;  % Allow only one transformation tagged as "exclusive" in a transform-sequence
                                      % For now these are exclusive: 'Mean center', 'Autoscale' and 'Pareto scaling'

  trf_info.helptext  = char('');

% Also write/edit these three functions below:
%
%      calculateTransform
%      applyTransform
%      canApply
%
% =======================================================


% DO NOT EDIT HERE:

  funcname = getfuncname;
  trf_info.availableTransform = funcname;    % e.g. 'lxt_None'; the transformation method file name

  nvarin  = numel(varargin);
  nvarout = nargout;

% Handling if params are given in input or not (case: "Calculate transformation"):
  if nvarout == 2

    if nvarin == 2             % [Xtr, TransData] = lxt_None(lx, X, params)
        params = varargin{2};
    elseif nvarin == 1         % [Xtr, TransData] = lxt_None(lx, X)
        params = [];
        nvarin = 2;
    end

  end

      if nvarin == 0,                                 varargout{1} = trf_info;                                              % Set relevant information only,                 e.g. trf_info = lxtransform(lx);
  elseif nvarout == 2 && nvarin == 2, [varargout{1}, varargout{2}] = calculateTransform(lx, varargin{1}, params, funcname); % Calculate the transform and set the TransData, e.g. [X, TransData] = lxt_None(lx, X, params)
  elseif nvarout == 1 && nvarin == 2,                 varargout{1} = applyTransform(lx, varargin{1}, varargin{2});          % Apply a previously calculated transform,       e.g. X = lxt_None(lx, X, TransData);
  else
    ME = MException([funcname,':inputArguments'], 'Wrong number of output and input-arguments (o,u): (1,1), (1,3), (2,2) or (2,3) or  allowed'); throw(ME);
  end


% =======================================================
% Edit the three functions below
% =======================================================


% ------------------------------------------------
function [Xtr, TransData] = calculateTransform(lx, X, params, funcname)
%

  TransData.funcname = funcname; % do not edit
  TransData.params   = params;   % do not edit
  Xtr = X;

% ------------------------------------------------
function Xtr = applyTransform(lx, X, TransData)
% Mean center input data using previously calculated mean

  if canApply(X, TransData)
     Xtr = X;
  else
     Xtr = [];
  end


% -------------------------------------------------
function OK = canApply(X, TransData)

  OK = true;
