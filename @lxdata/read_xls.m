function read_xls(lx,filename)
% READ_XLS Read XL-files
%
% CR, 30-04-2015 11:16:19


  if nargin == 1 || isempty(filename)
    FileFilter = {'*.xls','Excel-files (*.xls)'; '*.xlsx','Excel-files (*.xlsx)'};
    [filename,pathname] = uigetfile(FileFilter,'Select file','MultiSelect','off');
    if isnumeric(filename) & isnumeric(pathname)  % Cancel pressed
      return;
    end
  end

  lx.filename = filename;
  lx.pathname = pathname;


  lx.data = readtable(fullfile(pathname,filename),'ReadRowNames',true);
  lx.data.Properties.Description = fullfile(pathname,filename);
  lx.data.Properties.UserData.OrigLabels{1} = lx.data.Properties.RowNames;
  lx.data.Properties.UserData.OrigLabels{2} = lx.data.Properties.VariableNames;

