function lxcalculate(lx, varargin);



% LXCALCULATE Method of lxdata. Calculate PCA- or PLS-models
%
% Carsten Ridder, 2003 - 2008
% Henrik Toft, 2008
%
% Re-written to OOP:
%
% Carsten Ridder, maj 2015
%                 april 2016
%                 september 2016

        %
        % GAMMEL: [output, Jnovar, MatNum, dim, showtxt] = lxcalculate(handles, varargin);
        %
        %
        % CR, 19-04-2016 11:00:59 / skriv klasserne @lxplsmodel, @lxpcamodel; lidt ligesom MATLAB's classifier-klasser /

        % CR, 07-07-2004
        % Jeg beslutter at skrive funktionen helt om (CR, 31-03-2005 14:14)
        %
        % Calls to "lxcheckillegalgui" added (CR, 07-09-2005)
        % Calls to "lxmisssvd" added (CR, 01-03-2006)
        % showtxt added to the output (CR, 12-06-2006 08:58)
        % Jnovar added to the output (CR, 15-06-2006 11:36)
        % Jnovar are the global indices of variables with no variance. We might deselect these and recalculate?


return % BEM�RK: lx3calculate, som er en kopi af lxcalculate, men hvor "lx" er ikke et objekt, anvendes (28-06-2017 16:21:58)

  WS = warning;
  warning off % MATLAB:divideByZero

  if isempty(lx.data)
    ME = MException('lxcalculate:NoData', ...
                    'No data selected!');
    throw(ME);
    return
  end

  rwmat = lx.rwmat;  % the 4 raw, un-transformed data, maybe with missing data (CR, 21-09-2016 14:51:58)
  trmat = lx.trmat;  % the 4 basic matrices in their transformed forms (CR, 26-09-2016 09:49:13)

  [I,J] = size(rwmat{1});                 % number of objects and variables in selected Xcal-matrix
                                          % (always matrix number one (1))
  show_wait_bar = true;
  if length(varargin) == 0
    CalMethod = lx.selectedCalMethod;
    ValMethod = lx.selectedValidation;
    NumSegs   = lx.selectedNumSegs;
    MaxPC     = lx.selectedMaxPC;

  else
    CalMethod = varargin{1};  % CR, 06-06-2005 15:01 (Use LXCALCULATE without LATENTIX.M or from LXREPEATEDCV) - DON'T DELETE !!!
    ValMethod = varargin{2};
    NumSegs   = varargin{3};
    MaxPC     = varargin{4};
    if length(varargin) > 4
       show_wait_bar = varargin{5};
    end

  end

  try
    % Her kunne man s�tte MaxPC op, hvis den er lavere end handles.GlobalMinPC (f.eks. 12) (CR, 15-06-2006 13:14)
    % Det er hamrende irriterende, hvis MaxPC ved et tilf�lde er blevet for lav.
      MaxPC = max(MaxPC,lx.selectedGlobalMinPC); % oop
  end

% Adjust the number of crossval-segments and PC's
  NumSegs = min([I NumSegs]); % The maximal number of crossval-segments
  MaxPC   = min([I J MaxPC]); % The maximal number of components

  switch ValMethod
    case 'No validation',     CVon = false; SegMat = [];
    case 'Test set',          CVon = false; SegMat = [];
    case 'CV: Full',          CVon = true;  SegMat = lxgetsegs(I,I,0);
    case 'CV: Syst123',       CVon = true;  SegMat = lxgetsegs(I,NumSegs,0);
    case 'CV: Syst111',       CVon = true;  SegMat = lxgetsegs(I,NumSegs,1);
    case 'CV: Random',        CVon = true;  SegMat = lxgetsegs(I,NumSegs,2);

    case 'CV: Random (repeated)',
                              NumRuns = varargin{6}; % Called from LXREPEATEDCV
                              CVon = true;  SegMat = lxgetsegs(I,NumSegs,2); % v2.00, 07-01-2008 15:58

    case 'CV: Sets',          %CVon = true;
                              %SegMat = lxgetsegs(I,handles.CatVar.vector,2); % oop senere! 07-05-2015 14:06:42
                              %NumSegs = size(SegMat,2);
                              %set(handles.etNumSegs,'String',int2str(NumSegs));  % oop senere! 07-05-2015 14:06:42

    case 'CV: Variable ...',  %CVon = true;
                              %SegMat = lxgetsegs(I,handles.CatVar.vector,2); % oop senere! 07-05-2015 14:06:42
                              %NumSegs = size(SegMat,2);
                              %set(handles.etNumSegs,'String',int2str(NumSegs)); % oop senere! 07-05-2015 14:06:42

    case 'Category variable', CVon = true;  SegMat = [];  % kommer senere ...

  otherwise
    lxlogerror('Kludder i navngivningen i LXCALCULATE.M + LATENTIX.M')
  end % switch ValMethod

% Only X or both X and Y?
  switch CalMethod
    case {'PCA','SIMCA'}, UseY = false;
    case {'PLS','PCR'},   UseY = true;
  otherwise
    lxlogerror('Ny kalibreringsmetode indf�rt. Tilret koden i LXCALCULATE.M')
  end % switch CalMethod

  if CVon % Flyttet hertil (CR, 22-03-2006 17:00)
    % Adjust the number of PC's
      FewestCalObjs = min(sum(~SegMat));
      MaxPC         = min([FewestCalObjs J MaxPC]); % The maximal number of components
  end

  lx.selectedMaxPC   = MaxPC;
  lx.selectedNumSegs = NumSegs;


%  ************************************ ESTIMATE MISSING - START *****************************************
%  Totalt omskrevet CR, 29-09-2016 18:06:10

%[PX, PY] = getTransOverview(lx.lxt);

  X = [ [rwmat{1}, rwmat{2}];
        [rwmat{3}, rwmat{4}] ];

%Tester
%X(1,2) = nan;
%X(8,3) = nan

  [X, MissStats] = lxmisssvd(lx, X);

  if ~isempty(MissStats)
    % De enkelte tr-objs (meancenter, autoscale, osv.) SKAL v�re handle-underklasser!
    % Og lxTransform fungerer som b�de value og handle ...
    % CR, 29-09-2016 21:03:10

    % 1. Replace raw data (rwmat) with estimated data (no missing)
    % note: some matrices can be empty of varying sizes, but the transform-methods works anyway.
      rwmat{1} = X(  1:I  ,   1:J);
      rwmat{2} = X(  1:I  , J+1:end);
      rwmat{3} = X(I+1:end,   1:J);
      rwmat{4} = X(I+1:end,   J+1:end);


    % ENTEN: (godkendt, CR, 29-09-2016 21:10:39)                                                   % ELLER:
    % 2. Transform and replace trmat                            %      % 2. Transform and replace trmat
    % note: set and use the trans-vectors                       %      % note: set and use the trans-vectors
      trmat{1} = lx.lxt(1).calculateTransform(rwmat{1});        %        trmat{1} = lxt_missvals(1).calculateTransform(rwmat{1});
      trmat{2} = lx.lxt(2).calculateTransform(rwmat{2});        %        trmat{2} = lxt_missvals(2).calculateTransform(rwmat{2});
      trmat{3} = lx.lxt(1).applyTransform(rwmat{3});            %        trmat{3} = lxt_missvals(1).applyTransform(rwmat{3});
      trmat{4} = lx.lxt(2).applyTransform(rwmat{4});            %        trmat{4} = lxt_missvals(2).applyTransform(rwmat{4});

%[PX, PY] = getTransOverview(lx.lxt);

  end % if ~isempty(MissStats)


%  ************************************* ESTIMATE MISSING - END ******************************************


%   %  ************************************ ESTIMATE MISSING - START *****************************************
%   %  Totalt omskrevet CR, 29-09-2016 16:53:50
%
%     X = [ [rwmat{1}, rwmat{2}];
%           [rwmat{3}, rwmat{4}] ];
%
%     [X, MissStats] = lxmisssvd(lx, X);
%
%     if ~isempty(MissStats)
%       % 1. Generate a work transformation object
%         lxt_missvals = lx.lxt;
%
%       % 2. Replace raw data (rwmat) with estimated data (no missing)
%       % note: some matrices can be empty of varying sizes, but the transform-methods works anyway.
%         rwmat{1} = X(  1:I  ,   1:J);
%         rwmat{2} = X(  1:I  , J+1:end);
%         rwmat{3} = X(I+1:end,   1:J);
%         rwmat{4} = X(I+1:end,   J+1:end);
%
%       % 3. Transform and replace trmat
%       % note: set and use the trans-vectors
%         trmat{1} = lxt_missvals(1).calculateTransform(rwmat{1});
%         trmat{2} = lxt_missvals(2).calculateTransform(rwmat{2});
%         trmat{3} = lxt_missvals(1).applyTransform(rwmat{3});
%         trmat{4} = lxt_missvals(2).applyTransform(rwmat{4});
%
%         lx.lxt = lxt_missvals; % EJ TESTET!! (CR, 28-09-2016 11:05:58)
%
%     end % if ~isempty(MissStats)
%
%   %  ************************************* ESTIMATE MISSING - END ******************************************


% Dette skal lige checkes. Jeg har nu lavet lxTransform om til en value class (CR, 29-09-2016 18:02:45)
%    % note: lxt_missvals = lx.lxt; duer vistnok ikke, n�r lxTransform er et handle-objekt
%      lxt_missvals(1)                   = lxTransform; % CR, 27-09-2016 16:15:24
%      lxt_missvals(1).selectedTransform = lx.lxt(1).selectedTransform;
%      lxt_missvals(2)                   = lxTransform; % CR, 28-09-2016 10:26:40
%      lxt_missvals(2).selectedTransform = lx.lxt(2).selectedTransform;



%    %  *******************************************************************************************************
%    %  ************************************ ESTIMATE MISSING - START *****************************************
%    %  **** Totalt omskrevet CR, 29-09-2016 16:53:50 *********************************************************
%
%      MissStats = [];
%
%      Xcal = rwmat{1}; % CR, 27-09-2016 17:58:39
%      Ycal = rwmat{2}; %
%      Xval = rwmat{3}; %
%      Yval = rwmat{4}; %
%
%
%
%    % Initialize to same transformation (CR, 27-09-2016 18:09:21) (lxt_missvals = lx.lxt; % duer vistnok ikke)
%      lxt_missvals(1)                   = lxTransform; % CR, 27-09-2016 16:15:24
%      lxt_missvals(1).selectedTransform = lx.lxt(1).selectedTransform;
%
%      lxt_missvals(2)                   = lxTransform; % CR, 28-09-2016 10:26:40
%      lxt_missvals(2).selectedTransform = lx.lxt(2).selectedTransform;
%
%      MissingInCal = any(isnan(Xcal(:))) | any(isinf(Xcal(:))) | any(isnan(Ycal(:))) | any(isinf(Ycal(:))); % CR, 29-09-2016 16:28:04
%      MissingInVal = any(isnan(Xval(:))) | any(isinf(Xval(:))) | any(isnan(Yval(:))) | any(isinf(Yval(:))); % CR, 29-09-2016 16:41:18
%
%
%      MissingInCal = any(isnan(rwmat{1}(:))) | ...
%                     any(isinf(rwmat{1}(:))) | ...
%                     any(isnan(rwmat{2}(:))) | ...
%                     any(isinf(rwmat{2}(:)));
%
%      MissingInVal = any(isnan(rwmat{3}(:))) | ...
%                     any(isinf(rwmat{3}(:))) | ...
%                     any(isnan(rwmat{4}(:))) | ...
%                     any(isinf(rwmat{4}(:)));
%
%      if MissingInCal | MissingInVal
%      % Base missing-estimation on original untransformed matrices
%      % Include Ycal, Xval and Yval as required  (HT, 2008-02-06)
%        cal = Xcal;
%        if UseY
%            cal = [cal,Ycal];
%        end
%
%        val = [];
%        if UseTestSet
%            val = Xval;
%            if UseY
%                val = [val,Yval];
%            end
%        end
%
%      % Estimate the missing values on the combined matrices (m = 1:4)
%        X = [cal;val];
%        [X, MissStats] = lxmisssvd(lx, X);
%        if isempty(X)
%            ME = MException('lxcalculate:NoData', ...
%                            'All data are missing!');
%            throw(ME);
%            return
%        end
%
%      % Extract the raw data matrices again; missing values are now replaced by estimated raw data values
%        Xcal = X(1:I,1:J);
%        if UseY
%            Ycal = X(1:I,J+1:end);
%        end
%
%        if UseTestSet
%            Xval = X(I+1:end,1:J);
%            if UseY
%                Yval = X(I+1:end,J+1:end);
%            end
%        end
%
%      % Now do this (CR, 27-09-2016 18:03:10):
%      % 1. Replace raw data (rwmat) with estimated data (no missing)
%      % 2. Transform and replace trmat
%
%        if MissingInCal     % Use "calculateTransform"
%            rwmat{1} = Xcal;
%            trmat{1} = lxt_missvals(1).calculateTransform(rwmat{1});
%            if UseY
%                rwmat{2} = Ycal;
%                trmat{2} = lxt_missvals(2).calculateTransform(rwmat{2});
%            end
%        end % if MissingInCal
%
%        if MissingInVal % Use "applyTransform"
%            if ~MissingInCal
%            % Set the transform vectors in lxt_missvals (CR, 29-09-2016 15:48:18)
%              lxt_missvals(1).calculateTransform(rwmat{1});
%              if UseY
%                  lxt_missvals(2).calculateTransform(rwmat{2});
%              end
%            end
%
%            rwmat{3} = Xval;
%            trmat{3} = lxt_missvals(1).applyTransform(rwmat{3});
%            if UseY && UseTestSet
%                rwmat{4} = Yval;
%                trmat{4} = lxt_missvals(2).applyTransform(rwmat{4});
%            end
%        end % if MissingInVal
%
%      end % if MissingInCal | MissingInVal
%
%      if ~isempty(MissStats)
%        lx.lxt = lxt_missvals; % EJ TESTET!! (CR, 28-09-2016 11:05:58)
%      end
%
%      %  *******************************************************************************************************
%      %% ************************************* ESTIMATE MISSING - END ******************************************
%      %  *******************************************************************************************************










% Now this is true:
%    rwmat{m} (m = 1:4) is raw data without missing values
%    trmat{m} (m = 1:4) is properly transformed data without missing values

% ===================================================================================
% Common to all CalMethod and ValMethod
% ===================================================================================

% Calculate the final model exploiting that data are already transformed in lxdata.trmat (CR, 28-09-2016 11:26:22) or above (CR, 02-03-2006 09:48)
  Xcal = trmat{1}; % oop % Xcal = handles.WorkSet.Matrix(1).data;  % Properly transformed Xcal-matrix
  Ycal = trmat{2}; % oop % Ycal = handles.WorkSet.Matrix(2).data;  % Properly transformed Ycal-matrix
  Xval = trmat{3}; % CR, 27-09-2016 18:24:46
  Yval = trmat{4}; % CR, 27-09-2016 18:24:49

% Check for illegal data after transformation (LN + CR, 02-03-2006 10:29)
  [Idxtot{1}, Idxtot{2}] = size(lx.data); % number of objects and variables in the parent dataset
                                          % only used in "lxcheckfornans"
  OK = lxcheckfornans(Xcal, Idxtot, lx.IndexIntoDataSet(1,:), 'Xcal') & ...
       lxcheckfornans(Ycal, Idxtot, lx.IndexIntoDataSet(2,:), 'Ycal') & ...
       lxcheckfornans(Xval, Idxtot, lx.IndexIntoDataSet(3,:), 'Xval');

  if ~OK
    ME = MException('lxcalculate:NoDataWithVariance', ...
                    'No variance in the data!');
    throw(ME);
    return % v2.00 20-02-2007 15:58
  end

  Model = lxcalibrate({Xcal, Ycal}, CalMethod, MaxPC); % The final model

% Regression coefficients to be used on raw un-scaled data.
% Model.B includes an offset as the last row for each PC and Y-variable
% CR, 07-07-2006 15:24
  if UseY
    [Model.B, my]  = lxregcoef_raw(Model, lx.lxt);
  end

  if isfield(Model,'Converged')
    if ~Model.Converged
     %showtxt = strvcat(showtxt,'No convergence of PLS2-algorithm for the final model'); % LN vil ikke ha' det 12-06-2006
      lxlogerror('No convergence of the global PLS2-model')
    end
  end

% Prediction of the calibration set
  PredCal = lxpredict({Xcal}, Model, lx.lxt(2)); % oop % PredCal = lxpredict({Xcal, Ycal}, Model,{handles.WorkSet.Matrix(1).TransData, handles.WorkSet.Matrix(2).TransData});

% ===================================================================================
  if CVon
    % Adjust the number of PC's
      FewestCalObjs = min(sum(~SegMat));
  end

  if UseY
   % Get the number of variables in Ycal-matrix (always matrix number two (2))
     K = size(rwmat{2},2); % sum(lx.IndexIntoDataSet{2,2}); % oop % K = length(handles.WorkSet.Matrix(2).Idx{2});

     if CVon
       % Initialize the Ypred-matrix
       % if ~CVon then Ypred is not initialized, but simply calculated below
         Ypred  = NaN*ones(I,K,MaxPC);
         Ypred0 = NaN*ones(I,K,1);
     end % if CVon

     YpredCal = PredCal.Ypred;

   % Predictions of zero'th component for the calibration set is set to
   % the mean of the transformed data rescaled to original units.
   % This is a scalar value, which is "repmat'ed" to the proper size
   % depending on the validation method used.
     Pred0means = lxrescale(mean(trmat{2}), lx.lxt(2)); % oop % Pred0means = lxrescale(mean(handles.WorkSet.Matrix(2).data),handles.WorkSet.Matrix(2).TransData);

   % Calculating ssY0 and ssY
     PredCal.ssY0 = zeros(K+1,1);     % LN+CR, 15-08-2005
     PredCal.ssY  = zeros(K+1,MaxPC); %

     Ytemp = trmat{2};
     for k=1:K
       PredCal.ssY0(k) = PredCal.ssY0(k) + sum(sum(Ytemp(:,k).^2)); % LN+CR, 15-08-2005 11:24
     end
     PredCal.ssY0(K+1,:) = sum(PredCal.ssY0(1:K,:),1);
     for a=1:MaxPC
       temp = PredCal.Ypred(:,:,a);
       temp = Ytemp - lx.lxt(2).applyTransform(temp); % oop, 27-09-2016 16:09:48
       for k=1:K
         PredCal.ssY(k,a)  = PredCal.ssY(k,a) + sum(sum(temp(:,k).^2));
       end
     end
     PredCal.ssY(K+1,:) = sum(PredCal.ssY(1:K,:),1);
  end


% There is 6 situations to consider:
%
% A) UseY = true
%
%       A1 - No validation
%       A2 - CVon = true    oop-test: T er OK, men P eller W er ikke OK (CR, 28-09-2016 12:05:12)
%       A3 - Test set
%
% B) UseY = false
%
%       B1 - No validation   oop-OK 27-09-2016 14:47:18 + 27-09-2016 17:34:56
%       B2 - CVon = true     oop-OK 27-09-2016 15:05:17 + 27-09-2016 17:43:37
%       B3 - Test set        oop-OK 27-09-2016 15:14:29


  if UseY % Situation A) i.e. PLS, PCR
      if strcmp(ValMethod,'No validation') % A1
          Ypred0 = repmat(Pred0means, I, 1);  % Adjusting size of Ypred0 to the calibration set
        % Prediction of the calibration set has been performed above
          Ypred = PredCal.Ypred;
          PredVal = PredCal;

        % Preparing the output
          Yval = rwmat{2}; % oop % Yval = handles.DataSet.data(handles.WorkSet.Matrix(2).Idx{1}, handles.WorkSet.Matrix(2).Idx{2});
      end % A1

%      if CVon
%        Ypred0 = repmat(Pred0means, I, 1);  % Adjusting size of Ypred0 to the calibration set
%        % v2.00 30-11-2006 15:43 LN+CR
%      end

      if CVon % A2

          if show_wait_bar
            hWait = waitbar(0,'...','name','Cross Validation in progress ...','CreateCancelBtn','closereq','Tag','LXwaitbar');  % Updated by HTP 15-09-2005
          end

          PredVal.ssX0 = 0;                % CR, 11-08-2005 16:22
          PredVal.ssX  = zeros(1,MaxPC);   % CR, 11-08-2005 16:22
          PredVal.ssY0 = zeros(K+1,1);     % LN+CR
          PredVal.ssY  = zeros(K+1,MaxPC); %

          Converged = size(SegMat,2); % Counting the number of segments where the PLS2-algorithm has converged (CR, 12-06-2006 08:52)

          for seg = 1:size(SegMat,2)
              lxt_seg(1) = lxTransform; % oop, 27-09-2016 16:15:24
              lxt_seg(1).selectedTransform = lx.lxt(1).selectedTransform;
% Den g�r ikke med SG .../savgol (CR, 30-09-2016 08:07:41)
              lxt_seg(2) = lxTransform; % 28-09-2016 10:26:40
              lxt_seg(2).selectedTransform = lx.lxt(2).selectedTransform;

              if show_wait_bar
                if ishandle(hWait)    % HTP 15-09-2005
                  waitbar(seg/size(SegMat,2),hWait,['Segment ',int2str(seg),'/',int2str(size(SegMat,2))])
                else                  % HTP 15-09-2005
                  return              % HTP 15-09-2005
                end                   % HTP 15-09-2005
              end

            % CALIBRATION
              m = 1; % 26-09-2016 16:25:44
              Xcal = rwmat{1}(~SegMat(:,seg),:);                             % oop % Xcal = handles.WorkSet.Matrix(1).data(~SegMat(:,seg),:);
              temp = lxt_seg(1).calculateTransform(Xcal); % CR, 27-09-2016 18:31:37

            % Check for constant variables leading to e.g. zero standard deviation (CR, 09-08-2005 14:20)
              if isempty(find(isinf(temp))) & isempty(find(isnan(temp)))
                Xcal = temp;
              else
                if show_wait_bar
                  lxlogerror(['WARNING: Invalid X-scaling in segment #',int2str(seg),'. Global scaling is used.'])
                end
                lxt_seg(1)= lx.lxt(1); % EJ TESTET!! Men s� simpelt burde det v�re!? (CR, 28-09-2016 10:50:43)
                Xcal = lxt_seg(1).calculateTransform(Xcal);
              end

              Ycal = rwmat{2}(~SegMat(:,seg),:);
              temp = lxt_seg(2).calculateTransform(Ycal); % CR, 28-09-2016 10:21:42

            % Check for constant variables leading to e.g. zero standard deviation (CR, 09-08-2005 14:20)
              if isempty(find(isinf(temp))) & isempty(find(isnan(temp)))
                Ycal = temp;
              else
                if show_wait_bar
                  lxlogerror(['WARNING: Invalid Y-scaling in segment #',int2str(seg),'. Global scaling is used.'])
                end
                lxt_seg(2) = lx.lxt(2); % EJ TESTET!!
                Ycal = lxt_seg(2).calculateTransform(Ycal); % CR, 28-09-2016 10:22:14
              end

            % For calculation of statistics for 0'th component (LN + CR aftale 8/7-04)
            % Eventuel median i stedet for mean
              mYcal = lxrescale(mean(Ycal), lxt_seg(2));
              Ypred0(SegMat(:,seg),:) = repmat(mYcal,size(find(SegMat(:,seg)))); % oop
% v2.00 30-11-2006 15:41 LN+CR
              ModelSeg = lxcalibrate({Xcal, Ycal}, CalMethod, MaxPC); % ModelSeg is not saved (should it be?)

              if isfield(ModelSeg,'Converged')
                Converged = Converged - ModelSeg.Converged; % CR, 12-06-2006 08:51
              end

            % PREDICTION
                 Xval = rwmat{1}(SegMat(:,seg),:);
                 Xval = lxt_seg(1).applyTransform(Xval); % CR, 28-09-2016 10:42:19
              Predseg = lxpredict({Xval}, ModelSeg, lxt_seg(2)); % CR, 28-09-2016 11:40:52

              PredVal.ssX0 = PredVal.ssX0 + Predseg.ssX0; % CR, 11-08-2005 16:26
              PredVal.ssX  = PredVal.ssX  + Predseg.ssX;

            % Calculating ssY0 and ssY
              Ytemp = rwmat{2}(SegMat(:,seg),:);
              Ytemp = lxt_seg(2).applyTransform(Ytemp); % CR, 28-09-2016 10:42:45
              for k=1:K
                PredVal.ssY0(k) = PredVal.ssY0(k) + sum(sum(Ytemp(:,k).^2)); % LN+CR, 15-08-2005 11:24
              end
              PredVal.ssY0(K+1,:) = sum(PredVal.ssY0(1:K,:),1);
              for a=1:MaxPC
                temp = Predseg.Ypred(:,:,a);
                temp = Ytemp - lxt_seg(2).applyTransform(temp); % CR, 28-09-2016 10:43:29
                for k=1:K
                  PredVal.ssY(k,a)  = PredVal.ssY(k,a) + sum(sum(temp(:,k).^2));
                end
              end
              PredVal.ssY(K+1,:) = sum(PredVal.ssY(1:K,:),1);

            % Inserting predictions at the proper row indices
              Ypred(SegMat(:,seg),:,1:MaxPC) = Predseg.Ypred(:,:,1:MaxPC);
              PredVal.Si(SegMat(:,seg),:)    = Predseg.Si(:,1:MaxPC);
              PredVal.Hi(SegMat(:,seg),:)    = Predseg.Hi(:,1:MaxPC);
            % CR, 09-05-2005 11:19
              PredVal.resXi(SegMat(:,seg),:) = Predseg.resXi(:,1:MaxPC);
              PredVal.T2(SegMat(:,seg),:)    = Predseg.T2(:,1:MaxPC);

            % CR, 13-09-2006 14:51
            % PredVal.Ftest(SegMat(:,seg),:) = Predseg.Ftest(:,1:MaxPC);

              PredVal.Siy(SegMat(:,seg),:)   = Predseg.Siy(:,1:MaxPC); % 19-04-2005 15:23
              PredVal.Hiy(SegMat(:,seg),:)   = Predseg.Hiy(:,1:MaxPC); % 19-04-2005 15:23

              PredVal.T(SegMat(:,seg),:)     = Predseg.T(:,1:MaxPC);
          end % for seg = 1:size(SegMat,2)

          if Converged < size(SegMat,2)
            %showtxt = strvcat(showtxt,['No convergence of PLS2-algorithm in ',int2str(size(SegMat,2)-Converged),' segments of ',int2str(size(SegMat,2))]);
          end

          if show_wait_bar
            if ishandle(hWait)    % HTP 15-09-2005
              close(hWait);
            else                  % HTP 15-09-2005
              output = [];        % HTP 15-09-2005
              return;             % HTP 15-09-2005
            end                   % HTP 15-09-2005
          end

        % Preparing the output
          Yval = rwmat{2}; % oop % Yval = handles.DataSet.data(handles.WorkSet.Matrix(2).Idx{1}, handles.WorkSet.Matrix(2).Idx{2});
      end % A2

      if strcmp(ValMethod,'Test set') % A3
          Xval = trmat{3}; % oop % Properly transformed Xt-matrix % oop % Xval = handles.WorkSet.Matrix(3).data; % Properly transformed Xt-matrix

          It = size(Xval,1);
          if It < 3
            warndlg('PLS test set validation requires at least 3 test objects','Too few objects!','modal')
            return
          end

          Ypred0 = repmat(Pred0means, It, 1);    % Adjusting size of Ypred0 to the test set
          Pred   = lxpredict({Xval}, Model, lx.lxt(2));
          Ypred  = Pred.Ypred;

          PredVal.ssX0 = Pred.ssX0; % CR, 11-08-2005 16:28
          PredVal.ssX  = Pred.ssX;

          PredVal.Si = Pred.Si;
          PredVal.Hi = Pred.Hi;
        % CR, 09-05-2005 11:19
          PredVal.resXi = Pred.resXi;
          PredVal.T2  = Pred.T2;

        % CR, 13-09-2006 14:55
        % PredVal.Ftest = Pred.Ftest;

          PredVal.Siy = Pred.Siy;
          PredVal.Hiy = Pred.Hiy;

          PredVal.T  = Pred.T;


          PredVal.ssY0 = zeros(K+1,1);     % LN+CR,
          PredVal.ssY  = zeros(K+1,MaxPC); %

        % Calculating ssY0 and ssY

          Ytemp = trmat{4}; % oop % Ytemp = handles.WorkSet.Matrix(4).data; % Already transformed data
          for k=1:K
            PredVal.ssY0(k) = PredVal.ssY0(k) + sum(sum(Ytemp(:,k).^2)); % LN+CR, 15-08-2005 11:24
          end
          PredVal.ssY0(K+1,:) = sum(PredVal.ssY0(1:K,:),1);

          for a=1:MaxPC
            temp = Pred.Ypred(:,:,a);
            temp = Ytemp - lx.lxt(2).applyTransform(temp); % CR, 29-09-2016 20:02:13
            for k=1:K
              PredVal.ssY(k,a)  = PredVal.ssY(k,a) + sum(sum(temp(:,k).^2));
            end
          end

          PredVal.ssY(K+1,:) = sum(PredVal.ssY(1:K,:),1);

        % Preparing the output
          Yval = rwmat{4}; % oop % Yval = handles.DataSet.data(handles.WorkSet.Matrix(4).Idx{1}, handles.WorkSet.Matrix(4).Idx{2});
      end % A3

    % -----------------------
    % Calculating statistics
    % -----------------------

    % VALIDATION STATISTICS
    % Inserting Ypred0 into Ypred at index #1 (CR, 03-08-2004 12:13)
      Ypred(:,:,2:end+1) = Ypred;
      Ypred(:,:,1)       = Ypred0;  % PC#0 is at the first index (cf. LXMEASPRED.M)
      for a = 1:MaxPC+1
        valstats(a) = lxmeaspredstats(Yval, Ypred(:,:,a));
      end
      valstats(1).slope     = repmat(NaN,1,K); % CR, 12-04-2006 12:26 ----> repmat (CR, 28-04-2006 14:32)
      valstats(1).intercept = repmat(NaN,1,K); % Values for PC# 0 obscures the other PC's
      stats.val = valstats;

    % CALIBRATION STATISTICS
    % Get the un-transformed Ycal
      Ycal = rwmat{2}; % oop % Ycal = handles.DataSet.data(handles.WorkSet.Matrix(2).Idx{1}, handles.WorkSet.Matrix(2).Idx{2});

      if strmatch(ValMethod,'Test set')
        % The variable Ypred0 has the size of the test set, i.e.
        % have to "re-calculate Ypred0" to get the calibration error
          calstats(1) = lxmeaspredstats(Ycal, repmat(Pred0means,I,1));
      else
        % The variable Ypred0 has already the size of the calibration set
          calstats(1) = lxmeaspredstats(Ycal, Ypred0);
      end
      calstats(1).slope      = repmat(NaN,1,K); % CR, 12-04-2006 12:28
      calstats(1).intercept  = repmat(NaN,1,K); % Values for PC# 0 obscures the other PC's

      for a = 1:MaxPC
        calstats(a+1) = lxmeaspredstats(Ycal, YpredCal(:,:,a));  % note the "a+1"
      end

    % oop: her kunne vi passende afskaffe denne udokumenterede metode! (CR, 08-05-2015 15:43:27)
      AngleON = 1;  % Tilf�j lxangle(Model.P,Model.W); til calstats (CR, 12-04-2006 12:58)
      if AngleON
        calstats(1).angPW = repmat(NaN,1,K);
        for a = 1:MaxPC
          calstats(a+1).angPW = repmat(lxangle(Model.P(:,a),Model.W(:,a)),1,K);
        end
      end

    % Tilf�j (CR, 12-04-2006 13:30)
      BstatsON = 1; % Coded CR, 03-08-2006 12:58
      if ~isempty(Model.B) && BstatsON
          B = Model.B; % What about Model.Bscaled?
          calstats(1).B0      = my;      % intercept/offset
          calstats(1).Bsum    = my;      % sum of regression coefficients
          calstats(1).B2sum   = my.^2;   % sum of squared regression coefficients
         %calstats(1).Bmaxabs = abs(my); % highest absolute regression coefficient (sl�et fra CR, 15-08-2006 11:21)
          for a = 1:MaxPC
            calstats(a+1).B0      = B(end,:,a);
            calstats(a+1).Bsum    = sum(B(1:end-1,:,a));
            calstats(a+1).B2sum   = sum((B(1:end-1,:,a)).^2);
           %calstats(a+1).Bmaxabs = max(abs(B(1:end-1,:,a)));
          end
      end % if BstatsON

      stats.cal = calstats;

    % Inserting Ypred0 into YpredCal at index #1 (CR, 03-08-2004 12:58)
      YpredCal(:,:,2:end+1) = YpredCal;
      if CVon
      % Adjusting the size of YpredCal. If CVon, then MaxPC might have changed
        YpredCal = YpredCal(:,:,1:MaxPC+1);
        YpredCal(:,:,1) = Ypred0;
      else
        YpredCal(:,:,1) = repmat(Pred0means,I,1);
      end

  elseif ~UseY % Situation B) i.e. PCA

      if strcmp(ValMethod,'No validation') % B1
        PredVal = PredCal;
      end % B1

      if CVon % B2
          matnum = 1;
          hWait = waitbar(0,'...','name','Cross Validation in progress ...','CreateCancelBtn','closereq','Tag','LXwaitbar'); % Updated, HTP 15-09-2005

          PredVal.ssX0 = 0;              % CR, 11-08-2005 16:29
          PredVal.ssX  = zeros(1,MaxPC); % CR, 11-08-2005 16:30
          for seg = 1:size(SegMat,2)

              lxt_seg = lxTransform; % oop, 27-09-2016 14:48:03
              lxt_seg.selectedTransform = lx.lxt(1).selectedTransform;

              if ishandle(hWait)  %HTP 15-09-2005
                waitbar(seg/size(SegMat,2),hWait,['Segment ',int2str(seg),'/',int2str(size(SegMat,2))])
              else                %HTP 15-09-2005
                return,           %HTP 15-09-2005
              end                 %HTP 15-09-2005
            % CALIBRATION
              Xcal = rwmat{1}(~SegMat(:,seg),:); % !! CR, 27-09-2016 17:37:09
              temp = lxt_seg.calculateTransform(Xcal); % CR, 27-09-2016 17:22:56

            % Check for constant variables leading to e.g. zero standard deviation (CR, 09-08-2005 14:20)
              if isempty(find(isinf(temp))) & isempty(find(isnan(temp)))
                Xcal = temp;
              else
                if show_wait_bar
                  lxlogerror(['WARNING: Invalid X-scaling in segment #',int2str(seg),'. Global scaling is used.'])
                end
                lxt_seg.selectedTransform = lx.lxt(1).selectedTransform;
                Xcal = lxt_seg.calculateTransform(Xcal); % CR, 27-09-2016 17:21:36
              end

              ModelSeg = lxcalibrate({Xcal}, CalMethod, MaxPC); % ModelSeg is not saved (should it be?)
            % PREDICTION
                 Xval = rwmat{1}(SegMat(:,seg),:);
                 Xval = lxt_seg.applyTransform(Xval); % CR, 27-09-2016 17:25:10
              Predseg = lxpredict({Xval}, ModelSeg, lx.lxt(2));   % oop % Predseg = lxpredict({Xval}, ModelSeg);

              PredVal.ssX0 = PredVal.ssX0 + Predseg.ssX0; % CR, 11-08-2005 16:30
              PredVal.ssX  = PredVal.ssX  + Predseg.ssX;

              PredVal.Si(SegMat(:,seg),:) = Predseg.Si(:,1:MaxPC);
              PredVal.Hi(SegMat(:,seg),:) = Predseg.Hi(:,1:MaxPC);
            % CR, 09-05-2005 11:19
              PredVal.resXi(SegMat(:,seg),:) = Predseg.resXi(:,1:MaxPC);
              PredVal.T2(SegMat(:,seg),:)    = Predseg.T2(:,1:MaxPC);

            % CR, 13-09-2006 14:52
            % PredVal.Ftest(SegMat(:,seg),:) = Predseg.Ftest(:,1:MaxPC);

              PredVal.T(SegMat(:,seg),:)  = Predseg.T(:,1:MaxPC);
              % other prediction statistics will properly come later ...
          end % for seg = 1:size(SegMat,2)
          if ishandle(hWait)
            close(hWait)
          else
            output = [];
            return;
          end
      end % if CVon % B2

      if strcmp(ValMethod,'Test set') % B3
          Xval = trmat{3};

          Pred = lxpredict({Xval}, Model, lx.lxt(2));

          PredVal.ssX0 = Pred.ssX0; % CR, 11-08-2005 16:31
          PredVal.ssX  = Pred.ssX;

          PredVal.Si = Pred.Si;
          PredVal.Hi = Pred.Hi;
        % CR, 09-05-2005 11:19
          PredVal.resXi = Pred.resXi;
          PredVal.T2  = Pred.T2;

        % CR, 13-09-2006 14:56
        % PredVal.Ftest  = Pred.Ftest;

          PredVal.T  = Pred.T;
          % other prediction statistics will properly come later ...
      end % B3

  end

% Find i latentix.m disse linier og skrive koden ind ca. her ... (CR, 08-05-2015 16:01:04)
% "Det f�lgende skal alts� ikke blive st�ende her. Vi mangler at indf�re denne beregning (oop: g�r det nu! (CR, 08-05-2015 15:58:36))
%  i LXCALCULATE.M"

% Kunne dette st� her? (CR, 09-09-2005 09:51)
% Fors�ger at kode det (CR, 11-05-2015 13:55) ups! 10 �r!!!

% --------------------------
% CALIBRATION STATISTICS
% --------------------------
% X-matrix
  output.resXcal    = [PredCal.ssX0, PredCal.ssX]'; % For 0:MaxPC
  output.expvarXcal = 100*(1 - output.resXcal/output.resXcal(1));  % ssX0 is the first element   (% Model.PctDes = diff(expvarXcal);  % CR, 09-09-2005 10:35)

% Y-matrix
  % oop udkommenteret (11-05-2015 15:03:21)
  % if UseY
  %   resY    = [PredCal.ssY0(end,:), PredCal.ssY(end,:)];
  %   output.expvarYcal = 100*(1 - resY/resY(1));
  % else
  %   output.expvarYcal = NaN;
  % end

% Y-matrix
  if UseY
     K = sum(lx.IndexIntoDataSet{2,2}); % oop % Number of variables in Ycal-matrix (always matrix number two (2))
     outdata = GetYstats(K, PredCal);
     output.resYcal    = outdata.resY;
     output.expvarYcal = outdata.expvarY;
  end

% --------------------------
% VALIDATION STATISTICS
% --------------------------
  output.resXval    = [PredVal.ssX0, PredVal.ssX]'; % For 0:MaxPC
  output.expvarXval = 100*(1 - output.resXval/output.resXval(1));  % ssX0 is the first element

  if UseY
     outdata = GetYstats(K, PredVal);
     output.resYval    = outdata.resY;
     output.expvarYval = outdata.expvarY;
  end

% output skal s�ttes afh�ngig af CalMethod
  switch CalMethod
    case 'PCA',
              output.Model             = Model;    % Includes Si and Hi for calibration samples
              output.PredCal           = PredCal;  % 31-03-2005 13:59
              output.PredVal           = PredVal;  % 06-04-2005 16:03

    case {'PLS','PCR'},
              output.Model             = Model;   % Includes Si and Hi for calibration samples
              output.PredCal           = PredCal; % 27-04-2005 10:35
              output.PredVal           = PredVal; % 07-04-2005 09:56 T, Si and Hi for val samples

              output.MeasPred.Ycal     = rwmat{2}; % CR, 28-09-2016 11:56:24
              output.MeasPred.YpredCal = YpredCal;

              output.MeasPred.Yval     = Yval;   % rows: I eller It
              output.MeasPred.YpredVal = Ypred;  % rows: I eller It

              output.MeasPred.stats    = stats;  % use of stats (e.g. in LXMEASPRED.M):
                                                 %    FN = fieldnames(stats.val);
                                                 %    for f=1:length(FN)
                                                 %      cat(1,stats.(FN{f}));
                                                 %    end
                                                 % or:
                                                 %    RMSE = cat(1,stats.val.RMSE)
  end % switch CalMethod

  output.MaxPC     = MaxPC;     % CR, 03-08-2004 16:33
  output.SegMat    = SegMat;    % CR, 04-08-2004 14:18
  output.NumSegs   = NumSegs;   % CR, 15-09-2004 13:51
% HT, missing, 2008-02-06: MissStats contains information on all involved matrices!
  output.MissStats = MissStats; % CR, 02-03-2006 11:50
  output.ModelDate = now;


% oop (08-05-2015 16:03:18)
% Set the fields of lxm.Data
  lxm = lxmodel;
  FN = fieldnames(lxm.Data);
  for i = 1:length(FN)
    if isfield(Model,FN{i})
      lxm.Data.(FN{i}) = Model.(FN{i});
    else
      disp([FN{i},' not found'])
    end
  end
  output = rmfield(output,'Model');

  lxm.Data.output = output;
  lxm.DataSetName = lx.filename;

% M�ske mangler der ikke noget (CR, 26-09-2016 16:05:19)
%   % Transfer lx.Selected to lxm.Selected (15-05-2015 13:32:04)
%   % Den er jo lidt sv�r efter at jeg har trukket alle lx.Selected.(FN) ud til lx.selected<FN>!! (CR, 22-09-2016 10:31:29)
%   % find en l�sning ... (22-09-2016 10:32:14)
%     FN = fieldnames(lx.Selected);
%     for i = 1:numel(FN)
%       if isfield(lxm.Selected,FN{i})
%         lxm.Selected.(FN{i}) = lx.Selected.(FN{i});
%       end
%     end

% Hvad med "lxm.lxt"? Den skal s�ttes rigtigt! (CR, 27-09-2016 11:28:48)
  lxm.lxt = lx.lxt; % CR, 28-09-2016 12:04:38

% Add the model to the lxdata-object
  if isempty(lx.Models(end).Number)
    lxm.Number = 1;
    lx.Models(1) = lxm;
  else
    lxm.Number = lx.Models(end).Number + 1;
    lx.Models(end+1) = lxm;
  end


  warning(WS);

%[PX, PY] = getTransOverview(lx.lxt);

% use of stats (e.g. in LXMEASPRED.M):
%    FN = fieldnames(stats.val);
%    for f=1:length(FN)
%      cat(1,stats.(FN{f}));
%    end
% or:
%    RMSE = cat(1,stats.val.RMSE)




