classdef lxTransform < handle
%LXTRANSFORM LatentiX transformation class
%   Use lxTransform for handling data transformations.
%
%      lxt = lxTransform;
%      lxt.selectedTransform = lxt.transformDescriptions([2,3])
%  or  lxt.selectedTransform = {'Mean centering'}
%      datat = lxt.calculateTransform(data);

% Skal laves om til en abstrakt klasse (CR, 17-01-2017 11:30:35):
%
   % Abstract class for creating a default template for implementing
   % existing and new transformation methods.
   % Subclass constructors must align with this layout and only transformation
   % special information must exist in the relevant transformation objects
   % e.g. asking for parameters in Savitsky-Golay.

% TEST AF SOURCETREE (CR, 17-01-2017 11:43:52)
% Det virker (hvorfor er jeg overrasket!?)
% Jeg vil skrive al transformationskode i denne branch med navnet "LX3_transform_code",
% som ligger p� "C:\LX3_transform_code" p� min lokale disk


   properties
      availableTransform
      transformDescriptions
      selectedTransform
      transformParmprefix % CR, 01-10-2016 18:46:40
      transformHelptext   % CR, 29-09-2016 11:41:57

      allowRepeated  % CR, 13-10-2016 16:47:06
      exclusive      % do.


   end

%   events
%     TransformChanged    % CR, 29-09-2016 12:27:17
%   end

methods

function obj = lxTransform(varargin)
% constructor

         currentPath = pwd;
         if isequal(nargin,0)
            tfPath = currentPath;
         elseif isequal(nargin,1)
            tfPath = varargin{1};
         end
         tfFiles = dir(fullfile(tfPath,'+transformations'));
         tfFiles = tfFiles(~[tfFiles.isdir]);
         obj.availableTransform = cell(size(tfFiles));
         for m=1:length(tfFiles)
            eval(horzcat('obj.availableTransform{m} = transformations.',strtok(tfFiles(m).name,'''.'''),';'))
         end
       % tfOrder = cellfun(@(x) getfield(x,'order'),obj.availableTransform);
       % [~,tfOrder] = sort(tfOrder);
       % obj.availableTransform = obj.availableTransform(tfOrder);
         obj.transformDescriptions = cellfun(@(x) getfield(x,'menuname'),      obj.availableTransform,'UniformOutput',false);
         obj.transformHelptext     = cellfun(@(x) getfield(x,'helptext'),      obj.availableTransform,'UniformOutput',false);
         obj.transformParmprefix   = cellfun(@(x) getfield(x,'parmprefix'),    obj.availableTransform,'UniformOutput',false);
         obj.allowRepeated         = cellfun(@(x) getfield(x,'allowRepeated'), obj.availableTransform,'UniformOutput',false); % CR, 13-10-2016 16:39:49
         obj.exclusive             = cellfun(@(x) getfield(x,'exclusive'),     obj.availableTransform,'UniformOutput',false); % CR, 13-10-2016 16:43:00
%%%%     obj.showname              = cellfun(@(x) getfield(x,'showname'),      obj.availableTransform,'UniformOutput',false); % CR, 28-06-2017 14:43:13

end % lxTransform

function obj = set.selectedTransform(obj,stf)
% stf can contain both "menuname" and "showname"
%
% all shownames in stf are converted to menunames via the "set_parm" method, which
% also interprets the showname and sets the parameters - if any.
% Examples:
% showname           set_parm                       menuname
% 'SNV'              does nothing                    'Standard &Normal Variate (SNV)'
% 'SG w=9 o=2 d=2'   sets width, order and deriv    '&Savitsky-Golay ...'

  stf = utilities.removeampersand(stf); % 02-11-2016 11:27:10

  if isempty(stf) | strcmp(stf,'None')
    obj.selectedTransform = []; % 27-09-2016 13:56:21
    return
  end % CR, 26-09-2016 13:59:46

  tD = utilities.removeampersand(obj.transformDescriptions); % CR, 30-09-2016 08:53:06
  tP = obj.transformParmprefix;                    % CR, 02-10-2016 12:38:14

% Search for showname, set parameters and replace in stf with menuname
  if ~all(ismember(cellstr(stf), utilities.removeampersand(obj.transformDescriptions)))
    idx = find(~ismember(cellstr(stf), tD));

    if ~isempty(idx)           % parmprefix is found
       for i = 1:numel(idx)    % evaluate one by one
         for j = 1:numel(tP)   %
             if isempty(tP{j}) %
                 continue      %
             else
                 idx2 = find(strncmp(stf{idx(i)}, tP, numel(tP{j})));
                 if ~isempty(idx2)
                     tfCur = obj.availableTransform{idx2};
                     tfCur.set_parm(stf{idx(i)});
                     stf{idx(i)} = char(utilities.removeampersand(tfCur.menuname));
                 end
             end

        end % for j = 1:numel(tP)
       end % for i = 1:numel(idx)
    end % if ~isempty(idx)
  end % if ~all(ismember(cellstr(stf), removeampersand(obj.transformDescriptions)))


  idx = find(~ismember(cellstr(stf), tD));
  if ~isempty(idx)
     ME = MException('lxTransform:incorrectTransformationSpecified', ...
                     ['One or more of the transformations (e.g. "',stf{idx(1)},'") specified are unavailable or invalid!']); % ['One or more of the transformations (e.g. "',stf{idx(1)},'") specified are unavailable or invalid!']
     throw(ME);
  else
     obj.selectedTransform = stf;

  end


end % set.selectedTransform

function data = calculateTransform(obj,data)

         if isempty(data)
           return % CR, 29-09-2016 16:50:19
         end

       % UDKOMMENTERET; tillad "No transformations"! (CR, 29-06-2017 12:26:01)
       %  if isempty(obj.selectedTransform)
       %     ME = MException('lxTransform:noTransformationsSelected', ...
       %                     'One or more transformations must be selected!');
       %     throw(ME);
       %  end

         for m=1:length(obj.selectedTransform)
            tfIdx = ismember(utilities.removeampersand(obj.transformDescriptions),utilities.removeampersand(obj.selectedTransform(m)));
            tfCur = obj.availableTransform{tfIdx};
            data = tfCur.calculateTransform(data);
         end

        %utilities.ShowCallHistory

end % calculateTransform

function data = applyTransform(obj,data)

         if isempty(data)
           return % CR, 29-09-2016 16:50:19
         end

         for m=1:length(obj.selectedTransform)
            tfIdx = ismember(utilities.removeampersand(obj.transformDescriptions),obj.selectedTransform(m));
            tfCur = obj.availableTransform{tfIdx};
            data = tfCur.applyTransform(data);
            if isempty(data) && ~isdeployed
               ME = MException.last;
               MException.last('reset')
%                throw(ME)
               data = ME;
            end
         end

         %utilities.ShowCallHistory
end % applyTransform


function data = inverseTransform(obj,data)
% Bruges bl.a. ved estimering af missing data (lxModel.lxmisssvd)
% Only - by now - 'Autoscale', 'Pareto scaling' and 'Mean center' can be inverted
% Other transformations will throw an error (CR, 03-11-2016 14:42:47)

         if isempty(data)
           return % CR, 29-09-2016 16:50:19
         end

         for m=1:length(obj.selectedTransform)
            tfIdx = ismember(utilities.removeampersand(obj.transformDescriptions),obj.selectedTransform(m));
            tfCur = obj.availableTransform{tfIdx};
            data = tfCur.inverseTransform(data); % Eksisterer m�ske ikke for alle! (CR, 02-11-2016 15:07:30)
            if isempty(data) && ~isdeployed
               ME = MException.last;
               MException.last('reset')
%                throw(ME)
               data = ME;
            end
         end

         %utilities.ShowCallHistory
end % inverseTransform


function trans = getTransformationInformation(obj) % trans -> trans_table (CR, 28-09-2016 09:10:41)

         trans = cell(numel(obj.selectedTransform),1);
         for m=1:numel(obj.selectedTransform)
            tfIdx = ismember(utilities.removeampersand(obj.transformDescriptions),obj.selectedTransform(m));
            tfCur = obj.availableTransform{tfIdx};
            trans{m} = tfCur.getTransformationInformation;
         end

% Det m� vi ordne senere (28-09-2016 09:21:04)
%           % Create overview (CR, 28-09-2016 09:01:04)
%            trans_table = table; % CR, 28-09-2016 09:11:01
%            if isstruct(trans{m})
%              FN = fieldnames(trans{m});
%              for f = 1:numel(FN)
%                 fn = matlab.lang.makeValidName([obj(m).selectedTransform,'_',FN{f}]);
%                 trans_table{m}.(fn) =  trans{m}.(FN{f})(:);
%              end
%            end

end % getTransformationInformation

% -------------------
function NewTransform = lxchecktransformstring(obj);
% Skal "lige" skrives f�rdig!! (CR, 13-10-2016 16:55:13)

  for m = 1:2
    NewTransform = cellstr(obj(m).selectedTransform); % The current transform settings. e.g. set by a GUI

    % --------------------------
    % Delete repeated selections
    % --------------------------
   %if ~isempty(obj.allowRepeated)
    if any(cell2mat(obj(m).allowRepeated))
      % hvad g�r det her ud p�? (CR, 13-10-2016 16:51:52)
      % S� skal der ca. "lige" skrives �n linie f�r og �n linie efter "unique"
        A = ismember(NewTransform,obj(m).allowRepeated); % ... (g�res ikke nu!)
    end

    NewTransform = unique(NewTransform,'stable'); % CR, 28-04-2015 09:09:15

    % -------------------------------------------------------------------------------
    % Select only one of "Autoscale", "Mean center", "Pareto scaling" a move the
    % last selected to the end last position
    % -------------------------------------------------------------------------------
    [~,B] = ismember(obj(m).exclusive,NewTransform);
    if any(B)
      [~,idx] = max(B);
      SelectedLast = obj(m).exclusive{idx};        % Find the last selected
      A = ismember(NewTransform,obj(m).exclusive); % Find all exclusive
      NewTransform(A) = '';                                   % ... and remove all appearences
      NewTransform{end+1} = SelectedLast;                     % Insert the last selected at the last position
    end
    obj(m).selectedTransform = NewTransform;                   % Save the ratified transform settings in the obj-object

% �ndr til det korrekte her (13-10-2016 16:45:31):
%
%         % Match obj.selectedTransform to function names
%           if strcmp(NewTransform,'None')
%               obj.selectedTransFunc{m} = '';
%           else
%               tf_txt = '';
%               for i = 1:numel(NewTransform)
%                 tf_txt = strvcat(tf_txt, obj.TransformTypes.availableTransform{strcmp(removeampersand(obj.TransformTypes.transformDescriptions), NewTransform{i})});
%               end
%               obj.selectedTransFunc{m} = cellstr(tf_txt);
%           end

  end % for m = 1:2


% 22-09-2016 08:36:47

%%%%% ej kalde lxtransform, n�r der ikker bedt om det!?!? 23-09-2016 06:01:15
%%%%%  ST = dbstack;
%%%%%  if ~any(strcmp({ST.name},'lxdata.set_selected')) % Not Called from lxdata.set_selected
%%%%%     lxtransform(obj);  % Sets the obj.trmat property
%%%%%  end
%%%%%
% lx_oop, 28-04-2015 09:39:09
% Test e.g. med:
% obj.selectedMatNum = 1; obj.selectedTransform{obj.selectedMatNum} = {'Pareto scaling','Mean center','Pareto scaling','Standard Normal Variate (SNV)','Autosca
% obj.selectedMatNum = 2; obj.selectedTransform{obj.selectedMatNum} = {'MSC','BLAH-BLAH','Pareto scaling','Mean center'}


%Speak('Transformation has been changed')
%for i=1:numel(obj.selectedTransform{obj.Selected.MatNum})
%  Speak(char(obj.selectedTransform{obj.Selected.MatNum}{i}))
%end

end % lxchecktransformstring

end % methods
end % classdef

