% TestLatentiX.m
%
% Dette script oprettes for at kunne teste funktioner i LX version 3.0
%
% Carsten Ridder, 28-06-2017 11:23:01

% !xcopy "D:\OD\OneDrive - ridder\CR\ridder.xyz\Projects\LatentiX\kode\August_2017\*.*" "C:\Users\cr\Dropbox\CR\LATENTIX\August_2017"  /D/Y/E

  clear all
  P = load('GlucFrucSuc (SNV, MSC, DER).lxf','-mat');

% P.DataSet =
%
%              Title: 'ICNIRS 2017'
%               data: [232x353 double]
%             Labels: {[232x16 char]  [353x8 char]}
%               Sets: {[1x1 struct]  [1x1 struct]}
%           SetStyle: [1x2 struct]
%             Models: {[1x1 struct]  [1x1 struct]  [1x1 struct]}
%        Predictions: []
%      CatVarsLoaded: []
%                Rep: {[232x1 double]  [353x1 double]}


% Fra "runbatch" i latentix.m:
% latentix <filename> <OBJ_begin OBJ_end XVAR_begin XVAR_end YVAR_begin YVAR_end> <TransformFile> <CalMethod> <ValMethod> <NumSegs> <MaxPC> <OptPC> < <OBJ_TST_begin> <OBJ_TST_end> >
% latentix "runbatch" "2013-10-14_NIR_a.CSV" "1 217 1 99 100 100" "SNV; MSC; SG w=9 o=2 d=2; Mean center" "PLS" "CV: Random" "20" "20" "6"

  i_X  = 2:2:232; % The selection in Objects listbox ("Xcal" (hver anden, start fra j = 2))
  j_X  = 4:353;   % The selection in Variables listbox ("NIR spectra")
  j_Y  = 3;       % Sucrose
  i_Xt = 3:2:231; % (when Test set is selected) The selection in Objects listbox ("Xval" (hver anden, start fra j = 3))


% ============ Data, Transformation and Model setup: create the structure "DTM" ============

% 1. Model settings (de ting Klavs skal give til mig fra GUI'en)
  DTM.X                    = P.DataSet.data( i_X,  j_X ); % M� ikke v�re tom
  DTM.Y                    = P.DataSet.data( i_X,  j_Y ); % Gerne tom ved PCA, men ikke ved PLS
  DTM.Xt                   = P.DataSet.data( i_Xt, j_X ); % Gerne tom, men ikke ved Test set validation
  DTM.Yt                   = P.DataSet.data( i_Xt, j_Y ); % ...
  DTM.CalMethod            = 'PLS'; % 'PCA';
  DTM.ValMethod            = 'No validation';
  DTM.selectedTransform{1} = {'Standard Normal Variate (SNV)','Mean center'}; % 08-08-2017 13:30:19
  DTM.selectedTransform{2} = {''};
  DTM.NumSegs              = 4;
  DTM.OptimalDimension     = 1;
  DTM.MaxPC                = 6;
  DTM.GlobalMinPC          = 6;
  DTM.NumRandomRepeats     = 20;

% V�lg CalMethod blandt "lxm.ModelTypes"
% V�lg ValMethod blandt "lxm.ValidationTypes"
% V�lg transform method blandt "lxm.lxt(1).availableTransform"

% 2. Type of model (create the MATLAB model-object)
  switch DTM.CalMethod
    case 'PCA', lxm = lxmodels.lxpca; % 07-08-2017 14:02:39
    case 'PLS', lxm = lxmodels.lxpls; % 08-08-2017 11:20:37
  end

% 3. Transfer selections
%  lxm.ValMethod                 = DTM.ValMethod;

  lxm.ModelSettings.MaxPC            = DTM.MaxPC;                         % Max number of PC's to use in the model
  lxm.ModelSettings.GlobalMinPC      = DTM.GlobalMinPC
  lxm.ModelSettings.OptimalDimension = DTM.OptimalDimension;              % The optimal number of PC's for the model
  lxm.ModelSettings.Validation       = DTM.ValMethod;
  lxm.ModelSettings.NumSegs          = DTM.NumSegs;                       % in cross-validation (CV)
  lxm.ModelSettings.NumRandomRepeats = DTM.NumRandomRepeats;

% 4. Transfer data
% lxm.rwmat holds the four currently selected raw and un-transformed matrices
  lxm.rwmat{1} = DTM.X;
  lxm.rwmat{2} = DTM.Y;
  lxm.rwmat{3} = DTM.Xt;
  lxm.rwmat{4} = DTM.Yt;

% 5. Transformations
  lxm.lxt = [lxTransform, lxTransform]; % Initialize X- and Y-transform objects

% Define transformation type
% lxm.lxt(1).selectedTransform = {'SG w=9 o=2 d=2','Standard Normal Variate (SNV)','Mean center'}; % SG medtaget for at teste! (28-06-2017 14:28:12)
  lxm.lxt(1).selectedTransform = DTM.selectedTransform{1}; % fjerner lige SG (30-06-2017 10:17:29)
  lxm.lxt(2).selectedTransform = DTM.selectedTransform{2}; % fjerner lige SG (30-06-2017 10:17:29)

% TESTER
         lxm.rwmat{1}(1,2) = nan;
         lxm.rwmat{1}(8,3) = nan;

%% Calculate the transformed matrices.
%  calc_trmat(lxm)


%  DTM =
%
%              CalMethod: 'PCA'
%              ValMethod: 'No validation'
%      selectedTransform: {{1x2 cell}  {1x1 cell}}
%                NumSegs: []
%                  MaxPC: 6
%            GlobalMinPC: 6
%                    lxm: [1x1 lxmodels.lxpca]

%  lxm =
%
%    lxpca with properties:
%
%                Number: []
%                  Name: []
%               Comment: []
%                 NumPC: []
%           DataSetName: []
%             CalMethod: 'PCA'
%                 rwmat: {[116x350 double]  [116x1 double]  [115x350 double]  [115x1 double]}
%                 trmat: {[116x350 double]  [116x1 double]  [115x350 double]  [115x1 double]}
%                   lxt: [1x2 lxTransform]
%             MissStats: []
%             ModelData: [1x1 struct]
%              PredData: [1x1 struct]
%              ModelSettings: [1x1 struct]
%      IndexIntoDataSet: {4x2 cell}
%            ModelTypes: {2x1 cell}
%       ValidationTypes: {9x1 cell}


  testcase = 2;
  switch testcase
    case 1, % Test af modelberegninger
            lxm.ModelSettings.Validation = 'CV: Syst123';
            calc_model(lxm)

    case 2, lxm.ModelSettings.Validation = 'Test set';
            SubModel = calc_model(lxm)

           % X = lxm.rwmat{1};
           % Y = lxm.rwmat{2};
           % I = size(X,1);
           % tstobj = logical([zeros(50,1); ones(I-50,1)]);
           %
           % SubModel = testval(lxm, X, Y, tstobj)

    case 3, % Test af transformationer (28-06-2017 15:39:55)
            X  = lxm.rwmat{1};
            Xt = lxm.rwmat{3};

           %trsObj   = transformations.meancenter;
           %trsObj   = transformations.snv;
            trsObj   = transformations.paretoscaling;


            trs_X    = trsObj.calculateTransform(X);
            trs_Xt   = trsObj.applyTransform(Xt);
            trs_info = trsObj.getTransformationInformation;

            close all
            figure(1)
              subplot(221)
                plot(mean(trs_X))
                title('mean X')
                add
              subplot(222)
                plot(std(trs_X))
                title('std X')
                add
              subplot(223)
                plot(mean(trs_Xt))
                title('mean Xt')
                add
              subplot(224)
                plot(std(trs_Xt))
                title('std Xt')
                add

    otherwise, disp('g�r ikke noget ...')
  end




%  lxm =
%
%    lxpca with properties:
%
%           GeneralInfo: [1x1 struct]
%                Number: []
%                  Name: []
%               Comment: []
%                 NumPC: []
%           DataSetName: []
%             CalMethod: 'PCA'
%             ValMethod: 'No validation'
%                  UseY: 0
%                 rwmat: {[116x350 double]  [116x1 double]  [115x350 double]  [115x1 double]}
%                 trmat: {[116x350 double]  [116x1 double]  [115x350 double]  [115x1 double]}
%                   lxt: [1x2 lxTransform]
%                output: []
%             MissStats: [1x1 struct]
%             ModelData: [1x1 struct]
%              ModelSeg: [1x1 struct]
%              PredData: [1x1 struct]
%               PredVal: [1x1 struct]
%              ModelSettings: [1x1 struct]
%      IndexIntoDataSet: {4x2 cell}
%            ModelTypes: {2x1 cell}
%       ValidationTypes: {9x1 cell}
%
%
%
%
%  lxm.ModelSettings =
%
%                 MaxPC: 6
%           GlobalMinPC: 6
%      OptimalDimension: 1
%            Validation: 'No validation'
%               NumSegs: 4
%      NumRandomRepeats: 20
%
%
%
%  lxm.ModelData =
%
%            P: [350x6 double]
%            T: [116x6 double]
%      eigvals: [6x1 double]
%       PctDes: [6x1 double]
%       PctVar: [6x1 double]
%           Si: [116x6 double]
%           Sj: [6x350 double]
%           Hi: [116x6 double]
%           T2: [116x6 double]
%        resXi: [116x6 double]
%        resXj: [6x350 double]
%         ssX0: 342.8427
%          ssX: [42.5925 1.7667 0.6633 0.1144 0.0728 0.0481]